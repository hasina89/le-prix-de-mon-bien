

<section class="plan_visite">
   <div class="container">
      <div class="col_left" id="plan_rdv">
         <div class="content">
            <div class="textPlan">
               <p><b>il reste encore <span class="textNbvisite">5 </span> visites à planifier</b> réservez votre visite pour ce bien en vente</p>
            </div>
            <form class="blcChp_visite" action="/estimez-ma-maison/prix-de-mon-bien-v4/leprixdesonbien/index.php" method="post" id="olr_reservation_form">
               <div class="row_visite">
                  <div class="left">
                     <input type="hidden" class="form-control" placeholder="" value="1723257" name="reference" id="reference">
                     <input type="hidden" class="form-control" placeholder="" value="" name="propID" id="propID">
                     <input type="hidden" class="form-control" placeholder="" value="" name="post_id" id="post_id">

                     <input type="hidden" class="form-control" placeholder="" value="pp4" name="agent-name" id="agent-name" readonly="">
                    
                     <div class="listDate">
                        <div class="item item1">
                           
                           <label for="date1" class="titre">Visite 1</label>
                           <select name="date1" id="date1">
                              <option value="">Sélectionnez dans la liste</option>
                           </select>
                           <div id="selected_date1" class="selected_date hidden">
                              <label class="titre">Valider</label>
                              <span class="button-bloc"><input type="button" name="submit-abus" value="OUI" class="submit desk" id="submit_reservation_1">&nbsp;<input type="button" name="cancel-abus" value="NON" class="submit desk" id="cancel_reservation_1"></span>
                           </div>
                        </div>
                        <div class="item item3 desable">
                           <label for="date3" class="titre">Visite 3</label>
                           <select name="date3" id="date3" disabled="disabled">
                              <option value="">Sélectionnez dans la liste</option>
                           </select>
                           <div id="selected_date3" class="selected_date hidden"><label class="titre">Valider</label>
                              <span class="button-bloc"><input type="button" name="submit-abus" value="OUI" class="submit desk" id="submit_reservation_3">&nbsp;<input type="button" name="cancel-abus" value="NON" class="submit desk" id="cancel_reservation_3"></span>
                           </div>
                        </div>
                        <div class="item item5 desable">
                           <label for="date5" class="titre">Visite 5</label>
                           <select name="date5" id="date5" disabled="disabled">
                              <option value="">Sélectionnez dans la liste</option>
                           </select>
                           <div id="selected_date5" class="selected_date hidden"><label class="titre">Valider</label>
                              <span class="button-bloc"><input type="button" name="submit-abus" value="OUI" class="submit desk" id="submit_reservation_5">&nbsp;<input type="button" name="cancel-abus" value="NON" class="submit desk" id="cancel_reservation_5"></span>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="right">
                     <div class="listDate">
                        <div class="item item2 desable">
                           <label for="date2" class="titre">Visite 2</label>
                           <select name="date2" id="date2" disabled="disabled">
                              <option value="">Sélectionnez dans la liste</option>
                           </select>
                           <div id="selected_date2" class="selected_date hidden"><label class="titre">Valider</label>
                              <span class="button-bloc"><input type="button" name="submit-abus" value="OUI" class="submit desk" id="submit_reservation_2">&nbsp;<input type="button" name="cancel-abus" value="NON" class="submit desk" id="cancel_reservation_2"></span>
                           </div>
                        </div>
                        <div class="item item4 desable">
                        
                           <label for="date4" class="titre">Visite 4</label>
                           <select name="date4" id="date4" disabled="disabled">
                              <option value="">Sélectionnez dans la liste</option>
                           </select>
                           <div id="selected_date4" class="selected_date hidden"><label class="titre">Valider</label>
                              <span class="button-bloc"><input type="button" name="submit-abus" value="OUI" class="submit desk" id="submit_reservation_4">&nbsp;<input type="button" name="cancel-abus" value="NON" class="submit desk" id="cancel_reservation_4"></span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               
            </form>
         </div>
      </div>
      <div class="col_right" id="faire_offre">
         <div class="content">
            <div class="textPlan">Offres reçues</div>
            <div class="listOffre mCustomScrollbar _mCS_1 mCS_no_scrollbar">
               <div id="mCSB_1" class="mCustomScrollBox mCS-inset-dark mCSB_vertical mCSB_inside" style="max-height: none;" tabindex="0">
                  <div id="mCSB_1_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y" style="position:relative; top:0; left:0;" dir="ltr">
                     <div id="sans_offre" class="item">
                        Aucun offre
                     </div>
                  </div>
                  <div id="mCSB_1_scrollbar_vertical" class="mCSB_scrollTools mCSB_1_scrollbar mCS-inset-dark mCSB_scrollTools_vertical" style="display: none;">
                     <a href="#" class="mCSB_buttonUp"></a>
                     <div class="mCSB_draggerContainer">
                        <div id="mCSB_1_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; height: 0px; top: 0px;">
                           <div class="mCSB_dragger_bar" style="line-height: 30px;"></div>
                           <div class="mCSB_draggerRail"></div>
                        </div>
                     </div>
                     <a href="#" class="mCSB_buttonDown"></a>
                  </div>
               </div>
            </div>
            <div class="blcOffre">
               <div class="blcTitreOffre">
                  <span class="bouton_close"></span>
                  <a class="btn">Soyez le premier à faire une offre</a>
               </div>
               <div class="blc_chp_offre" style="display: none;">
                  <form id="form_offre" class="form_offre">
                     <div class="blc_chp">
                        <div class="chp w_100">
                           <div class="champ">
                              <input type="text" class="form-control" placeholder="Nom de l'agence" value="pp4" name="agence_text" readonly="">
                           </div>
                        </div>
                     </div>
                     
                     <div class="blc_chp">
                        <div class="chp w_100">
                           <div class="champ">
                              <input type="text" class="form-control" placeholder="Prix de l'offre" value="" name="prix">
                           </div>
                        </div>
                     </div>
                     <div class="blc_chp blc_chp_radio blc_chp_radio_taux">
                        <div class="input_radio">
                           <input type="radio" id="taux_acompte_5" class="form-control" value="5%" name="taux_accompte" checked="">
                           <label for="taux_acompte_5">5%</label>
                        </div>
                        <div class="input_radio">
                           <input type="radio" id="taux_acompte_10" class="form-control" value="10%" name="taux_accompte">
                           <label for="taux_acompte_10">10%</label>
                        </div>
                     </div>
                     <div class="blc_chp blc_chp_file">
                        <div class="chp w_50 chp_carte">
                           <div class="cont-file">
                              <div class="content_cont_file">
                                 <input type="file" name="file[]" class="input-file">
                                 <div class="listBoutton">
                                    <span></span>
                                    <i class="txtBroswer"> Ajouter ma carte d'identité</i>
                                    <i class="reset" style="display: none">Supprimer</i>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="chp w_50 chp_carte">
                           <div class="cont-file">
                              <div class="content_cont_file">
                                 <input type="file" name="file[]" class="input-file">
                                 <div class="listBoutton">
                                    <span></span>
                                    <i class="txtBroswer"> Déposer votre document d’offre </i>
                                    <i class="reset" style="display: none">Supprimer</i>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <input type="hidden" name="propID" value="">
                     <input type="hidden" name="agence" value="" readonly="">
                     <input type="hidden" name="email_agence" value="" readonly="">
                     <input type="submit" name="submit_abus" value="Envoyer mon offre" class="submit">
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<style type="text/css">
    .listDate .textError {
        color: red !important;
    }

    .textError.titre_acces {
            text-align: center;
            color:red;
    }

    .textSuccess.titre_acces {
        text-align: center;
        center;color:green;
    }

    .loading {
        display: block;
        background: url('../image/loading.gif') no-repeat center middle;
        width: 100%;
        height: 200px;
        margin: 0 auto;
    }
    /* Hide all the children of the 'loading' element */
    .loading * {
        display: none;
    }

    .listDate button, .listDate select {
        width: 100%;
        font-size: 16px;
        margin: 10px;
    }

    .plan_visite .blcChp_visite .submit {
        display: inline-block !important;
        margin-top: 10px !important;
        padding: 0 10px 0 10px !important;
        width: auto !important;
    }

    .hidden {
        display: none;
    }

    .listDate .selected_date  {
        font-size: 15px;
        padding: 16px 0 16px 10px;
    }



    .button-bloc {
        float: right;
    }

    .plan_visite .listDate select,
    .plan_visite .selected_date{ height: 71px;border-radius: 35px;margin: 0;font:bold 18px/28px "Typold";-webkit-box-shadow: 0px 4px 16px -7px rgba(0,0,0,0.75);-moz-box-shadow: 0px 4px 16px -7px rgba(0,0,0,0.75);box-shadow: 0px 4px 16px -7px rgba(0,0,0,0.75);border: none;;padding: 10px 35px;}
    .plan_visite .listDate select{ background:  url(../image/arrow-down-b.svg)calc(100% - 35px) center no-repeat #A29F78;-webkit-appearance:none; -ms-appearance:none; appearance:none; -moz-appearance:none; -o-appearance:none;padding: 10px 35px;color: #ffffff;text-transform: uppercase; }
    .plan_visite .selected_date{ display: flex;justify-content: space-between;align-items: center; }
    .plan_visite .selected_date.hidden{ display: none; }
    .blcChp_visite .right{ padding-left: 20px; }
    .plan_visite .listDate .item {width: 100%;margin-bottom: 27px;}

    .plan_visite .blcChp_visite .button-bloc .submit{ margin-top: 0!important;width: 100px!important;display: block;height: 54px;background: #ffffff; }
    .blcChp_visite .row_visite .selected_date label { margin-bottom: 0; }
    .plan_visite .selected_date{ background:#A29F78 ; }
    .plan_visite .blcChp_visite .button-bloc .submit[name="submit-abus"]{ margin-right: 5px; }
    .plan_visite .blcChp_visite .button-bloc .submit[name="cancel-abus"]{ margin-left: 5px; }
    .plan_visite .blcChp_visite .button-bloc .submit:checked{ background: #000; }
    .plan_visite .blcChp_visite .button-bloc .submit:hover{ background:#00E29D;color: #ffffff; }


    @media (max-width:601px) {
        .blcChp_visite .right{ padding-left: 0; }
    }


</style>

