<?php

/**
 * Template Name: Autre Page
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}
get_header(); ?>

<main>

	<section>
		<div class="container pageOther">
			<div class="row">
				<div class="col-md-12">
					<?php while(have_posts()) : the_post(); ?>
						<h2><?php the_title(); ?></h2>
						<?php the_content(); ?>
					<?php endwhile; ?>
				</div>
			</div>
		</div>
	</section>

</main>

<?php get_footer(); ?>