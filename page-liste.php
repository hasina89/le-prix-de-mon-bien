<?php

/**

 * Template Name: Liste Biens

 */
get_header('partenaires'); 

get_template_part( 'template-parts/header/header', 'partenaire' );

$arg = array(
    'post_type'      => 'property',
    'posts_per_page' => -1,
    'paged'          => get_query_var('paged'),    
 );

$arg_spec = array(
  'meta_key'       => 'prix_de_vente',
    'orderby'        => array(
       'meta_value_num' => 'ASC',
       'ID'         => 'DESC',       
    ),
);

if( 
  ( isset( $_GET['type']) && !empty( $_GET['type'] ) && '*' != strip_tags( $_GET['type'] ) ) ||
  ( isset( $_GET['ville']) && !empty( $_GET['ville'] ) && '*' != strip_tags( $_GET['ville'] ) ) || 
  ( isset( $_GET['budget']) && !empty( $_GET['budget'] ) && '*' != strip_tags( $_GET['budget'] ) )
  ) {

  // query Type
  $type = array();
  if( '*' != strip_tags( $_GET['type'] ) ){
    $get_type = strip_tags( $_GET['type'] );
    $type = array(
      'key'     => 'categorie_batiment',
      'compare' => '=',
      'value'   => $get_type,
    );
  }

  // query Ville
  $ville = array();
  if( '*' != strip_tags( $_GET['ville'] ) ){
    $get_ville = strip_tags( $_GET['ville'] );
    $ville = array(
      'key'     => 'ville',
      'compare' => '=',
      'value'   => $get_ville,
    );
  }

  //query budget
  $budget = array();
  if( '*' != strip_tags( $_GET['budget'] ) ){
    $get_budget = strip_tags( $_GET['budget'] );
    $budgs = strip_tags( $_GET['budget'] );
    $budgs = explode(',', $budgs);
      $budget = array(
        'type'    => 'numeric',
        'key'     => 'prix_de_vente',
        'compare' => 'BETWEEN',
        'value'   => array( $budgs[0], $budgs[1] ),
      );
  }

  //query ordre prix
  $prix_ordre = strip_tags( $_GET['prix_ordre'] );
  if( 'ASC' != $prix_ordre && 'DESC' != $prix_ordre ){
    wp_die('Erreur ! Pirate en vue.');
  }

  $arg_spec = array(
    'meta_query' => array(
      'relation' => 'AND',
      $type,
      $ville,
      $budget,
    ),
    'meta_key' => 'prix_de_vente',
    'orderby' => array(
      'meta_value_num' => $prix_ordre,
      'ID'             => 'DESC',
    ),
  );
}else{
  //query ordre prix
  if( isset( $_GET['prix_ordre'] ) ){
    $prix_ordre = strip_tags( $_GET['prix_ordre'] );
    if( 'ASC' != $prix_ordre && 'DESC' != $prix_ordre ){
      wp_die('Erreur ! Pirate en vue.');
    }

    $arg_spec = array(
      'meta_key' => 'prix_de_vente',
      'orderby' => array(
        'meta_value_num' => $prix_ordre,
        'ID'             => 'DESC',
      ),
    );
  }
}

$arg += $arg_spec;

$q = new WP_Query( $arg );

$total_posts = $q->found_posts;
$par_page = 12;
$nbr_de_page = (int)ceil( $total_posts / $par_page );

?>

<form id="blcSearch" action="<?= site_url('liste-biens') ?>">
  <div class="sec_filter">
    <div class="listFiltre" id="filter_form">
       <div class="filtre2">
          <div class="item item1">
             <span class="label">Vous cherchez ?</span>
             <div class="select">
                <select name="type">
                    <option value="*">Tout type</option>
                    <option value="maison" <?php if( $get_type == 'maison') echo "selected" ?>>Maison</option>
                    <option value="appartement" <?php if( $get_type == 'appartement') echo "selected" ?>> Appartement</option>
                    <option value="terrain" <?php if( $get_type == 'terrain') echo "selected" ?>>Terrain</option>
                    <option value="bureau" <?php if( $get_type == 'bureau') echo "selected" ?>>Bureau</option>
                    <option value="commerce" <?php if( $get_type == 'commerce') echo "selected" ?>>Commercial</option>
                    <option value="industriel" <?php if( $get_type == 'industriel') echo "selected" ?>>Industriel</option>
                    <option value="garage/parking" <?php if( $get_type == 'garage/parking') echo "selected" ?>>garage/parking</option>
                  </select>
             </div>
          </div>
          <div class="item item2">
             <span class="label">Dans quelle ville ?</span>
             <div class="select">
                 <select name="ville">
                    <option value="*">Toutes</option>
                    <optgroup label="Belgique">
                       <option value="Anderlues" <?php if( $get_ville == 'Anderlues') echo "selected" ?>>Anderlues</option>
                       <option value="Audregnies" <?php if( $get_ville == 'Audregnies') echo "selected" ?>>Audregnies</option>
                       <option value="Baudour" <?php if( $get_ville == 'Baudour') echo "selected" ?>>Baudour</option>
                       <option value="Bernissart harchies" <?php if( $get_ville == 'Bernissart harchies') echo "selected" ?>>Bernissart harchies</option>
                       <option value="Binche" <?php if( $get_ville == 'Binche') echo "selected" ?>>Binche</option>
                       <option value="Binche bray" <?php if( $get_ville == 'Binche bray') echo "selected" ?>>Binche bray</option>
                       <option value="Binche waudrez" <?php if( $get_ville == 'Binche waudrez') echo "selected" ?>>Binche waudrez</option>
                       <option value="Blaton" <?php if( $get_ville == 'Blaton') echo "selected" ?>>Blaton</option>
                       <option value="Boussu" <?php if( $get_ville == 'Boussu') echo "selected" ?>>Boussu</option>
                       <option value="Boussu hornu" <?php if( $get_ville == 'Boussu hornu') echo "selected" ?>>Boussu hornu</option>
                       <option value="Boussu-bois" <?php if( $get_ville == 'Boussu-bois') echo "selected" ?>>Boussu-bois</option>
                       <option value="Colfontaine" <?php if( $get_ville == 'Colfontaine') echo "selected" ?>>Colfontaine</option>
                       <option value="Dour" <?php if( $get_ville == 'Dour') echo "selected" ?>>Dour</option>
                       <option value="Dour elouges" <?php if( $get_ville == 'Dour elouges') echo "selected" ?>>Dour elouges</option>
                       <option value="Dour wiheries" <?php if( $get_ville == 'Dour wiheries') echo "selected" ?>>Dour wiheries</option>
                       <option value="Elouges" <?php if( $get_ville == 'Elouges') echo "selected" ?>>Elouges</option>
                       <option value="Erquennes" <?php if( $get_ville == 'Erquennes') echo "selected" ?>>Erquennes</option>
                       <option value="Frameries" <?php if( $get_ville == 'Frameries') echo "selected" ?>>Frameries</option>
                       <option value="Haine saint pierre" <?php if( $get_ville == 'Haine saint pierre') echo "selected" ?>>Haine saint pierre</option>
                       <option value="Hainin" <?php if( $get_ville == 'Hainin') echo "selected" ?>>Hainin</option>
                       <option value="Hautrage" <?php if( $get_ville == 'Hautrage') echo "selected" ?>>Hautrage</option>
                       <option value="Hensies hainin" <?php if( $get_ville == 'Hensies hainin') echo "selected" ?>>Hensies hainin</option>
                       <option value="Honnelles" <?php if( $get_ville == 'Honnelles') echo "selected" ?>>Honnelles</option>
                       <option value="Hornu" <?php if( $get_ville == 'Hornu') echo "selected" ?>>Hornu</option>
                       <option value="Houdeng-aimeries" <?php if( $get_ville == 'Houdeng-aimeries') echo "selected" ?>>Houdeng-aimeries</option>
                       <option value="Jemappes" <?php if( $get_ville == 'Jemappes') echo "selected" ?>>Jemappes</option>
                       <option value="La louviere" <?php if( $get_ville == 'La louviere') echo "selected" ?>>La louviere</option>
                       <option value="Lessines" <?php if( $get_ville == 'Lessines') echo "selected" ?>>Lessines</option>
                       <option value="Mons" <?php if( $get_ville == 'Mons') echo "selected" ?>>Mons</option>
                       <option value="Montroeul-sur-haine" <?php if( $get_ville == 'Montroeul-sur-haine') echo "selected" ?>>Montroeul-sur-haine</option>
                       <option value="Paturages" <?php if( $get_ville == 'Paturages') echo "selected" ?>>Paturages</option>
                       <option value="Petit dour" <?php if( $get_ville == 'Petit dour') echo "selected" ?>>Petit dour</option>
                       <option value="Quaregnon" <?php if( $get_ville == 'Quaregnon') echo "selected" ?>>Quaregnon</option>
                       <option value="Quevy" <?php if( $get_ville == 'Quevy') echo "selected" ?>>Quevy</option>
                       <option value="Quievrain" <?php if( $get_ville == 'Quievrain') echo "selected" ?>>Quievrain</option>
                       <option value="Saint-ghislain" <?php if( $get_ville == 'Saint-ghislain') echo "selected" ?>>Saint-ghislain</option>
                       <option value="Sirault" <?php if( $get_ville == 'Sirault') echo "selected" ?>>Sirault</option>
                       <option value="Thulin" <?php if( $get_ville == 'Thulin') echo "selected" ?>>Thulin</option>
                       <option value="Uccle" <?php if( $get_ville == 'Uccle') echo "selected" ?>>Uccle</option>
                       <option value="Wasmes" <?php if( $get_ville == 'Wasmes') echo "selected" ?>>Wasmes</option>
                       <option value="Wasmuel" <?php if( $get_ville == 'Wasmuel') echo "selected" ?>>Wasmuel</option>
                    </optgroup>
                    <optgroup label="France">
                       <option value="Crespin" <?php if( $get_ville == 'Crespin') echo "selected" ?>>Crespin</option>
                       <option value="Hon hergies" <?php if( $get_ville == 'Hon hergies') echo "selected" ?>>Hon hergies</option>
                    </optgroup>
                 </select>
             </div>
          </div>
          <div class="item item3">
             <span class="label">Quel budget ?</span>
             <div class="select">
                 <select name="budget">
                     <option value="*">Tous</option>
                     <option value="0,100000" <?php if( $get_budget == '0,100000') echo "selected" ?>>
                        0€ - 100.000€                   
                     </option>
                     <option value="100001,200000" <?php if( $get_budget == '100001,200000') echo "selected" ?>>
                        100.001€ - 200.000€                   
                     </option>
                     <option value="200001,300000" <?php if( $get_budget == '200001,300000') echo "selected" ?>>
                        200.001€ - 300.000€                   
                     </option>
                     <option value="300001,400000" <?php if( $get_budget == '300001,400000') echo "selected" ?>>
                        300.001€ - 400.000€                   
                     </option>
                     <option value="400001,500000" <?php if( $get_budget == '400001,500000') echo "selected" ?>>
                        400.001€ - 500.000€                   
                     </option>
                     <option value="500001,600000" <?php if( $get_budget == '500001,600000') echo "selected" ?>>
                        500.001€ - 600.000€                   
                     </option>
                     <option value="600001,700000" <?php if( $get_budget == '600001,700000') echo "selected" ?>>
                        600.001€ - 700.000€                   
                     </option>
                     <option value="700001,800000" <?php if( $get_budget == '700001,800000') echo "selected" ?>>
                        700.001€ - 800.000€                   
                     </option>
                     <option value="800001,900000" <?php if( $get_budget == '800001,900000') echo "selected" ?>>
                        800.001€ - 900.000€                   
                     </option>
                     <option value="900001,1000000" <?php if( $get_budget == '900001,1000000') echo "selected" ?>>
                        900.001€ - 1.000.000€                   
                     </option>
                     <option value="1000001,999999999999" <?php if( $get_budget == '1000001,999999999999') echo "selected" ?>>
                        Plus de 1.000.000€                   
                     </option>
                  </select>
             </div>
          </div>
       </div>
       <div class="search">
          <button type="submit" class="submit btn btn_partenaire" >Chercher</button>
       </div>
    </div>
  </div>
  <div class="triage">
    <div class="container">
      <div class="list_bouton">
          <div class="order">
            <div class="chp">
                <label class="label">Trier par</label>
                <div class="select">
                  <select class="order_price" name="prix_ordre">
                    <option value="ASC" <?php if( $prix_ordre == 'ASC') echo "selected" ?>>Prix croissant</option>
                    <option value="DESC" <?php if( $prix_ordre == 'DESC') echo "selected" ?>>Prix décroissant</option>
                  </select>
                </div>
            </div>
          </div>
          <div class="etat_biens">
                <div class="inner">
                   <div class="label">Voir les biens</div>
                   <div class="v-biens">Disponibles</div>
                   <ul class="list">
                     <li data-filter=".dispo"><span class="item">Disponibles</span></li>
                     <li data-filter=".nouveau"><span class="item">Nouveaux </span></li>
                     <li data-filter=".sous_options"><span class="item">Sous options</span></li>
                     <li data-filter=".vendu"><span class="item">Vendu</span></li>
                   </ul>
                   <input type="hidden" id="type" value="<?= $_GET['type'] ? $_GET['type']:'*' ?>">
                   <input type="hidden" id="ville" value="<?= $_GET['ville'] ? $_GET['ville']:'*' ?>">
                   <input type="hidden" id="budget" value="<?= $_GET['budget'] ? $_GET['budget']:'*' ?>">
                   <input type="hidden" id="prix_ordre" value="<?= $_GET['prix_ordre'] ? $_GET['prix_ordre']:'ASC' ?>">
               </div>
          </div>
      </div>
      <div class="pagination">
        <div class="inner">
          <div class="label filter-count">Page</div>
          <ul class="listPage" data-parpage="<?= $par_page ?>">
            <?php for( $i = 1; $i <= $nbr_de_page; $i++ ){ ?>
              <li class="pager <?= $i == 1 ? 'active':'' ?>" data-filter=".<?= $i ?>"><span><?= $i ?></span></li>
            <?php } ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
</form>

<div class="listBiens filter_biens" id="listBiens">
    <?php
        if( $q->have_posts() ){

            $now = new DateTime();

            $k = 1;
            $j = 1;

            while( $q->have_posts() ){
                $q->the_post();

                $propID = get_the_ID();

                $fiche_cloturee = get_post_meta( get_the_ID(), 'fiche_cloturee', true );

                $statut = get_field('statut_transaction');
                if( $fiche_cloturee && 'Vendu' != $statut ){
                  update_field('statut_transaction','Sous option', $propID);
                  $statut = get_field('statut_transaction');
                }

                if( 'Vendu' == $statut ){
                  $color = '#a29f78 !important';
                }elseif( 'Sous option' == $statut ){
                  $color = '#9b9b9b !important';
                }else{
                  $color = '#00E29D !important';
                }

                $date_creation = get_field('field_creation' ) ? get_field('field_creation' ) : "01/01/2021 13:42";

                $date_creation_1 = new DateTime( $date_creation );
                $date_creation_1 = $date_creation_1->format('Y-m-d H:i:s');
                $date_creation_1 = new DateTime( $date_creation_1 );

                $date_creation_2 = new DateTime( $date_creation );
                $date_creation_2 = $date_creation_2->format('Y-m-d H:i:s');
                $date_creation_2 = new DateTime( $date_creation_2 );

                $_24h_apres_ajout = $date_creation_1->add( new DateInterval('P1D') );

                $fin = $date_creation_2->add( new DateInterval('P2D') );
                $diff = $now->diff($fin);

                $isotope_class = 'dispo';
                if( 'Vendu' == $statut ){
                    $isotope_class = 'vendu';
                }elseif( 'Sous option' == $statut ){
                    $isotope_class = 'sous_options';
                }elseif( 'A vendre' == $statut && $now->diff( $_24h_apres_ajout )->invert > 0 ){
                    $isotope_class = 'dispo';
                }elseif( 'A vendre' == $statut && $now->diff( $_24h_apres_ajout )->invert == 0 ){
                    $isotope_class = 'nouveau dispo';
                }

                
    ?>
                    <div class="item <?= 'Vendu' == $statut || 'Sous option' == $statut ? 'no_btn_vente':''; ?> <?= 'Vendu' == $statut ? 'container_vendu':''; ?> <?= $isotope_class.' '.$k ?>">
                      <div class="content" <?= 'Vendu' == $statut ? 'style="opacity:0.5"':''; ?>>
                          <div class="top_biens">
                              <div class="blcImg">
                                  
                                      <?php
                                         $existing_imgs = get_field('field_60e44529224ad', $propID);
                                         if( is_array($existing_imgs) && count( $existing_imgs ) ){
                                           $pic = $existing_imgs[0];
                                         if( array_key_exists('image_large', $pic)):
                                      ?>
                                      <div class="img">
                                         <img class="image" src="<?= $pic['image_large'] ?>">
                                      </div>
                                      <?php endif; }else{ ?>
                                       <div class="img img_logo">
                                         <img class="image logo_vdmb" src="<?= IMG_DIR . 'logo_vendez_partenaires.svg' ?>">
                                      </div>
                                      <?php } ?>
                     
                                  
                                  <div class="info_top">
                                       <?php 
                                            $peb_img = 'peb-A.png';
                                            if( get_field('niveau_peb', $propID) ) $peb_img = 'peb-'.get_field('niveau_peb', $propID).'.png';
                                        ?>
                                      <span class="peb"><img  class="img_peb" src="<?= IMG_DIR. $peb_img ?>"></span>
                                      <div class="blcSous_option">
                                          <span class="tag" style="background:<?= $color ?>">
                                             <?php echo $statut; ?>
                                          </span>
                                          <?php
                                            $show_buynow_btn = false;

                                            if( $statut != 'Vendu' && $statut != 'Sous option' ){
                                                if( $now->diff( $_24h_apres_ajout )->invert == 0 ){

                                                  $was_bought_now = get_post_meta( $propID, 'was_bought_now', true );

                                                  if( !$was_bought_now || ''==$was_bought_now ){

                                                    $show_buynow_btn = true;

                                                    $url = wp_nonce_url( get_permalink($propID), "nonce_buy_now", '_sc' );
                                          ?>
                                                    <a href="<?= $url ?>" class="buy_now" data-prop="<?= $propID ?>">buy now</a>
                                          <?php      
                                                  }
                                                }
                                            } 
                                          ?>
                                      </div>
                                  </div>
                                  <?php if( 'Vendu' == $statut ){ ?>
                                    <div class="bien_vendu">
                                       <div class="titre_vendu">Bien vendu par</div> 
                                       <div><img class="image_oblique" src="<?= IMG_DIR . 'logo_vendez_partenaires.svg' ?>"></div>
                                    </div>
                                 <?php } ?>
                              </div>
                              
                              <div class="etat_biens d-flex justify-content-center">
                                  <div class="left">
                                       <?php
                                          $fiche_id = get_current_product_id() ? get_current_product_id() : 0;
                                          $sales = (int)get_post_meta( $fiche_id, 'total_sales', true );
                                          $achat_max = 5;
                                          $total_sales = $achat_max - $sales;

                                           if( $total_sales < 0 ) {
                                              $total_sales = 0;
                                           }
                                        ?>
                                      Il reste :<br> 
                                      <?php echo $total_sales .'/5' ?> achat(s)
                                  </div>
                              </div>
                          </div>
                          <div class="bottom_biens<?= $show_buynow_btn ? ' misy_buy_now':'' ?>">
                              <div class="blcPrix">
                                  <div class="location"><?= get_field('categorie_batiment') ?> à <?= get_field('ville') ?></div>
                                  <div class="prix">
                                      Faire offre à partir de
                                      <span><?= millier( get_field('prix_de_vente') ); ?>€</span>
                                  </div>
                              </div>
                              <div class="blc_caract_biens">
                                  <div class="blc">
                                      <?php if (get_field('field_60e44a6bf62db', $propID)) { ?>
                                          <span class="picto"><img  class="img_peb" src="<?= IMG_DIR.'icon-bed.png' ?>"></span>
                                      <?php } ?>
                                      <span><?= get_field('field_60e44a6bf62db', $propID) ?></span>
                                  </div>
                                  <div class="blc">
                                      <span class="picto"><img  class="img_peb" src="<?= IMG_DIR.'icon-distance.png' ?>"></span>
                                      <span>
                                          <?php if( get_field('field_60e44899d4def', $propID) )
                                              echo get_field('field_60e44899d4def', $propID);
                                          ?> <sup>m2</sup>
                                      </span>
                                  </div>
                              </div>
                              <?php 
                                 $fiche_id = get_current_product_id() ? get_current_product_id() : 0;
                                 $sales = (int)get_post_meta( $fiche_id , 'total_sales', true );
                                 if( !$fiche_cloturee && 'Vendu' != $statut ): ?>
                                  <div class="hide">
                                    <?php
                                      $texte_btn = $sales < 5 ? "vendre ce bien" : "accès complet";

                                      if( $show_buynow_btn ){
                                    ?>
                                        <a href="<?= $url ?>" class="btn buy_now">buy now</a>
                                    <?php } ?>

                                      <a href="<?= get_permalink( get_the_ID() ) ?>" class="btn"><?= $texte_btn ?></a>

                                  </div>
                              <?php endif; ?>
                          </div>
                      </div>
                    </div>
    <?php
              $j++;
              if( $k == 1 ){
                if( $j == $par_page + 1 ){
                  $j = 0;
                  $k++;
                } 
              }else{
                if( $j == $par_page ){
                  $j = 0;
                  $k++;
                } 
              }
                           
            }
            wp_reset_query();
        }
    ?>
    

</div>

<div class="pagination">
  <div class="inner">
    <div class="label filter-count">Page</div>
    <ul class="listPage" data-parpage="<?= $par_page ?>">
      <?php for( $i = 1; $i <= $nbr_de_page; $i++ ){ ?>
        <li class="pager <?= $i == 1 ? 'active':'' ?>" data-filter=".<?= $i ?>"><span><?= $i ?></span></li>
      <?php } ?>
    </ul>
    <!-- <div class="v-biens">1</div> -->
    <?php //cq_pagination( $q->max_num_pages); ?>
  </div>
</div>

<?php get_footer('partenaire'); 
