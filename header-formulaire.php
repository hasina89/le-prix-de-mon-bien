<!DOCTYPE html>
<?php astra_html_before(); ?>
<html <?php language_attributes(); ?>>
<head>
    <?php //astra_head_top(); ?>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11" />
    <link rel="icon" href="<?= IMG_DIR . 'fav.png'; ?>">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?> >
<header class="header-arson formulaire" style="background-image: url(<?php the_field('banner', 'option') ?>);" >
    <div class="top">
        <a href="<?= site_url(); ?>" class="logo wow fadeInUp">
            <img src="<?php the_field('logo','option') ?>" alt="">
        </a>
    </div>
</header>