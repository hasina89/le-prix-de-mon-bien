<?php
add_action('wp_ajax_handler_9', 'handler_9');
add_action('wp_ajax_nopriv_handler_9', 'handler_9');

function handler_9(){
	if(isset($_POST) && !empty($_POST)){
		$str = http_build_query($_POST);
    parse_str($str, $Data);

    extract($Data);

    $current_user = get_unfinished_lead_expertise( sess('email') );
    $id = $current_user->id;

    update_lead_step_9( $id,$prixvente,$rgpd,$motiv,$acceptement );

    save_session_step_9(  $prixvente,$motiv,$rgpd,$acceptement );

    $ret = array();
    prepare_checkout();
    $ret['ck'] = site_url('/paiement');

    echo json_encode($ret);
		wp_die();
	}
}

function update_lead_step_9( $id,$prixvente,$rgpd,$motiv,$acceptement){
	global $wpdb;

	$lead_expertise = $wpdb->prefix.'lead_expertise';

  $new_data = array(
    'derniere_etape' => 9,
    'derniere_expertise' => date("d-m-Y H:i:s"),

    'envisage_vendre_bien'      => $prixvente,    
    'rgpd'                      => $rgpd,
    'motiv'                     => $motiv,
    'acceptement'               => $acceptement,
    
    );

    $update = $wpdb->update( 
        $lead_expertise,
        $new_data,
        array(
            'id' => $id
        ),
    );

    if( $update ){
        return true;
    }else{
        return false;
    }

}

function save_session_step_9( $prixvente,$motiv,$rgpd,$acceptement ){
	$_SESSION['envisage_vendre_bien'] = $prixvente;  
  $_SESSION['motiv'] = $motiv;
  $_SESSION['rgpd'] = $rgpd;    
  $_SESSION['acceptement'] = $acceptement;
	return true;
}

add_action('wp_ajax_back_from_9', 'back_from_9');
add_action('wp_ajax_nopriv_back_from_9', 'back_from_9');

function back_from_9(){
  if(isset($_POST) && !empty($_POST))
    {
      $str = http_build_query($_POST);
      parse_str($str, $Data);

      extract($Data);

      ob_start();
        include ASTRA_THEME_CHILD_DIR . 'steps/s'. $show .'.php';
      $out = ob_get_clean();

      echo $out;
      wp_die();

    }
}