<?php
add_action('wp_ajax_handler_8', 'handler_8');
add_action('wp_ajax_nopriv_handler_8', 'handler_8');

function handler_8(){
	if(isset($_POST) && !empty($_POST)){
		$str = http_build_query($_POST);
    parse_str($str, $Data);

    extract($Data);

    $current_user = get_unfinished_lead_expertise( sess('email') );
    $id = $current_user->id;

    update_lead_step_8( $id,$proprietaire_bien,$select_call,$obje_estim,$informe_evolution_marche,$informe_achat_bien,$date );

    save_session_step_8( $proprietaire_bien,$informe_evolution_marche,$informe_achat_bien,$obje_estim,$select_call,$date );

    ob_start();
			include ASTRA_THEME_CHILD_DIR . 'steps/s9.php';
		$out = ob_get_clean();

		echo $out;
		wp_die();
	}
}

function update_lead_step_8( $id,$proprietaire_bien,$select_call,$obje_estim,$informe_evolution_marche,$informe_achat_bien,$date ){
	global $wpdb;

	$lead_expertise = $wpdb->prefix.'lead_expertise';

  $new_data = array(
    'derniere_etape' => 8,
    'derniere_expertise' => date("d-m-Y H:i:s"),
    'proprietaire_bien'         => $proprietaire_bien,
    'select_call'                => $select_call,
    'obje_estim'                => $obje_estim,
    'informe_evolution_marche'  => $informe_evolution_marche,
    'informe_achat_bien'        => $informe_achat_bien,
    'date'                      => $date,
    );

    $update = $wpdb->update( 
        $lead_expertise,
        $new_data,
        array(
            'id' => $id
        ),
    );

    if( $update ){
        return true;
    }else{
        return false;
    }

}

function save_session_step_8( $proprietaire_bien,$informe_evolution_marche,$informe_achat_bien,$obje_estim,$select_call,$date ){
  $_SESSION['proprietaire_bien'] = $proprietaire_bien;
  $_SESSION['informe_evolution_marche'] = $informe_evolution_marche;
  $_SESSION['informe_achat_bien'] = $informe_achat_bien;
  $_SESSION['obje_estim'] = $obje_estim;
  $_SESSION['select_call'] = $select_call;
  $_SESSION['date'] = $date;

	return true;
}

add_action('wp_ajax_back_from_8', 'back_from_8');
add_action('wp_ajax_nopriv_back_from_8', 'back_from_8');

function back_from_8(){
  if(isset($_POST) && !empty($_POST))
    {
      $str = http_build_query($_POST);
      parse_str($str, $Data);

      extract($Data);

      ob_start();
        include ASTRA_THEME_CHILD_DIR . 'steps/s'. $show .'.php';
      $out = ob_get_clean();

      echo $out;
      wp_die();

    }
}