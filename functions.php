<?php
/**
 * Vendez mon bien Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Vendez mon bien
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_VENDEZ_MON_BIEN_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'vendez-mon-bien-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_VENDEZ_MON_BIEN_VERSION, 'all' );

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

/**
 * Define Constants
 */
define( 'ASTRA_THEME_SETTINGS', 'astra-settings' );
define( 'ASTRA_THEME_CHILD_DIR', trailingslashit( get_stylesheet_directory() ) );
define( 'ASTRA_THEME_CHILD_URI', trailingslashit( esc_url( get_stylesheet_directory_uri() ) ) );

define( 'INC_DIR', ASTRA_THEME_CHILD_DIR . '/inc/' );
define( 'ASSETS_DIR', get_stylesheet_directory_uri() . '/assets/' );
define( 'CSS_DIR', ASSETS_DIR . 'css/' );
define( 'FONT_DIR', ASSETS_DIR . 'fonts/' );
define( 'IMG_DIR', ASSETS_DIR . 'images/' );
define( 'JS_DIR', ASSETS_DIR . 'js/' );

require_once ASTRA_THEME_CHILD_DIR . 'inc/produit_abonnement.php';

/* Handlers des étapes */
for ($i=0; $i <11; $i++) { 
  require_once ASTRA_THEME_CHILD_DIR . 'handler/h'. $i .'.php';
}

/* Handler abus */
require_once ASTRA_THEME_CHILD_DIR . 'inc/abus.php';

/* count view */
require_once ASTRA_THEME_CHILD_DIR . 'inc/count_view.php';

require_once ASTRA_THEME_CHILD_DIR . 'inc/faire-offre.php';
require_once ASTRA_THEME_CHILD_DIR . 'inc/buy-lead.php';
require_once ASTRA_THEME_CHILD_DIR . 'inc/buy-abonnement.php';
require_once ASTRA_THEME_CHILD_DIR . 'inc/buy-now.php';

/* CPTs */
require_once ASTRA_THEME_CHILD_DIR . 'cpts/cpts.php';

define("TRANSLATION_URL", ASTRA_THEME_CHILD_DIR . "/languages");

function my_theme_localized($locale)
{
    if (isset($_GET['lang'])){
        return sanitize_key($_GET['lang']);
    }

    return $locale;
}
add_filter('locale', 'my_theme_localized');

function my_theme_setup(){
    load_theme_textdomain('themehigh-multiple-addresses', TRANSLATION_URL);
}
add_action('after_setup_theme', 'my_theme_setup');

/**
 * Assets management
 */
add_action( 'wp_enqueue_scripts', 'assets_loader' );
function assets_loader() {
    // CSS
    wp_enqueue_style( 'slick', CSS_DIR . 'slick.css' );
    wp_enqueue_style( 'animate', CSS_DIR . 'animate.css' );
     wp_enqueue_style( 'datepicker_css', CSS_DIR . 'datepicker.css' );
    wp_enqueue_style( 'bootstrap', CSS_DIR . 'bootstrap.min.css' );
    wp_enqueue_style( 'jquery-ui', CSS_DIR . 'jquery-ui.min.css' );
     wp_enqueue_style( 'mCustomScrollbar', CSS_DIR . 'jquery.mCustomScrollbar.min.css' );
    wp_enqueue_style( 'app-form', CSS_DIR . 'app-form.css' );
    wp_enqueue_style( 'fancybox-css', "https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" );
    wp_enqueue_style( 'style', CSS_DIR . 'styles.css' );

    // JS
    wp_enqueue_script( 'jquery', JS_DIR . 'jquery-3.5.1.min.js' , array(), false, true );
    wp_enqueue_script( 'fancybox-js', "https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js" , array(), false, true );
    wp_enqueue_script( 'slick', JS_DIR . 'slick.min.js' , array(), false, true );
    wp_enqueue_script( 'countdown', JS_DIR . 'jquery.simple.timer.js' , array(), false, true );
    wp_enqueue_script( 'bootstrap', JS_DIR . 'bootstrap.min.js' , array(), false, true );
    wp_enqueue_script( 'mCustomScrollbar', JS_DIR . 'jquery.mCustomScrollbar.min.js' , array(), false, true ); 
    wp_enqueue_script( 'wow', JS_DIR . 'wow.js' , array(), false, true );
    wp_enqueue_script( 'jquery-ui', JS_DIR . 'jquery-ui.min.js' , array(), false, true );
    wp_enqueue_script( 'datepicker', JS_DIR . 'jquery-ui-timepicker-addon.js' , array(), false, true );
    wp_enqueue_script( 'validate', JS_DIR . 'validate.js' , array(), false, true );
    wp_enqueue_script( 'isotope', JS_DIR . 'isotope.js' , array(), false, true );
    if( is_home() || is_front_page() || is_page('faq') || is_page('shop') ){
        wp_enqueue_script( 'home_script', JS_DIR . 'home_script.js' , array(), false, true );    
        wp_enqueue_script( 'home_ajax', JS_DIR . 'home_ajax.js' , array(), false, true );    
        // AJAX
        wp_localize_script( 'home_ajax', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
    }
    if( is_singular( array('property','proprietaire') ) ){
        wp_enqueue_script( 'home_ajax', JS_DIR . 'home_ajax.js' , array(), false, true ); 
        wp_enqueue_script( 'single_js', JS_DIR . 'single.js' , array(), false, true );    
        // AJAX
        wp_localize_script( 'home_ajax', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
    }
    if( is_page('demander-expertise') || is_page('demande-expertise-coach') ){
        wp_enqueue_script( 'demander_expertise', JS_DIR . 'demander_expertise.js' , array(), false, true );
        // AJAX
        wp_localize_script( 'demander_expertise', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
    }
    if( is_page('shop') ){
        wp_enqueue_script( 'shop', JS_DIR . 'shop.js' , array(), false, true );
        // AJAX
        wp_localize_script( 'shop', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
    }
    if( is_page('partenaires') ){
        wp_enqueue_script( 'partenaires', JS_DIR . 'partenaires.js' , array(), false, true );
        // AJAX
        wp_localize_script( 'partenaires', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
    }
    if( is_page('mon-compte') ){
        wp_enqueue_script( 'mon_compte', JS_DIR . 'mon_compte.js' , array(), false, true );
        // AJAX
        wp_localize_script( 'mon_compte', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
    }

    if( is_page('liste-biens') ){
        wp_enqueue_script( 'liste_bien', JS_DIR . 'liste_bien.js' , array(), false, true );
    }

}

/**
 * Traitement du formulaire
 */
//include INC_DIR . 'create_contact.php';
function action_wp_mail_failed($wp_error)
{
    return error_log(print_r($wp_error, true));
}
add_action('wp_mail_failed', 'action_wp_mail_failed', 10, 1);

// home ajax
include INC_DIR . 'home_ajax.php';

// Subscribe on Mailchimp
function subscribeMailchimp($email, $firstname, $lastname, $phone=null) {
    $authToken = 'b34520532d915734ec9225cc0ed5ef8c-us12';
    $list_id = '91799cd57b';
    
    $postData = array(
        "email_address" => $email, 
        "status" => "subscribed", 
        "merge_fields" => array(
            "PHONE" => $phone,
            'FNAME' => $firstname,
            'LNAME' => $lastname
        )
    );

    // Setup cURL
    $dataCenter = substr($authToken,strpos($authToken,'-')+1);

    $ch = curl_init('https://'.$dataCenter.'.api.mailchimp.com/3.0/lists/'.$list_id.'/members/');
    curl_setopt_array($ch, array(
        CURLOPT_POST => TRUE,
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_HTTPHEADER => array(
            'Authorization: apikey '.$authToken,
            'Content-Type: application/json'
        ),
        CURLOPT_POSTFIELDS => json_encode($postData)
    ));
    // Send the request
    $response = curl_exec($ch);
    $result = json_decode( $response);
}

// Estimation
include INC_DIR . 'estimation.php';

// Check Coupon
include INC_DIR . 'coupon.php';

// Database leads
include INC_DIR . 'db.php';

/**
 * Page d'option
 */
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page();
}

function procede_commande(){
    $new_order = wc_create_order();

    $product_to_add = array( '174' => 1, );

    $new_order->add_product( wc_get_product($product_to_add ));

    $gateways = WC()->payment_gateways->payment_gateways();
       $new_order->set_payment_method( $gateways['stripe'] );
        
       // Let WooCommerce calculate the totals
       $new_order->calculate_totals();
        
      // Update the status and add a note
       $new_order->update_status('completed', 'Order added programmatically!', true);
        
       // Save
       $new_order->save();

       return $new_order;
}

// Count coupon
function get_coupon_id(){
    $arg = array(
        'post_type'      => 'shop_coupon',
        'posts_per_page' => 1,
    );
    $id = 0;
    $q = new WP_Query( $arg );
    if( $q->have_posts() ):
        while( $q->have_posts() ): $q->the_post();
            $id = get_the_ID();
        endwhile;
    endif;
    wp_reset_postdata();
    return $id;
}

function count_coupon( $id ){
    return (int)get_field( 'nombre_dutilisation', $id );
}

function update_coupon_usage(){
    $id = get_coupon_id();
    $count_now = count_coupon( $id );
    $count_new = $count_now + 1;
    $update = update_field( 'nombre_dutilisation', $count_new, $id );

    return $update;
}

function get_coupon_use( $order_id ) {
    // Get $order object
    $order = wc_get_order( $order_id );
    $coupon_id = get_coupon_id();
    foreach( $order->get_coupon_codes() as $coupon_code ) {
        // Get the WC_Coupon object
        $coupon = new WC_Coupon( $coupon_code );

        // Get usage count
        $count = $coupon->get_usage_count();
        
        // OR use this instead, to get coupon usage limit.
        $limit = $coupon->get_usage_limit();
        
        // NOT empty
        if ( ! empty ( $count ) && ! empty ( $limit ) ) {
            // Calculate remaining
            $remaining = $limit - $count;
        
            // Output
            $update = update_coupon_usage();
        }
    }
}
add_action( 'woocommerce_thankyou', 'get_coupon_use', 10, 1 );

// add_action( 'woocommerce_thankyou', 'custom_woocommerce_auto_complete_order' );
function custom_woocommerce_auto_complete_order( $order_id ) { 
    if ( ! $order_id ) {
        return;
    }

    $order = wc_get_order( $order_id );
    // $order->update_status( 'completed' );

    add_filter( 'wp_mail_from', 'sender_email_lpdmb' );
    add_filter( 'wp_mail_from_name', 'sender_name_lpdmb' );

    $items = $order->get_items();
    foreach ( $items as $item ) {
        $product_id = $item->get_product_id();
    }
    //send facture

    if( $product_id == 176 || $product_id == 360 ){
        //send estimation
        $str = http_build_query( $_SESSION );
        parse_str($str, $Data);

        extract($Data);
        $prixvente = $envisage_vendre_bien;
        $ret = [];

        $destinataire = $email;

        $sender = get_field('sender','option');

        $subject = "Estimation de mon bien";    

        $atts = array();
        $atts = json_decode( $attachment );
        $influence = json_decode( $influence );
        // $str_pj = '';
        // if( is_array( $atts ) ):
          
        //   foreach( $atts as $k => $pj ){            
        //       $str_pj .= $pj;
        //       if( $k + 1 < count( $atts ) )
        //           $str_pj .= '|';
        //   }

        //   $str_pj = urlencode($str_pj);
        // endif;

        // require_once( get_template_directory().'/inc/pdf/index.php' );       
        // $str_pj = $str_pj .'|'.urlencode($pdf);
       
        $pour_client = true;
        ob_start();
            include 'inc/template_email/email_estimation.php';
        $body_mail = ob_get_clean();

        $headers = array("Reply-To : $sender", "Cc: $sender",'Content-Type: text/html; charset=UTF-8');


        if(@wp_mail( $destinataire, $subject, $body_mail, $headers, $atts )){
            // $attachments[] = $pdf; 
            // $headers[] = 'Cc: '.$sender;
            $pour_client = false;
            ob_start();
                include 'inc/template_email/email_estimation.php';
            $body_mail = ob_get_clean();
            @wp_mail( $sender, $subject, $body_mail, $headers, $atts );

            turn_lead_into_client( $email );
        }

    }elseif( $product_id == 364 || $product_id == 374 ){ 
      // action à venir
    }elseif( $product_id == ID_ABO || $product_id == ID_ABO_AN ){ 
      
    }else{
      // action à venir
    }

}

function filter_woocommerce_order_button_text( $btn ) { 
    $btn = 'Recevoir son rapport';
    return $btn; 
}; 

function filter_woocommerce_order_button_text_fiche( $btn ) { 
    $btn = 'Acheter le lead';
    return $btn; 
}; 
         
// add the filter 
// add_filter( 'woocommerce_order_button_text', 'filter_woocommerce_order_button_text', 10, 1 ); 
// add_filter( 'woocommerce_order_button_text', 'filter_woocommerce_order_button_text_fiche', 10, 1 ); 

add_filter( 'woocommerce_thankyou_order_received_text', 'misha_thank_you_title', 20, 2 );
 
function misha_thank_you_title( $thank_you_title, $order ){
 
    return 'Le paiement de votre demande d\'expertise a bien été reçu.';
 
}

add_filter( 'woocommerce_enable_order_notes_field', '__return_false' );

// add_action( 'template_redirect', 'define_default_payment_gateway' );
function define_default_payment_gateway(){
    if( is_checkout() && ! is_wc_endpoint_url() ) {
        // HERE define the default payment gateway ID
        $default_payment_id = 'stripe';

        WC()->session->set( 'chosen_payment_method', $default_payment_id );
    }
}

//Change checkout coupon notice position
function replace_checkout_default_scripts() {
 
/**
* Remove default woocommerce checkout scripts.
*/
wp_deregister_script( 'wc-checkout' );
wp_dequeue_script( 'wc-checkout' );
 
/**
* Add own modify scripts.
*/
wp_register_script( 'wc-checkout', get_stylesheet_directory_uri() . '/assets/js/checkout.js', array( 'jquery', 'woocommerce', 'wc-country-select', 'wc-address-i18n' ), WC_VERSION, true );
 
if( is_checkout() ) {
    wp_enqueue_script( 'wc-checkout' );
}
}
 
add_action( 'wp_enqueue_scripts', 'replace_checkout_default_scripts', 20 );

remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form');
add_action( 'woocommerce_after_checkout_form', 'woocommerce_checkout_coupon_form' );

function wc_billing_field_strings( $translated_text, $text, $domain ) {
    switch ( $translated_text ) {
        case 'Billing details' :
        case 'Détails de facturation' :
            $translated_text = "Adresse de facturation";
        break;
        case "Credit or debit card":
            $translated_text = "";
        break;
        case "Card Number":
            $translated_text = "N° de Carte";
        break;
        case "Complete the order with Payconiq app.":
            $translated_text = "Terminez la commande avec l’application Payconiq.";

    }
    return $translated_text;
}
add_filter( 'gettext', 'wc_billing_field_strings', 20, 3 );

add_filter( 'tiny_mce_before_init', 'scanwp_font_size' );
function scanwp_font_size( $initArray ){
    $initArray['fontsize_formats'] = "9px 10px 11px 12px 13px 14px 15px 16px 17px 18px 19px 20px";
    return $initArray;
  }

function scanwp_buttons( $buttons ) {
    
    array_unshift( $buttons, 'fontsizeselect' ); 
    return $buttons;
  }
add_filter( 'mce_buttons_2', 'scanwp_buttons' );

// Sous-total
add_action( 'woocommerce_calculate_totals', 'add_custom_price', 10, 1);
function add_custom_price( $cart_object ) {

    if ( is_admin() && ! defined( 'DOING_AJAX' ) )
        return;

    if ( did_action( 'woocommerce_calculate_totals' ) >= 2 )
        return;

    $cartcontents = $cart_object->cart_contents;
    foreach( $cartcontents as $cc ){
        $product_id = $cc['product_id'];
    }
    if( $product_id == 176 ){
        $cart_object->subtotal = 346;
    }
    
}

// Prix total
add_filter( 'woocommerce_calculated_total', 'change_calculated_total', 10, 2 );
function change_calculated_total( $total, $cart ) {
    global $woocommerce;
    $current_product_id = 0;
    $cart_contents = $cart->cart_contents;
    foreach( $cart_contents as $cont ){
        $current_product_id = $cont['product_id'];
    }
    if( $current_product_id == 176 ){
        $prix = 49;
        if ( $woocommerce->cart->get_applied_coupons() ){
            
            $prix = 0;
        }
        
    }else{
        $prix = $total;
    }
    return $prix;
}

add_action('woocommerce_calculate_totals', 'mysite_box_discount');
function mysite_box_discount($cart ){
    global $woocommerce;
    // $woocommerce->cart->discount_total = 346;
}

// Note kely eo ambany nom de produit
add_action('woocommerce_review_order_after_cart_contents','lpdmb_note_cart', 10 );
function lpdmb_note_cart(){ 
    foreach( WC()->cart->get_cart() as $cart_item ){
        $product_id = $cart_item['product_id'];
    }
    if( $product_id == 176 ) {
        $text = "+ En bonus : <br>\"Le guide des 10 astuces du vendeur\" offert ";
        if( isset( $_GET['pay'] ) && !empty( $_GET['pay'] ) && strip_tags( $_GET['pay']) == 'full' ){
            echo "<tr class='note_kely'><td>$text</td><td><span>97,00€</span></td></tr>";
        }else{
            echo "<tr class='note_kely'><td>$text</td><td style='text-align:right'><span class='texte_barre'>97,00€</span></td></tr>";
        }  
    }      
}

add_action('wp_ajax_buy_book', 'buy_book');
add_action('wp_ajax_nopriv_buy_book', 'buy_book');

function buy_book(){
    global $woocommerce;

    $woocommerce->cart->empty_cart();

    $woocommerce->cart->add_to_cart( 364, 1 );

    $ret['chekout'] = site_url( '/paiement' );        

    echo json_encode($ret);

    die();  
}

add_action('wp_ajax_buy_estimation', 'buy_estimation');
add_action('wp_ajax_nopriv_buy_estimation', 'buy_estimation');

function buy_estimation(){
    global $woocommerce;

    $woocommerce->cart->empty_cart();

    $_SESSION['mode'] = 'plein';

    $ret['home'] = site_url();        

    echo json_encode($ret);

    die();  
}

add_action('wp_ajax_buy_estimation_reduit', 'buy_estimation_reduit');
add_action('wp_ajax_nopriv_buy_estimation_reduit', 'buy_estimation_reduit');

function buy_estimation_reduit(){
    global $woocommerce;

    $woocommerce->cart->empty_cart();

    $_SESSION['mode'] = 'reduit';

    $ret['home'] = site_url();        

    echo json_encode($ret);

    die();  
}

function convert_month_to_fr( $month ){
    $month_en = array('January','February','March','April','May','June','July','August','September','October','November','December');
    $month_fr = array('janvier','février','mars','avril','mai','juin','juillet','août','septembre','octobre','novembre','décembre');

    return str_replace( $month_en, $month_fr, $month);
}

function convert_day_to_fr( $day ){
    $day_en = array("Monday",'Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday');
    $day_fr = array("lundi",'mardi','mercredi','jeudi','vendredi','samedi','dimanche');

    return str_replace( $day_en, $day_fr, $day);
}

// Function to change email address
function sender_email_lpdmb( $original_email_address ) {
    return get_field('sender','option');
}
function sender_email_wicoach( $original_email_address ) {
    return get_field('coach','option');
}
 
// Function to change sender name
function sender_name_lpdmb( $original_email_from ) {
    return 'Vendez mon bien';
}
function sender_name_wicoach( $original_email_from ) {
    return 'Wicoach';
}

add_action('woocommerce_review_order_after_order_total','phrase_sous_total');

function phrase_sous_total(){
    foreach( WC()->cart->get_cart() as $cart_item ){
        $product_id = $cart_item['product_id'];
    }
    if( $product_id == 176 ) {
        echo "<tr class='texte_sous_total'><td colspan='2'>
            100 % REMBOURSÉ si vous vendez via un des partenaires vendezmonbien.be*
        </td></tr>";
    }
}

if( function_exists('get_estates_list')) add_action( 'bl_cron_hook', 'get_estates_list' );

function run_activate_plugin( $plugin ) {
     $current = get_option( 'active_plugins' );
     $plugin = plugin_basename( trim( $plugin ) );

     if ( !in_array( $plugin, $current ) ) {
     $current[] = $plugin;
      sort( $current );
      do_action( 'activate_plugin', trim( $plugin ) );
      update_option( 'active_plugins', $current );
      do_action( 'activate_' . trim( $plugin ) );
      do_action( 'activated_plugin', trim( $plugin) );
     }

    return null;
  }

// // add_action('before_delete_post','before_delete_property');

// function before_delete_property( $postID, $post ){
//     if ( 'property' !== $post->post_type ) {
//         return;
//     }

//     require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
//     deactivate_plugins('whise-to-wp/whise-to-wp.php');

//     return;
// }

// // add_action('after_delete_post', 'after_delete_property');

// function after_delete_property($postID, $post ){
//     if ( 'property' !== $post->post_type ) {
//         return;
//     }

//     run_activate_plugin( 'whise-to-wp/whise-to-wp.php' );

//     return;
// }

function get_bought_products(){
  $current_user = wp_get_current_user();
  if ( 0 == $current_user->ID ) return false;
 
  // GET USER ORDERS (COMPLETED + PROCESSING)
  $customer_orders = get_posts( array(
      'numberposts' => -1,
      'meta_key'    => '_customer_user',
      'meta_value'  => $current_user->ID,
      'post_type'   => wc_get_order_types(),
      // 'post_status' => array_keys( wc_get_is_paid_statuses() ),
      'post_status' => array('wc-processing','wc-completed'),
  ) );

  // LOOP THROUGH ORDERS AND GET PRODUCT IDS
  if ( ! $customer_orders ) return false;
  $product_ids = array();
  foreach ( $customer_orders as $customer_order ) {
      $order = wc_get_order( $customer_order->ID );
      $items = $order->get_items();
      foreach ( $items as $item ) {
          $product_id = $item->get_product_id();
          $product_ids[] = $product_id;
      }
  }
  $product_ids = array_unique( $product_ids );

  return $product_ids;
}

function get_current_product_id(){
  $id = get_field('id');
  $name = get_field('nom_du_bien');
  $product_name =  $id .' - '. $name;
  $product = get_page_by_title( $product_name, OBJECT, 'product' );

  return $product->ID;
}

function get_current_product_fiche( $product_id ){
    
    $title = get_the_title( $product_id );
    
    $id_fiche = preg_split('/ (-|&#8211;) /', $title);
    $id_fiche = is_array( $id_fiche ) ? $id_fiche[0] : 0;

    $arg = array(
        'post_type' => 'property',
        'meta_key'  => 'id',
        'meta_value'=> $id_fiche,
    );

    $fiche = get_posts( $arg );
    if( $fiche ) return $fiche[0]->ID;
}

function authentication_needed() {
  $product_base          = array( 176, 360, 364, 374 );
  $product_abos          = array( ID_ABO, ID_ABO_AN );

  if( is_checkout() ){
    foreach( WC()->cart->get_cart() as $cart_item ){
      $product_id = $cart_item['product_id'];
    }

    if( in_array( $product_id, $product_base ) ){ 
        return false;
    }elseif( in_array( $product_id, $product_abos ) ){
        if( is_user_logged_in() ){
            return false;
        }else{
            return true;
        }        
    }else{
        if( is_user_logged_in() ){
            return false;
        }else{
            return true;
        }  
    }
  }

}
// add_action('template_redirect', 'authentication_needed');

/**
 Remove all possible fields
 **/
function wc_remove_checkout_fields( $fields ) {

    // Billing fields
    unset( $fields['billing']['billing_company'] );
    // unset( $fields['billing']['billing_email'] );
    unset( $fields['billing']['billing_phone'] );
    unset( $fields['billing']['billing_state'] );
    unset( $fields['billing']['billing_first_name'] );
    unset( $fields['billing']['billing_last_name'] );
    unset( $fields['billing']['billing_address_1'] );
    unset( $fields['billing']['billing_address_2'] );
    unset( $fields['billing']['billing_city'] );
    unset( $fields['billing']['billing_postcode'] );

    // Shipping fields
    unset( $fields['shipping']['shipping_company'] );
    unset( $fields['shipping']['shipping_phone'] );
    unset( $fields['shipping']['shipping_state'] );
    unset( $fields['shipping']['shipping_first_name'] );
    unset( $fields['shipping']['shipping_last_name'] );
    unset( $fields['shipping']['shipping_address_1'] );
    unset( $fields['shipping']['shipping_address_2'] );
    unset( $fields['shipping']['shipping_city'] );
    unset( $fields['shipping']['shipping_postcode'] );

    // Order fields
    unset( $fields['order']['order_comments'] );

    return $fields;
}

function wc_unrequire_wc_phone_field( $fields ) {
    $fields['billing_phone']['required'] = false;
    $fields['billing_company']['required'] = false;
    // unset( $fields['billing']['billing_email'] );
    $fields['billing_phone']['required'] = false;
    $fields['billing_state']['required'] = false;
   $fields['billing_first_name']['required'] = false;
    $fields['billing_last_name']['required']= false;
     $fields['billing_address_1']['required'] = false;
    $fields['billing_address_2']['required'] = false;
     $fields['billing_city']['required'] = false;
     $fields['billing_postcode']['required'] = false;

    return $fields;
}

function current_user_is_subscribed( $user_id = '' ){
    if( '' == $user_id && is_user_logged_in() ) 
        $user_id = get_current_user_id();
    // User not logged in we return false
    if( $user_id == 0 ) 
        return false;

    return wcs_user_has_subscription( $user_id, '', 'active' );
}

function auto_login_new_user( $username, $pass ) {
  $creds = array();
  $creds['user_login'] = $username;
  $creds['user_password'] = $pass;
    $creds['remember'] = true;
  
  $user = wp_signon( $creds, true );
}

function subscription_plug( $translated_text, $text, $domain ){

    if( $domain == 'subscriptions-for-woocommerce' ){
        if( $translated_text == 'ID' ){
            $translated_text = 'Commande';
        }
        if( $translated_text == 'Next payment date' ){
            $translated_text = 'Prélèvement';
        }
        if( $translated_text == 'Recurring Total' ){
            $translated_text = 'Récurrence';
        }
        if( $translated_text == 'Show' ){
            $translated_text = 'Arrêter';
        }
        if( $translated_text == ' / %s ' ){
            $translated_text = ' / mois ';
        }
        if( $translated_text == ' / month ' ){
            $translated_text = ' / mois ';
        }
        if( $translated_text == 'You have not any active subscriptions.' ){
            $translated_text = "Vous n'êtes souscrit à aucun abonnement actuellement.";
        }
        if( $translated_text == 'Subscription Cancelled Successfully' ){
            $translated_text = "Votre abonnement a bien été arrêté.";
        }
        if( $translated_text == 'Subscription Details' ){
            $translated_text = "Détail de l'abonnement";
        }
        if( $translated_text == 'Subscription Date' ){
            $translated_text = "Date de souscription";
        }
        if( $translated_text == 'Next payment' ){
            $translated_text = "Prochain prélèvement";
        }
    }
    
    return $translated_text;
}
add_filter( 'gettext', 'subscription_plug', 20, 3 );

if( !function_exists('mwb_sfw_cancel_url_') ){
    function mwb_sfw_cancel_url_( $mwb_subscription_id, $mwb_status ) {

    $mwb_link = add_query_arg(
    array(
        'mwb_subscription_id'        => $mwb_subscription_id,
        'mwb_subscription_status' => $mwb_status,
    )
    );
    $mwb_link = wp_nonce_url( $mwb_link, $mwb_subscription_id . $mwb_status );

    return $mwb_link;
    }
}

function get_all_partners(){
    $active_clients = array();
    $args1 = array(
     'role' => 'customer',
     'orderby' => 'id',
     'order' => 'ASC'
    );

    $clients = get_users($args1);

     foreach ($clients as $client) {
         if( current_user_is_subscribed( $client->ID ) ){
            $active_clients[] = $client->data;
         }
     }
     
    return $active_clients;
    
}


function adjust_subscription_price(){
    // $nbr = (int)get_post_meta( ID_ABO, 'total_sales', true ) + (int)get_post_meta( ID_ABO_AN, 'total_sales', true );
    $nbr = (int)get_post_meta( ID_ABO_AN, 'total_sales', true );

    if( $nbr > NBR_REDUCTION ){
        // update_post_meta( ID_ABO, '_price', PRIX_NORMAL);
        update_post_meta( ID_ABO_AN, '_price', PRIX_NORMAL_AN);
    }else{
        // update_post_meta( ID_ABO, '_price', PRIX_REDUIT);
        update_post_meta( ID_ABO_AN, '_price', PRIX_REDUIT_AN);
    }
        // reset_nbr_of_subscription();
}
add_action( 'init', 'adjust_subscription_price' );

function get_nbr_of_subscription(){
    $nbr = (int)get_post_meta( ID_ABO_AN, 'total_sales', true );
    return $nbr;
}

function is_TVA_format( $tva ){
    $pattern = '/^BE\d{4}\.\d{3}\.\d{3}/';
    if( preg_match($pattern, $tva)){
        return true;
    }else{
        return false;
    }
}

// reset the nbr of subsciption
// for admin use only

function reset_nbr_of_subscription(){
    update_post_meta( ID_ABO, 'total_sales', 0);
    update_post_meta( ID_ABO_AN, 'total_sales', 0);
}

add_action( 'woocommerce_save_account_details', 'save_custom_fields_account_details', 12, 1 );
function save_custom_fields_account_details( $user_id ) {

    if( isset( $_POST ) && !empty( $_POST ) ){
        $str = http_build_query($_POST);
        parse_str($str, $Data);

        extract($Data);

        if( isset( $_POST['invoice_first_name'] ) )
            update_user_meta( $user_id, 'billing_first_name', wp_strip_all_tags( $invoice_first_name ) );
        if( isset( $_POST['invoice_name'] ) )
            update_user_meta( $user_id, 'billing_last_name', wp_strip_all_tags( $invoice_name ) );
        if( isset( $_POST['invoice_agence_name'] ) )
            update_user_meta( $user_id, 'invoice_agence_name', wp_strip_all_tags( $invoice_agence_name ) );
        if( isset( $_POST['invoice_ipi_number'] ) )
            update_user_meta( $user_id, 'invoice_ipi_number', wp_strip_all_tags( $invoice_ipi_number ) );
        if( isset( $_POST['invoice_adresse'] ) )
            update_user_meta( $user_id, 'billing_address_1', wp_strip_all_tags( $invoice_adresse ) );
        if( isset( $_POST['invoice_postal_code'] ) )
            update_user_meta( $user_id, 'billing_postcode', wp_strip_all_tags( $invoice_postal_code ) );
        if( isset( $_POST['invoice_ville'] ) )
            update_user_meta( $user_id, 'billing_city', wp_strip_all_tags( $invoice_ville ) );
        if( isset( $_POST['invoice_tva_number'] ) )
            update_user_meta( $user_id, 'invoice_tva_number', wp_strip_all_tags( $invoice_tva_number ) );
    }
    // For Favorite color
    // if( isset( $_POST['favorite_color'] ) )
    //     update_user_meta( $user_id, 'favorite_color', sanitize_text_field( $_POST['favorite_color'] ) );
   
}


add_filter('woocommerce_cart_item_subtotal','change_unit', 8, 3);

function change_unit($p1, $p2, $p3){
    return str_replace( array('month','months','year','years','for','ss'), array('mois','moi','an','ans','pour','s'), $p1 );
}

add_filter ( 'woocommerce_account_menu_items', 'misha_one_more_link' );
function misha_one_more_link( $menu_links ){
    // we will hook "anyuniquetext123" later
    $new = array( 
        'mes-factures' => 'Mes factures',
        'mes-offres'   => 'Mes offres', 
        'les-biens'    => 'Biens disponibles', 
        // 'adresses' => 'Mes adresses de paiement', 
    );

    // or in case you need 2 links
    // $new = array( 'link1' => 'Link 1', 'link2' => 'Link 2' );

    // array_slice() is good when you want to add an element between the other ones
    $menu_links = $menu_links + $new;

    $menu_links = array(
        'dashboard'       => 'Tableau de bord',
        'orders'          => 'Mes fiches',
        'mes-offres'      => 'Mes offres',
        'les-biens'       => 'Biens disponibles',
        'edit-account'    => 'Détails du compte',
        'adresses'    => 'Mes adresses de facturation',
        'payment-methods' => 'Mes moyens de paiement',
        'subscriptions'   => 'Mon abonnement',
        'mes-factures'    => 'Mes factures',        
        'customer-logout' => 'Déconnexion',
        
    );

    return $menu_links; 
 
}

add_filter( 'woocommerce_get_endpoint_url', 'misha_hook_endpoint', 10, 4 );
function misha_hook_endpoint( $url, $endpoint, $value, $permalink ){
 
    if( $endpoint === 'mes-factures' ) {
        $url = site_url('mon-compte').'/mes-factures'; 
    }
    if( $endpoint === 'mes-offres' ) {
        $url = site_url('mon-compte').'/mes-offres'; 
    }
    if( $endpoint === 'les-biens' ) {
        $url = site_url('mon-compte').'/les-biens'; 
    }
    return $url;
 
}

add_action( 'init', 'my_account_new_endpoints' );

 function my_account_new_endpoints() {
    add_rewrite_endpoint( 'mes-factures', EP_ROOT | EP_PAGES );
    add_rewrite_endpoint( 'mes-offres', EP_ROOT | EP_PAGES );
    add_rewrite_endpoint( 'les-biens', EP_ROOT | EP_PAGES );
 }

add_action( 'woocommerce_account_mes-factures_endpoint', 'factures_endpoint_content' );
function factures_endpoint_content() {
    get_template_part('mes_factures');
}

add_action( 'woocommerce_account_mes-offres_endpoint', 'offres_endpoint_content' );
function offres_endpoint_content() {
    get_template_part('mes_offres');
}

add_action( 'woocommerce_account_les-biens_endpoint', 'biens_endpoint_content' );
function biens_endpoint_content() {
    get_template_part('les_biens');
}


add_action( 'wpo_wcpdf_after_billing_address', 'add_ipi_tva_invoice', 10, 2 );
function add_ipi_tva_invoice ($template_type, $order) {
    if ( $template_type == 'invoice' ) {
        $user_id = $order->get_user_id();

        $ipi = get_user_meta( $user_id, 'invoice_ipi_number', true );
        $tva = get_user_meta( $user_id, 'invoice_tva_number', true );
        $agn = get_user_meta( $user->ID, 'invoice_agence_name', true );

        if( $agn ) echo "<br />Agence : $agn";
        if( $ipi ) echo "<br />I.P.I. : $ipi";
        if( $tva ) echo "<br />N° TVA : $tva";
        
    }
}

add_shortcode('add_peb','add_peb_display');

function add_peb_display($args) {
  $html = '';
  $propID = (int)get_post_meta( $args['post_id'] , 'id_de_son_bien', true );
  
    
      $html .= '<div>
          <h3>Caractéristiques énergétiques</h3>
          <ul style="list-style: none;padding: 0;">';
      $peb_img = 'peb-a.png';
      if( get_field('niveau_peb', $propID) ) $peb_img = 'peb-'.get_field('niveau_peb', $propID).'.png';

      $html .= '<li><img src="'. IMG_DIR . $peb_img .'" alt="PEB"></li>
            <li>E spéc. : '. get_field('energie_specifique', $propID) .' kWh/m².an</li>
            <li>PEB No. : '. get_field('certficat_peb', $propID) .'</li>
            <li>Niveau PEB : '. get_field('niveau_peb', $propID) .'</li>
            <li>PEB Total : '. get_field('peb_total', $propID) .' Kwh/an</li>
            <li>CO2 : '. get_field('emission_co2', $propID) .' kg/m2/an</li>
          </ul>
           </div>';
    
        return $html;
}

function get_product_id_from_order(){
    global $woocommerce;
    
    $items = $woocommerce->cart->get_cart();

    foreach( $items as $item ){
        $product_id = $item['product_id'];
    }

    return $product_id;
}
add_action('woocommerce_checkout_process', 'ck_mis_field_vaovao');
function ck_mis_field_vaovao(){
    
    $product_id = get_product_id_from_order();

    if( $product_id == ID_ABO || $product_id == ID_ABO_AN || 'property' == get_post_type( get_current_product_fiche($product_id) ) ) {
        if (!$_POST['nom_agence'] || '' == $_POST['nom_agence'] ) wc_add_notice(__('Entrer le nom de l\'agence') , 'error');
        if (!$_POST['num_tva'] || '' == $_POST['num_tva'] ) wc_add_notice(__('Entrer le numero de TVA') , 'error');
        if (!$_POST['ipi_number'] || '' == $_POST['ipi_number'] ) wc_add_notice(__('Entrer le numero IPI') , 'error');
    }    
}

add_action('woocommerce_checkout_update_order_meta', 'update_ck_new_fields');
function update_ck_new_fields( $order_id ){

    $product_id = get_product_id_from_order();
    
    if( $product_id == ID_ABO || $product_id == ID_ABO_AN || 'property' == get_post_type( get_current_product_fiche($product_id) ) ) {

        if ($_POST['nom_agence'])
            update_post_meta($order_id, 'nom_agence',sanitize_text_field($_POST['nom_agence']));

        if ($_POST['num_tva'])
            update_post_meta($order_id, 'num_tva',sanitize_text_field($_POST['num_tva']));

        if ($_POST['ipi_number'])
            update_post_meta($order_id, 'ipi_number',sanitize_text_field($_POST['ipi_number']));
     } 
}

function millier( $prix ){
    return number_format( $prix, 0, ',','.');
}

add_filter( 'woocommerce_checkout_fields' , 'override_billing_checkout_fields', 20, 1 );
function override_billing_checkout_fields( $fields ) {
    $fields['billing']['billing_city']['placeholder'] = 'Ville';
    $fields['billing']['billing_postcode']['placeholder'] = 'Code postal';
    $fields['billing']['billing_phone']['placeholder'] = 'Téléphone';
    $fields['billing']['billing_email']['placeholder'] = 'Adresse email';
    $fields['billing']['billing_first_name']['placeholder'] = 'Prénom';
    $fields['billing']['billing_last_name']['placeholder'] = 'Nom';
    $fields['billing']['billing_state']['placeholder'] = 'Région/département';
    return $fields;
}

add_action('bl_cron_hook','send_email_offre_ultime');
add_action('bl_cron_hook','invite_proprio_a_valider_offre');

function send_email_offre_ultime(){ 
    $arg = array(
        'post_type' => 'property',
        'posts_per_page' => -1,
        'meta_key'  => 'est_actif',
        'meta_value'=> true,
    );

    $props = get_posts( $arg );

    if( is_array( $props ) ){

        $now = new DateTime();

        foreach( $props as $prop ){

            $propID = $prop->ID;

            $date_creation = get_field('field_creation',$propID ) ? get_field('field_creation',$propID ) : "01/01/2021 13:42";

            $date_creation_2 = new DateTime( $date_creation );
            $date_creation_2 = $date_creation_2->format('Y-m-d H:i:s');
            $date_creation_2 = new DateTime( $date_creation_2 );

            $_10j_47h_apres_creation = $date_creation_2->add( new DateInterval('P10DT47H') );

            if( $now->diff( $_10j_47h_apres_creation )->invert > 0 ){ 

                $ultime_sent = get_post_meta( $prop->ID,'ultime_sent',true ); 

                if( ! $ultime_sent || $ultime_sent == '' ){ 

                    // send ultime offre email
                    add_filter( 'wp_mail_from', 'sender_email_lpdmb' );
                    add_filter( 'wp_mail_from_name', 'sender_name_lpdmb' );

                    $reply = get_field('sender','option');
                    $destinataire = get_field('julie','option');

                    $headers = array("Reply-To: $reply","Cc: $reply",'Content-Type: text/html; charset=UTF-8');

                    $ref = get_field('reference', $prop->ID);
                    $obj = str_replace("{{REF}}", $ref, get_field('objet_offre_ultime','option'));

                    $product_id = get_product_id_of_property( $prop->ID );
                    $customers = get_all_partners_by_fiche( $product_id );

                    ob_start();
                        include INC_DIR . 'template_email/bien_ultime_offre.php';
                    $body_mail = ob_get_clean();

                    // @wp_mail( $destinataire, $obj, $body_mail, $headers );
                    // @wp_mail( get_field('email_proprietaire', $prop->ID ) , $obj, $body_mail, $headers );

                    @wp_mail( $customers, $obj, $body_mail, $headers );
                    
                    update_post_meta( $propID, 'ultime_sent', true );
                }
            }

        }
    }
}

function invite_proprio_a_valider_offre(){ 
    $arg = array(
        'post_type' => 'property',
        'posts_per_page' => -1,
        'meta_key'  => 'est_actif',
        'meta_value'=> true,
    );

    $props = get_posts( $arg );

    if( is_array( $props ) ){

        $now = new DateTime();

        foreach( $props as $prop ){

            $propID = $prop->ID;

            $date_creation = get_field('field_creation',$propID ) ? get_field('field_creation',$propID ) : "01/01/2021 13:42";

            $date_creation_3 = new DateTime( $date_creation );
            $date_creation_3 = $date_creation_3->format('Y-m-d H:i:s');
            $date_creation_3 = new DateTime( $date_creation_3 );

            $_12j_apres_creation     = $date_creation_3->add( new DateInterval('P12DT1M') );

            if( $now->diff( $_12j_apres_creation )->invert > 0 ){ 

                $invite_proprio_a_valider_offre = get_post_meta( $propID,'invite_proprio_a_valider_offre',true ); 

                if( ! $invite_proprio_a_valider_offre || $invite_proprio_a_valider_offre == '' ){ 

                    // send email invitation à valider une offre

                    add_filter( 'wp_mail_from', 'sender_email_lpdmb' );
                    add_filter( 'wp_mail_from_name', 'sender_name_lpdmb' );

                    $reply = get_field('sender','option');
                    $destinataire = get_field('julie','option');

                    $headers = array("Reply-To: $reply","Cc: $reply",'Content-Type: text/html; charset=UTF-8');

                    $ref = get_field('reference', $propID);
                    $obj_p = str_replace("{{REF}}", $ref, get_field('objet_validation_offre','option'));

                    $proprio_email = get_field( 'email_proprietaire', $propID );

                    ob_start();
                        include INC_DIR . 'template_email/invite_proprio_a_valider_offre.php';
                    $body_mail_p = ob_get_clean();

                    @wp_mail( $proprio_email , $obj_p, $body_mail_p, $headers );
                    
                    /* 48h après offre, mahazo mail ko zay rehetra nividy anle fiche */
                    $product_id = get_product_id_of_property( $propID );
                    $customers = get_all_partners_by_fiche( $product_id );

                    if( count( $customers) > 0 ){
                        $obj_p = str_replace("{{REF}}", $ref, get_field('objet_fin_offre','option'));

                        ob_start();
                            include INC_DIR . 'template_email/notif_acheteur_fin_des_offres.php';
                        $body_mail_p = ob_get_clean();

                        @wp_mail( $customers, $obj_p, $body_mail_p, $headers );
                    }
                    /* fin notif fin offres */

                    // update invite_proprio_a_valider_offre field to true
                    update_post_meta( $propID, 'invite_proprio_a_valider_offre', true );

                }
            }

        }
    }
}

function get_proprietaire_id_by_property_id( $propID ){
    $args = array(
        'post_type'      => 'proprietaire',
        'posts_per_page' => 1,
        'meta_key'       => 'id_de_son_bien',
        'meta_value'     => $propID,
    );

    $q = get_posts( $args );

    wp_reset_postdata();

    if( is_array( $q ) ){
        $q = $q[0];

        return $q->ID;
    }
}

function get_product_id_of_property( $propID ){
    
    $title = get_the_title( $propID ); 
    $ref = get_field('id', $propID );
  
    $id_product = preg_split('/ (-|&#8211;) /', $title); 
    $id_product = is_array( $id_product ) ? $id_product[0] : 0;

    $arg = array(
        'post_type' => 'product',
        'posts_per_page' => -1,
    );

    $products = get_posts( $arg );
    $out = 0;

    if( $products ){
        $title = $ref .' - '.$id_product;  
        foreach( $products as $product ){
            if( $product->post_title == $title ){
                $out = $product->ID;
            }else{
                continue;
            }
            
        }
    }
    return $out;
}

function get_all_partners_by_fiche( $product_id ){
    // Access WordPress database
    global $wpdb;
           
    // Find billing emails in the DB order table
    $statuses = array_map( 'esc_sql', wc_get_is_paid_statuses() );
    $customer_emails = $wpdb->get_col("
       SELECT DISTINCT pm.meta_value FROM {$wpdb->posts} AS p
       INNER JOIN {$wpdb->postmeta} AS pm ON p.ID = pm.post_id
       INNER JOIN {$wpdb->prefix}woocommerce_order_items AS i ON p.ID = i.order_id
       INNER JOIN {$wpdb->prefix}woocommerce_order_itemmeta AS im ON i.order_item_id = im.order_item_id
       WHERE p.post_status IN ( 'wc-" . implode( "','wc-", $statuses ) . "' )
       AND pm.meta_key IN ( '_billing_email' )
       AND im.meta_key IN ( '_product_id', '_variation_id' )
       AND im.meta_value = $product_id
    ");
     
    // Print array on screen
    return $customer_emails;
}

add_filter('login_errors','login_error_message');

function login_error_message($error){
    //check if that's the error you are looking for
    $pos = strpos($error, 'incorrect');
    if (is_int($pos)) {
        //its the right error so you can overwrite it
        $error = "Erreur: identifiant ou mot de passe erroné.";
    }
    return $error;
}

// add_action( 'wp_loaded','update_products_price');
function update_products_price(){

    $args = array(
        'post_type' => 'property',
        'post_status' => 'publish',
        'posts_per_page' => -1,
    );

    $prods = get_posts( $args );

    foreach( $prods as $property ){
        $id = get_field('id', $property->ID );
        $name = get_field('nom_du_bien', $property->ID);
        $product_name =  $id .' - '. $name;
        $product = get_page_by_title( $product_name, OBJECT, 'product' );
        
        $price = get_field('prix_de_vente', $property->ID );

        if( $price >= 200000 && $price <= 400000 ){
            $prod_price = 197;
        }elseif( $price >= 400001 && $price <= 800000 ){
            $prod_price = 297;
        }elseif( $price >= 800001 && $price <= 1000000 ){
            $prod_price = 397;
        }elseif( $price > 1000000 ){
            $prod_price = 497;
        }

        update_post_meta( $product->ID, '_price', $prod_price );

    }

}

add_filter( 'woocommerce_email_subject_customer_processing_order', 'change_processing_email_subject', 1, 2 );
add_filter( 'woocommerce_email_subject_customer_completed_order', 'change_processing_email_subject', 1, 2 );

function change_processing_email_subject( $subject, $order ) {

    $product_id = get_product_id_from_order();

    if( $product_id == ID_ABO 
        || $product_id == ID_ABO_AN || 
            'property' == get_post_type( get_current_product_fiche($product_id) )
    ){
        $subject = "Votre commande sur Vendez mon bien Partenaires a bien été reçue";
    }else{
        $subject = $subject;
    }                                                                                                                                     
    return $subject;
}

add_action( 'add_meta_boxes', 'add_featured_checkbox_function' );
function add_featured_checkbox_function() {
   add_meta_box('featured_checkbox_id','Réinitialiser les metas', 'featured_checkbox_callback_function', 'property', 'normal', 'high');
}
function featured_checkbox_callback_function( $post ) {
   global $post;
?>
   <p><input type="checkbox" name="invite_proprio_a_valider_offre" value="yes"/> Reset Email validation offre par proprio</p>
   <p><input type="checkbox" name="ultime_sent" value="yes" /> Reset Email offre ultime</p>
   <p><input type="checkbox" name="fiche_cloturee" value="yes" /> Reset Fiche clôturée</p>
   <p><input type="checkbox" name="buy_now" value="yes" /> Reset Buy Now</p>
<?php
}

add_action('save_post', 'save_featured_post'); 
function save_featured_post($post_id){ 
    if( $_POST['invite_proprio_a_valider_offre'] ){
        update_post_meta( $post_id, 'invite_proprio_a_valider_offre', false);
    }

    if( $_POST['ultime_sent'] ){
        update_post_meta( $post_id, 'ultime_sent', false);
    }

    if( $_POST['fiche_cloturee'] ){
        update_post_meta( $post_id, 'fiche_cloturee', false);
    }
    if( $_POST['buy_now'] ){
        update_post_meta( $post_id, 'was_bought_now', false);
    }
}

function disable_plugin_updates( $value ) {
  if ( isset($value) && is_object($value) ) {
    if ( isset( $value->response['woocommerce-multiple-customer-addresses/woocommerce-multiple-customer-addresses.php'] ) ) {
      unset( $value->response['woocommerce-multiple-customer-addresses/woocommerce-multiple-customer-addresses.php'] );
    }
  }
  if ( isset($value) && is_object($value) ) {
    if ( isset( $value->response['login-and-registration/login-and-register.php'] ) ) {
      unset( $value->response['login-and-registration/login-and-register.php'] );
    }
  }
  return $value;
}
add_filter( 'site_transient_update_plugins', 'disable_plugin_updates' );

function liste_biens( $statut_transaction = '' ){
    ob_start();
    include INC_DIR . 'liste_biens.php';
    $out = ob_get_clean();

  return $out;
}

//  Custom pagination function 
    
function cq_pagination($pages = '', $range = 3){
        
           ob_start();
            include INC_DIR . 'h_pagination.php';
            $out = ob_get_clean();

            echo $out;
        
}

add_shortcode('objet_nouveau_bien','objet_nouveau_bien');
function objet_nouveau_bien(){
    $html = get_field('objet_nouveau_bien','option');
    return $html;
}

add_shortcode('titre_nouveau_bien','titre_nouveau_bien_fn');
function titre_nouveau_bien_fn(){
    $html = get_field('titre_nouveau_bien','option');
    return $html; 
}

add_shortcode('contenu_nouveau_bien','contenu_nouveau_bien_fn');
function contenu_nouveau_bien_fn(){
    $html = get_field('contenu_nouveau_bien','option');
    return $html; 
}

add_shortcode('objet_nouveau_bien_bn','objet_nouveau_bien_bn_fn');
function objet_nouveau_bien_bn_fn(){
    $html = get_field('objet_nouveau_bien_bn','option');
    return $html; 
}

add_shortcode('titre_nouveau_bien_bn','titre_nouveau_bien_bn_fn');
function titre_nouveau_bien_bn_fn(){
    $html = get_field('titre_nouveau_bien_bn','option');
    return $html; 
}

add_shortcode('contenu_nouveau_bien_bn','contenu_nouveau_bien_bn_fn');
function contenu_nouveau_bien_bn_fn(){
    $html = get_field('contenu_nouveau_bien_bn','option');
    return $html; 
}