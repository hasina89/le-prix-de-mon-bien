<div class="barre_top">
    <div class="blcTop fixed">
        <div class="container headParteneire">
            <!-- <a href="<?= site_url(); ?>" class="logo logo-fiche wow fadeInUp">
                <img src="<?= IMG_DIR ?>vendezmonbien-pos.svg" alt="">
            </a> -->
            <div class="row">
                <div class="col top-partenaire">
                    <?php if( 'property' == get_post_type( get_the_ID() ) ){ ?>
                      <a href="<?= site_url('partenaires'); ?>" class="logo logo_partenaire wow fadeInUp">
                          <img src="<?= IMG_DIR ?>logo-partenaire.svg" alt="">
                      </a>
                      <div class="blocLogin">
                          <?php if( is_user_logged_in() ): 
                              $current_user = wp_get_current_user();
                          ?>
                              <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" class="btn-connect lrm-show-if-logged-in"><?= $current_user->first_name.' '.$current_user->last_name ?></a>
                          <?php endif; ?>
                      </div>
                    <?php }else{ ?>
                      <a href="<?= site_url(); ?>" class="logo logo_partenaire wow fadeInUp">
                          <img src="<?= IMG_DIR ?>vendezmonbien-pos.svg" alt="">
                      </a>
                      <?php
                        if( $now->diff( $date_creation_0 )->invert > 0 && $now->diff($_10j_apres_creation)->invert == 0 ){
                            $cta_pro = 'Voir les visites programmées';
                            $href_pro = '#plan_rdv';
                        }elseif( $now->diff( $_10j_apres_creation )->invert > 0 && $now->diff($_12j_apres_creation)->invert == 0 ){
                            $cta_pro = 'Voir les offres en cours';
                            $href_pro = '#faire_offre';
                        }elseif( $now->diff($_12j_apres_creation)->invert > 0 ){
                            $cta_pro = 'Valider une offre';
                            $href_pro = '#plan_rdv';
                        }
                      ?>
                      <a href="<?= $href_pro ?>" class="btn scroll"><?= $cta_pro ?></a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>