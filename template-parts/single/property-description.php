<section class="blocDescri">
    <div class="imgDescri" id="slideImgDescri">
        <?php 
            if( is_array( $imgs ) ): 
                foreach( $imgs as $img ):
        ?>
            <div class="item_img" style="background: url(<?= $img['image_large'] ?>);"></div>
        <?php endforeach; endif; ?>
        <?php if( get_field('video_vv', $propID ) ){ ?>
            <iframe src="<?= get_field('video_vv', $propID ) ?>"></iframe>
        <?php } ?>
        <?php if( get_field('video_3d', $propID ) ){ ?>
            <iframe src="<?= get_field('video_3d', $propID ) ?>"></iframe>
        <?php } ?>
    </div>
    <div class="ctDescri">
        <div class="introDescri">
            <div class="itemDescri">
                <div class="itemIntro">
                    <?php if( 'property' == get_post_type( get_the_ID() ) ): ?>
                        <img src="<?= IMG_DIR ?>icone-bed.svg" alt="">
                    <?php else: ?>
                        <img src="<?= IMG_DIR ?>icone-bed_gold.svg" alt="">
                    <?php endif; ?>
                    <div>
                        <strong><?= get_field('titre_chambre','option') ?></strong>
                        <span><?= get_field('field_60e44a6bf62db', $propID) .' '. get_field('texte_chambre','option')?></span>
                    </div>
                </div>
                <div class="itemIntro">
                    <?php if( 'property' == get_post_type( get_the_ID() ) ): ?>
                        <img src="<?= IMG_DIR ?>icone-resize.svg" alt="">
                    <?php else: ?>
                        <img src="<?= IMG_DIR ?>icone-resize_gold.svg" alt="">
                    <?php endif; ?>
                    <div>
                        <strong><?= get_field('titre_superficie','option') ?></strong>
                        <span>
                            <?php 
                              if( get_field('field_60e44887d4dee', $propID) )
                                echo get_field('field_60e44887d4dee', $propID) .' '. get_field('texte_habitable','option');
                              if( get_field('field_60e44899d4def', $propID) )
                                echo ' - ' . get_field('field_60e44899d4def', $propID) .' '. get_field('superficie_totale','option');
                            ?> 
                        </span>
                    </div>
                </div>
            </div>
            <div class="blocBtnDetail">
                <!-- <a href="tel:<?= get_field('appel_copie','option') ?>" class="btn btn-tel"><b><?= get_field('appel','option') ?></b><?= get_field('appel_copie','option') ?></a> -->
                <?php if( 'property' == get_post_type( get_the_ID() ) ): 
                    if( in_array( $fiche_id, $boughts ) && current_user_is_subscribed() ){
                        /*
                        A la création de la fiche: offre et visite indisponible
                        48h après la création de la fiche: visite disponible, offre indisponible (flou et opacité 50%)
                        Pendant 10j, visite disponible
                        Après ces 10j, visite désactivée et offre disponible pour 48h
                        Si aucune offre, email pour offre ultime à la 47è H
                        après les 48h, offre et visite: bouton désactivés
                        */ 
                        $now = new DateTime();

                        $_48h_apres_creation__ = new DateTime( get_field('date_de_fin') );
                        $_48h_apres_creation__ = $_48h_apres_creation__->format('Y-m-d H:i:s');

                        $_48h_apres_creation   = new DateTime( $_48h_apres_creation__ );
                        $_48h_apres_creation_0 = new DateTime( $_48h_apres_creation__ );
                        $_48h_apres_creation_1 = new DateTime( $_48h_apres_creation__ );
                        $_48h_apres_creation_2 = new DateTime( $_48h_apres_creation__ );

                        $_10j_apres_48h = $_48h_apres_creation_0->add( new DateInterval('P10D') );

                        $_47h_apres_10j = $_48h_apres_creation_1->add( new DateInterval('P10DT47H') );

                        $_48h_apres_10j = $_48h_apres_creation_2->add( new DateInterval('P10DT48H') );

                        if( $now->diff( $_48h_apres_creation )->invert > 0 && $now->diff($_10j_apres_48h)->invert == 0 ){
                            echo '<a href="#plan_rdv" class="btn scroll">'. get_field('visite_btn','option') .'</a>';
                        }

                        if( $now->diff( $_10j_apres_48h )->invert > 0 && $now->diff($_48h_apres_10j)->invert == 0 ){
                            echo '<a href="#faire_offre" class="btn scroll">'. get_field('offre_btn','option') .'</a>';
                        }
                    }

                endif; ?>
            </div>
        </div>
        <div class="descri">
            <div>
                <div class="textDescri">
                    <?= get_field('field_60e444aaf9629', $propID) ?>
                    <?= get_field('field_60e444c4f962a', $propID) ?>
                </div>
                <span class="lire_la_suite"><?= get_field('suite__','option') ?></span>
            </div>
        </div>
    </div>
</section>