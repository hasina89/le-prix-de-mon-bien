<?php
    /*
    A la création de la fiche: offre et visite indisponible
    48h après la création de la fiche: visite disponible, offre indisponible (flou et opacité 50%)
    Pendant 10j, visite disponible
    Après ces 10j, visite désactivée et offre disponible pour 48h
    Si aucune offre, email pour offre ultime à la 47è H
    après les 48h, offre et visite: bouton désactivés
    */ 
    $now = new DateTime();

    $date_creation = get_field('field_creation' ) ? get_field('field_creation' ) : "01/01/2021 13:42";

    $date_creation_1 = new DateTime( $date_creation );
    $date_creation_1 = $date_creation_1->format('Y-m-d H:i:s');
    $date_creation_1 = new DateTime( $date_creation_1 );

    $date_creation_2 = new DateTime( $date_creation );
    $date_creation_2 = $date_creation_2->format('Y-m-d H:i:s');
    $date_creation_2 = new DateTime( $date_creation_2 );

    $date_creation_3 = new DateTime( $date_creation );
    $date_creation_3 = $date_creation_3->format('Y-m-d H:i:s');
    $date_creation_3 = new DateTime( $date_creation_3 );

    $date_creation_0         = new DateTime( $date_creation );
    $date_creation_0 = $date_creation_0->format('Y-m-d H:i:s');
    $date_creation_0 = new DateTime( $date_creation_0 );

    $_10j_apres_creation     = $date_creation_1->add( new DateInterval('P10D') );
    $_10j_47h_apres_creation = $date_creation_2->add( new DateInterval('P10DT47H') );
    $_12j_apres_creation     = $date_creation_3->add( new DateInterval('P12D') );

    $offres = loop_offres( $propID );
?>
<section class="plan_visite<?= !$offres || '' == $offres ? ' renin_i_sans_offre':'' ?>">
    <div class="container">
        <div class="col_left" id="plan_rdv">
            <div class="content">
               <?php
                    if( 'property' == get_post_type( get_the_ID() ) ) {
                        if( $now->diff( $date_creation_0 )->invert > 0 ){
                            if( $now->diff($_10j_apres_creation)->invert == 0 ){
                                $hide_button_rdv = false; 
                                // echo 'miseo ny visite, dia afaka miplanifié visite';
                            }else{
                                $hide_button_rdv = true; 
                                // echo 'miseo ny visite, fa tsy afaka miplanifié visite';
                            }

                            require_once 'property-visite.php'; 

                        }else{
                            // echo 'flou ny visite, tsy mahazo miplanifié';
                            require_once 'property-fake-rdv.php';
                        }
                    }else{
                        $hide_button_rdv = true; 
                        require_once 'property-visite.php';
                    }
                    
                ?>
            </div>
        </div>
        <div class="col_right" id="faire_offre">
            <div class="content">
                <?php 
                    if( 'property' == get_post_type( get_the_ID() ) ) {
                        if( $now->diff( $_10j_apres_creation )->invert > 0 ){
                            if( $now->diff( $_12j_apres_creation )->invert == 0 ){
                                $hide_button_offre = false;
                                // echo 'miseo ny offre, ary afaka manao offre';
                            }else{
                                $hide_button_offre = true;
                                 // echo 'miseo ny offre, fa tsy afaka manao offre';
                            }
                            require_once 'property-offre.php';

                        }else{
                            // echo 'flou ny visite, tsy mahazo manao offre';
                            require_once 'property-fake-offre.php';
                        }
                    }else{
                        require_once 'property-offre.php';
                    }
                    
                    
                ?>
            </div>
        </div>
    </div>
</section>