<div class="blcOffre">
    <div class="blcTitreOffre">
        <span class="bouton_close"></span>
        <?php
            $cta_txt = get_field('offre_btn_primo','option');
            if( is_array( $offres ) ) $cta_txt = get_field('offre_btn','option');

            $user = wp_get_current_user(); 
            $fn = $user->user_firstname;
            $ln = $user->user_lastname;
            $i_a_n = get_user_meta( $user->ID, 'invoice_agence_name', true );

        ?>
        <a class="btn" id="cta_btn_offre"><?= $cta_txt ?></a>
    </div>
    <div class="blc_chp_offre">
        <form id="form_offre" class="form_offre">
            <div class="blc_chp">
                <div class="chp w_100">
                    <div class="champ">
                        <input type="text"  class="form-control" placeholder="Nom de l'agence" value="<?= $fn .' '.$ln .' | '.$i_a_n ?>" name="agence_text" readonly>
                    </div>
                </div>
            </div>
           <!--  <div class="blc_chp">
                <div class="chp w_40 chp_date">
                    <div class="champ">
                        <input type="text"  class="form-control" id="date_offre" placeholder="Date" value="" name="date">
                    </div>
                </div>
                <div class="chp w_60">
                    <div class="champ">
                        <input type="text"  class="form-control" placeholder="délai de validité" value="" name="validite" id="validite">
                    </div>
                </div>
            </div> -->
            <div class="blc_chp">
                <div class="chp w_100">
                    <div class="champ">
                        <input type="text"  class="form-control" placeholder="Montant de l'offre" value="" name="prix">
                    </div>
                </div>
            </div>
            <div class="blc_chp blc_chp_radio">
                <div class="label_radio">Condition suspensive :</div>
                <div class="input_radio">
                    <input type="radio" id="condition_suspensive_yes" class="form-control"  value="oui" name="condition_suspensive">
                    <label for="condition_suspensive_yes">Oui</label>
                </div>
                <div class="input_radio">
                    <input type="radio" id="condition_suspensive_no"  class="form-control"  value="Non" name="condition_suspensive" checked>
                    <label for="condition_suspensive_no">Non</label>
                </div>
            </div>
            <div class="blc_chp blc_chp_radio blc_chp_radio_acompte">
                <div class="label_radio">Paiement d'un acompte :</div>
                <div class="input_radio">
                    <input type="radio" id="paiement_acompte_oui" class="form-control"  value="oui" name="paiement_accompte">
                    <label for="paiement_acompte_oui">Oui</label>
                </div>
                <div class="input_radio">
                    <input type="radio" id="paiement_acompte_non"  class="form-control"  value="Non" name="paiement_accompte" checked>
                    <label for="paiement_acompte_non">Non</label>
                </div>
            </div>
             <div class="blc_chp blc_chp_radio blc_chp_radio_taux">
                <div class="input_radio">
                    <input type="radio" id="taux_acompte_5" class="form-control"  value="5%" name="taux_accompte" checked>
                    <label for="taux_acompte_5">5%</label>
                </div>
                <div class="input_radio">
                    <input type="radio" id="taux_acompte_10"  class="form-control"  value="10%" name="taux_accompte">
                    <label for="taux_acompte_10">10%</label>
                </div>
            </div>
             <div class="blc_chp blc_chp_file">
                <div class="chp w_50 chp_carte">
                    <!-- <div class="label_radio">Ajouter ma carte d'identité</div>
                    <input type="file"  class="form-control" value="" name="numero carte"> -->
                    <div class="cont-file">
                        <div class="content_cont_file">
                            <input type="file" name="file[]" class="input-file input_file">
                            <div class="listBoutton">
                                <span style="display: none;"></span>
                                <i class="txtBroswer"> Ajouter ma carte d'identité</i>
                                <i class="reset" style="display: none">Supprimer</i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="chp w_50 chp_carte">
                    <!-- <div class="label_radio">Ajouter ma carte d'identité</div>
                    <input type="file"  class="form-control" value="" name="numero carte"> -->
                    <div class="cont-file">
                        <div class="content_cont_file">
                            <input type="file" name="file_offre[]" class="input-file input_file_offre">
                            <div class="listBoutton">
                                <span style="display: none;"></span>
                                <i class="txtBroswer"> Déposer votre document d’offre </i>
                                <i class="reset" style="display: none">Supprimer</i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <input type="hidden" name="propID" value="<?= get_the_ID() ?>">
            <input type="hidden" name="agence"  value="<?= $fn .' '.$ln .' | '.$i_a_n ?>" readonly>
            <input type="hidden" name="email_agence"  value="<?= $current_user->data->user_email ?>" readonly>
            <input type="submit" name="submit_abus" value="Envoyer mon offre" class="submit">
        </form>
    </div>
</div>