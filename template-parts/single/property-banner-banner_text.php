<div class="bannerText">
      <div class="text">
          <span><?= get_field('ref','option') ?> : <?= get_field('field_60e443276c38d', $propID) ?></span>
          <p><?= get_field('ville','option') ?> : <?= get_field('field_60e442f16c38a',$propID) ?></p>
          <p><?= get_field('pdv','option') ?> : <?php if( get_field('field_60e442f86c38b',$propID) ) echo millier( get_field('field_60e442f86c38b',$propID) ) ?> €</p>
          <div class="blcAcces">
          <?php 

            $was_bought_now = get_post_meta( $propID,'was_bought_now',true );

            $fiche_cloturee = get_post_meta( $propID, 'fiche_cloturee', true );

            $btn_special = false;

            if( 'property' == get_post_type( get_the_ID() ) ){ 

              if( !$fiche_cloturee || '' == $fiche_cloturee ){
                $show_cta = true;

                if( in_array( $fiche_id, $boughts ) && current_user_is_subscribed() ){
                  
                  if( $now->diff( $date_creation_0 )->invert > 0 && $now->diff($_10j_apres_creation)->invert == 0 ){
                      $nbr_visite = do_shortcode('[online_reservation ref='. get_field('field_60e443276c38d', $propID) .' count=1]');
                      $nbr_visite_restant = 5 - (int)$nbr_visite;
                      $cta = str_replace('%s', '<span id="nbr_visite_restant" class="textNbvisite">'.$nbr_visite_restant.'</span>', get_field('btn_abon_ach_visite','option')); //bouton visite
                      $href = '#plan_rdv';
                  }elseif( $now->diff( $_10j_apres_creation )->invert > 0 && $now->diff($_10j_47H_apres_creation)->invert == 0 ){
                      $cta = get_field('btn_abon_ach_offre','option'); //bouton offre
                      $href = '#faire_offre';
                  }elseif( $now->diff($_10j_47H_apres_creation)->invert > 0 && $now->diff($_12j_apres_creation)->invert == 0 ){
                      $cta = get_field('btn_abon_ach_offre_ultime','option') ? get_field('btn_abon_ach_offre_ultime','option'):'Déposer l\'ultime offre' ; //bouton offre ultime
                      $href = '#faire_offre';
                  }elseif( $now->diff($_12j_apres_creation)->invert > 0 ){
                      $cta = get_field('btn_abon_ach_fin_offre_visite','option'); //bouton fin offre et visite
                      $href = '#faire_offre';
                      $btn_special = true;
                  }
                  
                }elseif( !in_array( $fiche_id, $boughts ) && current_user_is_subscribed() ){
                  if( $total_sales > 0 ){
                    $cta = get_field('btn_abon_non_ach','option');
                    $href = '#slideImgDescri';
                  }else{
                    $cta = get_field('btn_abon_non_ach_no_acces_restant','option') ? get_field('btn_abon_non_ach_no_acces_restant','option') : 'Tous les accès ont été acheté' ;
                    $href = '#';
                  }
                  
                }else{
                  $cta = get_field('btn_non_abon','option');
                  $href = '#popup_achat_lead';
                }
            ?>
              <?php
                // if( $total_sales > 0 ){
                    if( $diff->invert == 0 ): 
                      $minutes = ($diff->days * 24 * 60) + ($diff->h * 60) + $diff->i + ( $diff->s / 60);
                  ?>
                      <div class="left">
                                      
                        <div class="titre_acces"><?= get_field('text_chrono','option') ?></div>
                        <ul class="countdown">
                            <li>
                                <div class="temps" data-minutes-left="<?= $minutes ?>"></div>
                            </li>
                        </ul>                 
                      </div>
                    <?php endif; ?>
                    <div class="<?php echo $diff->invert == 0 ? 'right':'center' ?>">
                        <?php if( $was_bought_now == '' || !$was_bought_now){ ?>
                          <?php if( $now->diff( $_24H_apres_creation )->invert > 0 ){ ?>
                            <div class="titre_acces"><?= get_field('vip','option') ?></div>
                          
                            <div class="rest_vip">
                              <?php 
                                if( $sales > 5 ) $sales = 5;
                                echo $sales .'/5'
                                ?>
                            </div>
                          <?php } 
                        } ?>
                        <div class="titre_acces soratra_vaovao">
                          <?php 
                            if( !in_array( $fiche_id, $boughts ) ){  

                              if( $now->diff( $_24H_apres_creation )->invert > 0 ){
                                echo str_replace('%', $total_sales, get_field( 'agence_restante','option') );
                              }else{
                                echo "Soyez le seul partenaire à offrir ce bien à vos clients";
                              }

                            }else{

                              if( $was_bought_now == '' || !$was_bought_now){
                                echo get_field('texte_apres_achat_normal','option');
                              }else{
                                echo get_field('texte_apres_achat_buynow','option');
                              }
                            } 
                          ?>
                        </div>
                    </div>
            <?php 
                // }
              } else {
                $show_cta = false;
            ?>
                <div class="d-flex justify-content-center">
                    <div class="titre_acces bien_cloture">
                      <?php echo get_field('text_bien_cloture','option') ? get_field('text_bien_cloture','option') : 'Bien clôturé' ?>
                    </div>
                </div>
          <?php 
              } 
          }elseif( 'proprietaire' == get_post_type( get_the_ID() ) ){ 
                  if( $fiche_cloturee ){
          ?>
                  <div class="d-flex justify-content-center">
                    <div class="titre_acces bien_cloture">
                      <?php echo get_field('text_bien_cloture','option') ? get_field('text_bien_cloture','option') : 'Bien clôturé' ?>
                    </div>
                </div>
          <?php 
                  }
          } 
          ?>
          </div>
           <?php 
             if( true === $show_cta ):
          ?>  
              <div class="list_btn">
                <?php if( !$btn_special ): ?>
                  <span class="sipan">
                    <a href="<?= $href ?>" class="btn scroll"><?= $cta ?></a>
                  </span>
                <?php else: ?>
                    <!-- Ato zan ilay bouton @tache 491 io -->
                    <span class="sipan sipan_fin_offre">
                      <span class="btn"><?= $cta ?></span>
                    </span>
                    <span class="sipan">
                      <a href="<?= $href ?>" class="btn scroll">Consulter les offres déposées</a>
                    </span>
                <?php endif; ?>
            <?php
                if( $now->diff( $_24H_apres_creation )->invert == 0 ){ 
                  $url = wp_nonce_url( get_permalink($propID), "nonce_buy_now", '_sc' );
            ?>
                 <span class="sipan sipan_buy_now">
                    <a href="<?= $url ?>#submit-buy" class="btn buy_now scroll">Buy now</a>
                 </span>
              <?php } ?>
             </div>
            <?php endif; ?>
      </div>
  </div>