<?php
    $id = (int)get_field('id', $propID);
    $estate = get_estate_details( $id );
    $details = $estate->details;
?>
<section class="blocSpec">
    <div class="container">
        <div class="slideBien">
            <?php if( is_array( $imgs ) ): 
                foreach( $imgs as $img ):
            ?>
            <div class="item"><a href="<?= $img['image_large'] ?>" class="galerie_detail"  data-fancybox="groupe1" rel="1"><img src="<?= $img['image_large'] ?>" alt=""></a></div>
            <?php endforeach; endif; ?>
            <?php if( get_field('video_vv', $propID ) ){ ?>
                <div class="item"><div class="content_video"><iframe src="<?= get_field('video_vv', $propID ) ?>"></iframe></div></div>
            <?php } ?>
            <?php if( get_field('video_3d', $propID ) ){ ?>
                <div class="item"><div class="content_video"><iframe src="<?= get_field('video_3d', $propID ) ?>"></iframe></div></div>
            <?php } ?>
        </div>
        <div class="spec">
            <div class="content">
                <div class="titre_spec">
                    <h2><?= get_field('ttire_detail_bein','option') ?></h2>
                    
                </div>

                <div>
                    <h3><?= get_field('det_int','option') ?></h3>
                    <ul>
                        <?php
                        if( $estate->area ) echo "<li>Superficie habitable : ".$estate->area." m²</li>";
                        if( $estate->rooms ) echo "<li>Nombre de pièces : ".$estate->rooms."</li>";
                        if( $estate->bathRooms ) echo "<li>Salle de bain : ".$estate->bathRooms."</li>";
                        if( search_details_by_id( $details, 55 ) ){
                            $value = search_details_by_id( $details, 55 );
                            echo "<li>Toilettes : ".$estate->bathRooms."</li>";
                        }
                        if( $estate->rooms ) echo "<li>Chambres : ".$estate->rooms."</li>";
                        if( search_details_by_id( $details, 1009 ) ){
                            $value = search_details_by_id( $details, 1009 );
                            echo "<li>Salle à manger : ".$value." m²</li>";
                        }
                        if( search_details_by_id( $details, 1595 ) ){
                            $value = search_details_by_id( $details, 1595 );
                            echo "<li>Cuisine : ".$value."</li>";
                        }
                        if( search_details_by_id( $details, 39 ) ){
                            $value = search_details_by_id( $details, 39 );
                            $value = $value == '1' ? "oui" : "non";
                            echo "<li>Buanderie : ".$value."</li>";
                        }
                        if( search_details_by_id( $details, 2101 ) ){
                            $value = search_details_by_id( $value, 2101 );
                            echo "<li>Garage : ".$value."</li>";
                        }
                        ?>
                    </ul>
                </div>
                <div>
                    <h3><?= get_field('det_ext','option') ?></h3>
                    <ul>
                        <?php
                            if( $estate->groundArea ) echo "<li>Superficie terrain : ".$estate->groundArea." m²</li>";
                            if( get_field('field_60e4461daa23b', $propID) ) echo "<li>Etat : ".get_field('field_60e4461daa23b', $propID)."</li>";
                            if( get_field('field_60e448a7d4df0', $propID) ) {
                                $jardin = ( "1" == get_field('field_60e448a7d4df0', $propID) ) ? "oui":"non";
                                echo "<li>Jardin : ".$jardin."</li>";
                            }
                            if( search_details_by_id( $details, 1005 ) ){
                                $value = search_details_by_id( $details, 1005 );
                                echo "<li>Terasse : ".$value."</li>";
                            }
                            if( search_details_by_id( $details, 322 ) ){
                                $value = search_details_by_id( $details, 322 );
                                $value = "1" == $value ? 'oui':'non';
                                echo "<li>Piscine : ".$value."</li>";
                            }
                            if( search_details_by_id( $details, 23 ) ){
                                $value = search_details_by_id( $details, 23 );
                                echo "<li>Orientation : ".$value."</li>";
                            }
                            if( get_field('field_60e448d8d4df2', $propID) ) {
                                $value = ( "1" == get_field('field_60e448d8d4df2', $propID) ) ? "oui":"non";
                                echo "<li>Parking : ".$value."</li>";
                            }
                            if( $estate->fronts ) echo "<li>Façade : ".$estate->fronts."</li>";
                        ?>
                    </ul>
                </div>
                <div>
                    <h3><?= get_field('caract_ener','option') ?></h3>
                    <ul>
                        <?php 
                            $peb_img = 'peb-A.png';
                            if( get_field('niveau_peb', $propID) ) $peb_img = 'peb-'. strtoupper( get_field('niveau_peb', $propID) ).'.png';
                        ?>
                            <li><img src="<?= IMG_DIR . $peb_img ?>" alt="PEB" class="peb"></li>
                        <?php
                            if( get_field('energie_specifique', $propID) )
                                echo "<li>Energie spécifique : ".get_field('energie_specifique', $propID)." kWh/m².an</li>";
                            if( get_field('certficat_peb', $propID) )
                                echo "<li>PEB No. : ".get_field('certficat_peb', $propID)."</li>";
                        ?>
                            
                        <?php
                            if( get_field('peb_total', $propID) )
                                echo "<li>PEB Total : ".get_field('peb_total', $propID)." Kwh/an</li>";
                            if( get_field('emission_co2', $propID) )
                                echo "<li>CO2 : ".get_field('emission_co2', $propID)." Kg/m²/an</li>";
                            if( search_details_by_id( $details, 1634 ) ){
                                $value = search_details_by_id( $details, 1634 );
                                echo "<li>Double vitrage : $value</li>";
                            }
                            if( search_details_by_id( $details, 757 ) ){
                                $value = search_details_by_id( $details, 757 );
                                $value = "1" == $value ? 'oui':'non';
                                echo "<li>Electricité : $value</li>";
                            }
                            if( search_details_by_id( $details, 43 ) ){
                                $value = search_details_by_id( $details, 43 );
                                $value = "1" == $value ? 'oui':'non';
                                echo "<li>Air conditionné : $value</li>";
                            }
                            if( search_details_by_id( $details, 53 ) ){
                                $value = search_details_by_id( $details, 53 );
                                echo "<li>Chauffage (ind/coll) : $value</li>";
                            }
                        ?>
                    </ul>
                </div>
                <div class="blocBtnDetail">
                    <a href="<?= get_field('field_60e5ff26eb53e', $propID) ?>" class="btn" target="_blank"><?= get_field('bouton_ag_copie','option') ?></a>
                </div>
            </div>
        </div>
    </div>
</section>