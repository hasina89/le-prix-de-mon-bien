<section class="sec_download_doc">
    <div class="container">
       <div class="blc_download">
            <div class="content">
               <h2 class="titre"><?= get_field('titre_docutile','option') ?></h2>
               <div class="listDoc">
                   <a href="<?php the_field('peb') ?>" class="btn btn_doc" target="_blank"><?= get_field('bouton_peb','option') ?></a>
                   <a href="<?php the_field('plan_secteur') ?>" class="btn btn_doc" target="_blank"><?= get_field('bouton_plan_secteur','option') ?></a>
                   <a href="<?php the_field('plan_cadastre') ?>" class="btn btn_doc" target="_blank"><?= get_field('bouton_plan_cadastre','option') ?></a>
                   <a href="<?php the_field('cert_elec') ?>" class="btn btn_doc" target="_blank"><?= get_field('bouton_cert_elec','option') ?></a>
                   <a href="<?php the_field('titre_propriete') ?>" class="btn btn_doc" target="_blank"><?= get_field('bouton_titre_proprio','option') ?></a>
                   <a href="<?php the_field('bdes') ?>" class="btn btn_doc" target="_blank"><?= get_field('bouton_bdes','option') ?></a>
                   <a href="<?php the_field('recap_ag') ?>" class="btn btn_doc" target="_blank"><?= get_field('bouton_ag','option') ?></a>
               </div>
           </div>
       </div>
   </div>
</section>