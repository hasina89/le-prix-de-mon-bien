 <div class="fake_chp">
    <div class="textPlan">
       <p><b>il reste encore <span class="textNbvisite">5 </span> visites à planifier</b> Choisissez un créneau horaire pour votre visite (5 visites maximum)</p>
    </div>
    <form class="blcChp_visite" action="" method="post" id="olr_reservation_form">
       <div class="row_visite">
            
             <div class="listDate">
                <div class="item item1">
                   
                   <label for="date1" class="titre">Visite 1</label>
                   <select name="date1" id="date1">
                      <option selected="true" value="">Sélectionnez dans la liste</option>
                   </select>
                   <div id="selected_date1" class="selected_date hidden">
                      <label class="titre">Valider</label>
                      <span class="button-bloc"><input type="button" name="submit-abus" value="OUI" class="submit desk" id="submit_reservation_1">&nbsp;<input type="button" name="cancel-abus" value="NON" class="submit desk" id="cancel_reservation_1"></span>
                   </div>
                </div>
                <div class="item item2 desable">
                
                   <label for="date2" class="titre">Visite 2</label>
                   <select name="date2" id="date2" disabled="disabled">
                      <option value="">Sélectionnez dans la liste</option>
                   </select>
                   <div id="selected_date2" class="selected_date hidden"><label class="titre">Valider</label>
                      <span class="button-bloc"><input type="button" name="submit-abus" value="OUI" class="submit desk" id="submit_reservation_2">&nbsp;<input type="button" name="cancel-abus" value="NON" class="submit desk" id="cancel_reservation_2"></span>
                   </div>
                </div>
                <div class="item item3 desable">
                  
                   <label for="date3" class="titre">Visite 3</label>
                   <select name="date3" id="date3" disabled="disabled">
                      <option value="">Sélectionnez dans la liste</option>
                   </select>
                   <div id="selected_date3" class="selected_date hidden"><label class="titre">Valider</label>
                      <span class="button-bloc"><input type="button" name="submit-abus" value="OUI" class="submit desk" id="submit_reservation_3">&nbsp;<input type="button" name="cancel-abus" value="NON" class="submit desk" id="cancel_reservation_3"></span>
                   </div>
                </div>
                <div class="item item4 desable"> 
                   <label for="date4" class="titre">Visite 4</label>
                   <select name="date4" id="date4" disabled="disabled">
                      <option value="">Sélectionnez dans la liste</option>
                   </select>
                   <div id="selected_date4" class="selected_date hidden"><label class="titre">Valider</label>
                      <span class="button-bloc"><input type="button" name="submit-abus" value="OUI" class="submit desk" id="submit_reservation_4">&nbsp;<input type="button" name="cancel-abus" value="NON" class="submit desk" id="cancel_reservation_4"></span>
                   </div>
                </div>
                <div class="item item5 desable">
                  
                   <label for="date5" class="titre">Visite 5</label>
                   <select name="date5" id="date5" disabled="disabled">
                      <option value="">Sélectionnez dans la liste</option>
                   </select>
                   <div id="selected_date5" class="selected_date hidden"><label class="titre">Valider</label>
                      <span class="button-bloc"><input type="button" name="submit-abus" value="OUI" class="submit desk" id="submit_reservation_5">&nbsp;<input type="button" name="cancel-abus" value="NON" class="submit desk" id="cancel_reservation_5"></span>
                   </div>
                </div>
             </div>
   
        
       </div>
    </form>
</div>
<style type="text/css">
   /*CSS RDV*/
.listDate .textError {
   color: red !important;
}

.textError.titre_acces {
      text-align: center;
      color:red;
}

.textSuccess.titre_acces {
   text-align: center;
   center;color:green;
}

.loading {
    display: block;
    background: url('../image/loading.gif') no-repeat center middle;
    width: 100%;
    height: 200px;
    margin: 0 auto;
}
/* Hide all the children of the 'loading' element */
.loading * {
    display: none;
}

.listDate button, .listDate select {
    width: 100%;
    font-size: 16px;
    margin: 10px;
}

.plan_visite .blcChp_visite .submit {
    display: inline-block !important;
    margin-top: 10px !important;
    padding: 0 10px 0 10px !important;
    width: auto !important;
}

.hidden {
    display: none;
}

.listDate .selected_date  {
    font-size: 15px;
    padding: 16px 0 16px 10px;
}



.button-bloc {
    float: right;
    display: flex;
}

.plan_visite .listDate select,
.plan_visite .selected_date{ height: 71px;border-radius: 35px;margin: 0;font:bold 18px/28px "lato";-webkit-box-shadow: 0px 4px 16px -7px rgba(0,0,0,0.75);-moz-box-shadow: 0px 4px 16px -7px rgba(0,0,0,0.75);box-shadow: 0px 4px 16px -7px rgba(0,0,0,0.75);border: none;;padding: 10px 35px;text-transform: uppercase;color: #ffffff;}
.plan_visite .listDate select{ background:  url(../image/arrow-down-b.svg)calc(100% - 35px) center no-repeat #A29F78;-webkit-appearance:none; -ms-appearance:none; appearance:none; -moz-appearance:none; -o-appearance:none;padding: 10px 35px;color: #ffffff;text-transform: uppercase; }
.plan_visite .selected_date{ display: flex;justify-content: space-between;align-items: center; }
.plan_visite .selected_date.hidden{ display: none; }
.blcChp_visite .right{ padding-left: 20px; }
.plan_visite .listDate .item {width: 50%;margin-bottom: 27px;padding: 0 10px}

.plan_visite .blcChp_visite .button-bloc .submit{ margin-top: 0!important;width: 100px!important;display: block;height: 54px;background: #ffffff; }
.blcChp_visite .row_visite .selected_date label { margin-bottom: 0; }
.plan_visite .selected_date{ background:#A29F78 ; }
.plan_visite .selected_date.planifie{ background: #ffffff;color: #002850;position: relative;padding-left: 60px; }
.plan_visite .selected_date.planifie:before{ content: '';width: 23px;height: 18px;background:  url(../image/check.svg)center no-repeat;position: absolute;top: 50%;margin-top: -9px;left: 29px; }
.plan_visite .blcChp_visite .button-bloc .submit[name="submit-abus"]{ margin-right: 5px; }
.plan_visite .blcChp_visite .button-bloc .submit[name="cancel-abus"]{ margin-left: 5px; }
.plan_visite .blcChp_visite .button-bloc .submit:checked{ background: #000; }
.plan_visite .blcChp_visite .button-bloc .submit:hover{ background:#00E29D;color: #ffffff; }


@media (max-width:1720px) {
    .plan_visite .listDate select, .plan_visite .selected_date{ padding: 10px 20px; }
    .plan_visite .blcChp_visite .button-bloc .submit { width: 80px!important; }

}

@media (max-width:1440px) {
    .plan_visite .selected_date.planifie::before { left: 15px; }
    .plan_visite .selected_date.planifie{ padding-left: 50px; }

}

@media (max-width:768px) {
    .plan_visite .listDate .item { width: 100%; }
}

@media (max-width:601px) {
   .blcChp_visite .right{ padding-left: 0; } 
   .plan_visite .selected_date.planifie{ padding-right: 15px;font-size: 16px; }
   .plan_visite .listDate .item{ padding: 0; }
}


@media (max-width:601px) {
    
}

</style>