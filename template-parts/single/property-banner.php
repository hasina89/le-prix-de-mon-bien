<?php
  $now = new DateTime();

  $date_creation = get_field('field_creation', $propID ) ? get_field('field_creation', $propID ) : "01/01/2021 13:42";

  $date_creation_1 = new DateTime( $date_creation );
  $date_creation_1 = $date_creation_1->format('Y-m-d H:i:s');
  $date_creation_1 = new DateTime( $date_creation_1 );

  $date_creation_2 = new DateTime( $date_creation );
  $date_creation_2 = $date_creation_2->format('Y-m-d H:i:s');
  $date_creation_2 = new DateTime( $date_creation_2 );

  $date_creation_0 = new DateTime( $date_creation );
  $date_creation_0 = $date_creation_0->format('Y-m-d H:i:s');
  $date_creation_0 = new DateTime( $date_creation_0 );

  $date_creation_3 = new DateTime( $date_creation );
  $date_creation_3 = $date_creation_3->format('Y-m-d H:i:s');
  $date_creation_3 = new DateTime( $date_creation_3 );

  $date_creation_4 = new DateTime( $date_creation );
  $date_creation_4 = $date_creation_4->format('Y-m-d H:i:s');
  $date_creation_4 = new DateTime( $date_creation_4 );
  
  $_24H_apres_creation     = $date_creation_4->add( new DateInterval('P1D') );
  $_10j_apres_creation     = $date_creation_1->add( new DateInterval('P10D') );
  $_12j_apres_creation     = $date_creation_2->add( new DateInterval('P12D') );
  $_10j_47H_apres_creation = $date_creation_3->add( new DateInterval('P10DT47H') );

  @include_once 'property-banner-barre_top.php';
?>

<section class="banner-detail" style="background: url( <?= $bg_url ?>) !important; background-size: cover !important">
  
  <?php @include_once 'property-banner-banner_text.php'; ?>

  <div class="nombre_visite">
    <?php
      $view = (int)get_post_meta( $propID, 'post_views_count', true );
      if( 'proprietaire' == get_post_type( get_the_ID() ) ) {
        if( $view >= 8 ){
          echo str_replace( '{nbr}', $view, get_field('nbr_consult','option') ) ;
        }        
      }else{
        echo str_replace( '{nbr}', $view, get_field('nbr_consult','option') ) ;
      }
    ?>   
  </div>
</section>