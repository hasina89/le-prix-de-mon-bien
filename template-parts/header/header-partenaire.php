<div class="barre_top">
    <div class="blcTop fixed">
        <?php 
            $nbr_subscription = (int)get_post_meta( ID_ABO_AN, 'total_sales', true );
            if( $nbr_subscription <= NBR_REDUCTION ){
                $show = NBR_REDUCTION - $nbr_subscription;
        ?>
            <!-- <div class="bandeau">         
                <div class="prom">
                    <div class="content-prom">
                        Bénéficiez de -50% à vie sur votre abonnement aux <?= $show ?> premiers abonnés
                    </div>
                </div>
           </div> -->
        <?php } ?>
        <div class="container headParteneire">
            <div class="row">
                <div class="col top-partenaire">
                    <a href="<?= site_url(); ?>" class="logo logo_partenaire wow fadeInUp">
                        <img src="<?= IMG_DIR ?>logo-partenaire.svg" alt="">
                    </a>
                    <div class="slogan">
                        <?php echo str_replace('{nbr_abo}', $show, get_field('promotion')) ?>
                    </div>


                    <div class="blocLogin">
                        <?php if(!is_page('liste-biens')) :?>
                            <a href="#section_biens_partenaires" class="scroll btn btn_partenaire">Voir les biens</a>
                        <?php endif; ?>
                        <?php if( !is_user_logged_in() ): ?>

                            <a href="#blocParte" class="btn scroll btn_abonnment lrm-hide-if-logged-in"> <?/**= get_field('bouton_abonnement') **/?>S'abonner</a>
                            <a href="#se_connecter" class="btn-connect lrm-hide-if-logged-in fancybox" data-fancybox=""><?=get_field('bouton_connexion') ?></a>
                        <?php else: 
                            $current_user = wp_get_current_user();
                        ?>
                            <a href="<?php echo site_url('mon-compte') ?>" class="btn-connect lrm-show-if-logged-in"><?= $current_user->first_name.' '.$current_user->last_name ?></a>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php 

    $type_de_banniere = get_field('type_de_banniere'); 

?>
<section class="banner_partenaire section_header_partenaire">
    <?php if( $type_de_banniere == "image"): ?>
        <div class="img_banner">
            <?php if( get_field('image_de_banniere') ) :?>
                <img src="<?= get_field('image_de_banniere') ?>">
            <?php else: ?>
                <img src="<?= IMG_DIR.'bg-partenaire.jpg' ?>">
            <?php endif;?>
        </div>
    <?php endif;?>
    <!-- bloc video BG -->
    <?php if( $type_de_banniere == 'video_int'): ?>
        <div class="video-banner">
            <video autoplay="true" loop="true" muted="muted" playsinline  webkit-playsinline>
                <source  type="video/mp4" src="<?= get_field('video_upload') ?>" >
            </video>
        </div>
    <?php 
        elseif( $type_de_banniere == 'video_ext'): 
            $video_url = get_field('video_externe'); 
            $video_url = explode('?v=', $video_url);
            $video_id = $video_url[1];
    ?>
        <div class="iframe-container">
          <div id="player"></div>
        </div>
        <script>
          var tag = document.createElement('script');

          tag.src = "https://www.youtube.com/iframe_api";
          var firstScriptTag = document.getElementsByTagName('script')[0];
          firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

          var player;
          function onYouTubeIframeAPIReady() {
            player = new YT.Player('player', {
              width: '100%',
              videoId: '<?= $video_id ?>',
              playerVars: { 
                'autoplay': 1, 
                'playsinline': 1, 
                'start': 9, 
                'end': 155,
                'controls': 0,
                'loop': 1
              },
              events: {
                'onReady': onPlayerReady
              }
            });
          }

          function onPlayerReady(event) {
             event.target.mute();
            event.target.playVideo();
          }
        </script>
    <?php endif; ?>
    <!-- /bloc video BG -->

    <div class="container">
        <div class="content">
            <div class="textBanner">
                <!-- <h1><?= get_field('grand_titre') ?></h1> -->
                <h1>Découvrez tous nos biens </h1>
                <!-- <p><?= get_field('petit_titre') ?></p> -->
                <div class="blcBoutton">
                    <a href="#" class="btn"><?= get_field('voir_plus') ?></a>
                    <a href="#blocParte" class="btn scroll"><?= get_field('les_offres') ?></a>
                </div>
            </div>
        </div>   
    </div>
</section>
