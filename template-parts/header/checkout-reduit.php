<?php if( !is_wc_endpoint_url( 'order-received' ) ): ?>
  <div class="barre_top">
       <div class="blcTop">
            <div class="container">
                <?php $link = get_field('s_bouton_2'); ?>
                 <a href="<?= site_url(); ?>" class="logo wow fadeInUp">
                    <img src="<?= IMG_DIR ?>vendezmonbien-pos.svg" alt="">
                </a>
            </div>
        </div>
   </div>
  <header class="header-arson <?php if(is_wc_endpoint_url( 'order-received' )){ echo 'order-received'; } ?> " style="background-image: url(<?php the_field('banner', 'option') ?>);" >
      <div class="top">
         <!--  <a href="<?= site_url(); ?>" class="logo wow fadeInUp">
              <img src="<?php the_field('logo','option') ?>" alt="">
          </a> -->
          <div class="container">
              <div class="row d-flex justify-content-center">
                  <div class="col-lg-10 col-md-12 wow fadeInUp">
                      <div class="sousCheckout">
                          <div class="cover cover1">
                              <img src="<?= IMG_DIR.'cover1.png'; ?>" alt="">
                              <div>
                                  Recevez votre <br>rapport d’Expertise
                                  <span>valeur totale</span>
                                  <strong class="strong">249€</strong>
                              </div>
                          </div>
                          <span class="plus"></span>
                          <div class="cover cover2">
                              <img src="<?= IMG_DIR.'cover2.png' ?>" alt="">
                              <div>
                                  <b>En Bonus :</b> 10 règles <br>d’or pour vendre votre <br>maison comme un PRO
                                  <span>offert d'une valeur de</span>
                                  <strong class="strong">97€</strong>
                              </div>
                          </div>
                          <div class="rapport">
                              <div>
                                  Votre rapport<br>d'expertise<br>"vendezmonbien.be"
                                  <span>seulement</span>
                                  <strong class="strong">49€</strong>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <a href="#customer_details" class="scroll scrollDown">Défilez pour terminer votre expertise</a>
  </header>
  <?php 
  else:
    get_template_part('template-parts/header/typ-payment');
  endif; 
?>