<?php
/**
 * The header for Astra Theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Astra
 * @since 1.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly.
}

?><!DOCTYPE html>
<?php astra_html_before(); ?>
<html <?php language_attributes(); ?>>
<head>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-MM2XMMM');</script>
    <!-- End Google Tag Manager -->

    <?php //astra_head_top(); ?>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php if( is_singular( array('property') ) ){ ?>
        <link rel="icon" href="<?= IMG_DIR . 'fav-part.png'; ?>">
    <?php }else{ ?> 
        <link rel="icon" href="<?= IMG_DIR . 'fav.png'; ?>">
    <?php } ?>

    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <?php if( is_home() || is_front_page() ): ?>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJwwWxqvXjx_NN1dq-DgGNhleyuZrSSyI&libraries=places&v=weekly"></script>
    <script src="<?= ASTRA_THEME_CHILD_URI.'assets/js/ac_address.js'; ?>"></script>
    <?php endif; ?>
    <?php wp_head(); ?>
</head>
<?php if( is_home() || is_front_page() ): ?>
    <body <?php body_class(); ?> onload="initAutocomplete()">
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src=https://www.googletagmanager.com/ns.html?id=GTM-MM2XMMM
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->
<?php 
    elseif( is_singular( 'property' ) ):
        if( !in_array( $fiche_id, $boughts ) && current_user_is_subscribed() ){ 
?>
            <body <?php body_class('abonne_tsy_mbol_nividy'); ?>>
<?php
        }else{
?>
            <body <?php body_class(); ?>>
<?php
        }
?>
<?php else: ?>
    <body <?php body_class(); ?>>
<?php endif; ?>

<?php if( !is_singular( array('property','proprietaire') )) : ?>
<header>
    <?php if(is_front_page()) {?>
        <!-- <a href="#blocForm" class="btn fixe scroll wow slideInRight"><?= $lien['title'] ?></a>-->
       <?php
            $coupon_id = get_coupon_id(); 
            $count_coupon = count_coupon( $coupon_id );
            $code = get_the_title( $coupon_id );
            $reste = 100 - $count_coupon;
            $texte_invitation = get_field('texte_invitation');
            $texte_reste = get_field('texte_reste');
            $asterisque = get_field('asterisque');

            $texte_invitation = str_replace( '{code_coupon}', '<span id="finaritra">'.$code.'</span>', $texte_invitation );
            $texte_reste = str_replace( '{reste_coupon}', '<span id="arson">'.$reste.'</span>', $texte_reste );
       ?>
       <?php if( ( isset( $_SESSION['mode'] ) && $_SESSION['mode'] == "reduit") || !isset( $_SESSION['mode'] ) ): ?>
           <div class="bandeau">
                <!-- <div class="row-bandeau">
                    <div class="left">
                        <?= $texte_invitation ?><span class="hasina"> <?= $texte_reste ?></span>
                    </div>
                    <div class="right">
                        <a href="#blocForm" class="btn scroll"> Expertise du bien</a>
                    </div>
                </div> -->            
                <!-- <div class="prom">
                    <div class="content-prom">
                        <a href="#blocForm" class="scroll"><?= $asterisque ?></a>OFFERT aux <strong><?= $reste ?></strong> prochaines demandes !
                        <span class="asterix">*</span>
                        <?= $asterisque ?></div>
                </div> -->
           </div>
         <?php endif; ?>
       <!-- <div class="triangle">
            <div class="content-triangle">
                <span class="close"></span>
                 <?= $texte_invitation ?><span class="hasina"> <?= $texte_reste ?></span>
                <span class="asterix">*</span><?= $asterisque ?>
           </div>
       </div> -->
       <div class="coupon-mobile"><div class="img"><img src="<?= IMG_DIR.'coupon.png' ?>"></div></div></div>
       <?php $type_de_banniere = get_field('type_de_banniere'); ?>
       <div class="barre_top">
           <div class="blcTop">
                <div class="container">
                    <?php $link = get_field('s_bouton_2'); ?>
                     <a href="<?= site_url(); ?>" class="logo wow fadeInUp">
                        <img src="<?= IMG_DIR ?>vendezmonbien-pos.svg" alt="">
                    </a>
                    <a href="<?= $link['url'] ?>" class="btn scroll"><?= $link['title'] ?></a>
                </div>
            </div>
        </div>
       <section class="blocSimulation" <?php if( $type_de_banniere == "image") echo 'style="background-image: url('. get_field('image_de_banniere').')"' ?> >
            <div class="container">

                <div class="row d-flex">

                    <div class="col-lg-12 col-md-12 wow slideInRight">

                        <h2><?php echo get_field('s_titre') ?></h2>

                        <?php echo get_field('s_contenu') ?>

                        <div class="blocBtn d-flex justify-content-center align-items-center wow fadeInUp">

                            <!-- <?php $link = get_field('s_bouton_1'); ?>

                            <a href="<?= $link['url'] ?>" class="btn scroll"><?= $link['title'] ?></a> -->

                            <?php $link = get_field('s_bouton_2'); ?>


                            <div class="text-transform">

                                <img src="<?php the_field('s_image') ?>" alt="">

                            </div>

                        </div>

                    </div>

                </div>
            </div>
            <?php if( $type_de_banniere == 'video_int'): ?>
                <div class="video-banner">
                    <video autoplay="true" loop="true" muted="muted" playsinline  webkit-playsinline>
                        <source  type="video/mp4" src="<?= get_field('video_upload') ?>" >
                    </video>
                </div>
            <?php 
                elseif( $type_de_banniere == 'video_ext'): 
                    $video_url = get_field('video_externe'); 
                    $video_url = explode('?v=', $video_url);
                    $video_id = $video_url[1];
            ?>
                <div class="iframe-container">
                  <div id="player"></div>
                </div>
                <script>
                  var tag = document.createElement('script');

                  tag.src = "https://www.youtube.com/iframe_api";
                  var firstScriptTag = document.getElementsByTagName('script')[0];
                  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                  var player;
                  function onYouTubeIframeAPIReady() {
                    player = new YT.Player('player', {
                      width: '100%',
                      videoId: '<?= $video_id ?>',
                      playerVars: { 
                        'autoplay': 1, 
                        'playsinline': 1, 
                        'start': 9, 
                        'end': 155,
                        'controls': 0,
                        'loop': 1
                      },
                      events: {
                        'onReady': onPlayerReady
                      }
                    });
                  }

                  function onPlayerReady(event) {
                     event.target.mute();
                    event.target.playVideo();
                  }
                </script>
            <?php endif; ?>
        </section>

        <section class="blocLance text-center">

        <div class="container">

            <h2 class="wow fadeInDown"><?php the_field('lc_titre_noir') ?><span><?php the_field('lc_titre_orange') ?></span>

            </h2>

            <div class="row">
                <?php 
                $nbr_demande = get_field('total_nbr_demande'); 
                $i = 0;

                $array_search = array( '%s', 'demandes' );
                if( (int)$nbr_demande < 2 ){
                    $array_replace = array( $nbr_demande, 'demande' );
                }else{
                    $array_replace = array( $nbr_demande, 'demandes' );
                }

                if(have_rows('colonnes')): while(have_rows('colonnes')): the_row(); ?>

                    <div class="item col-md-4 text-uppercase wow fadeInUp">
                        <span id="span_<?= $i ?>">
                            <?php echo str_replace( $array_search, $array_replace, get_sub_field('chiffre') ); ?>
                        </span>
                        <?php the_sub_field('texte'); $i++; ?>
                    </div>
                <?php endwhile; endif; ?>

            </div>

            <?php $link = get_field('bouton_estimer'); ?>

            <a href="<?= $link['url'] ?>" class="btn scroll wow fadeInUp"><?= $link['title'] ?></a>

        </div>
    </section>

    <?php if( is_home() || is_front_page() ):
       get_template_part( 'template-parts/header/bloc', 'timeline' );
     endif; ?>

    <?php } ?>


    <?php if(!is_front_page()) {?>
    <div class="barre_top barre_top_page">
       <div class="blcTop">
            <div class="container">
                <?php $link = get_field('s_bouton_2'); ?>
                <?php $lien = get_field('bouton_estimer','option'); ?>
                 <a href="<?= site_url(); ?>" class="logo wow fadeInUp">
                    <img src="<?= IMG_DIR ?>vendezmonbien-pos.svg" alt="">
                </a>
                <!-- <a href="<?= site_url(); ?>/#blocForm" class="btn scroll"><?= $lien['title'] ?></a> -->
            </div>
        </div>
    </div>
    <section 
        class="header-banner" 
        <?php 
            if( !is_front_page() && !is_page('faq') ){ ?> 
                style="background-image: url(<?php the_field('banner', 'option') ?>)" 
        <?php }elseif( is_page('faq') || is_page('faqs') ){ ?>
                style="background-image: url(<?php the_field('banner_faq', 'option') ?>)" 
        <?php } ?>
    >
        <div class="top">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-lg-10 col-md-12 wow fadeInUp">
                        <?php if(is_front_page()) {?>
                            <h1><?php the_field('titre_1', 'option') ?><?php the_field('titre_2', 'option') ?></h1>
                            <?php the_field('paragraphe', 'option') ?>
                        <?php }elseif(is_page('faq')) { ?>
                            <h1>Foire aux questions</h1>
                        <?php } ?>

                        <?php if(is_page('merci')) {?>
                            <a href="<?= site_url(); ?>" class="logo wow fadeInUp">
                                <img src="<?= IMG_DIR ?>logo-blanc.svg" alt="map">
                            </a>
                            <?php
                                if( isset($_GET['ct']) && $_GET['ct'] != 'coach'):
                            ?>
                                <p><b>Nos agents vous recontacteront dans les 48h.</b> Vendezmonbien.be vous remercie de votre confiance.</p>
                            <?php else: ?>
                                <p><b>Nos agents vous recontacteront dans les 48h.</b> Vendezmonbien.be vous remercie de votre confiance.</p>
                            <?php endif; ?>
                            <div class="map-head">
                                <img src="<?= IMG_DIR ?>map5.png" alt="map">
                            </div>
                        <?php }?>
                        
                        <div class="btnTransform">
                            <?php $lien = get_field('bouton_estimer','option'); ?>
                            <?php
                                if( is_array($lien) && array_key_exists('title', $lien) ): 
                                    if(is_front_page()) {?>
                                    <a href="#blocForm" class="btn scroll"><?= $lien['title'] ?></a>
                                <?php } else if(is_page('mentions-legales')){?>
                                    
                                <?php } 
                                endif; ?>
    						<?php if(is_page('merci')) {?>
                                <a href="<?= site_url(); ?>" class="btn">retour à l'accueil</a>
                            <?php }?>
                            <?php if(is_front_page()) {?>
                               <!--  <div class="text-transform wow bounce">
                                    <img src="<?= IMG_DIR.'prix3.svg'; ?>">
                                </div> -->
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
            <?php if(is_front_page()) {?>
               <!-- <a href="#blocForm" class="btn fixe scroll wow slideInRight"><?= $lien['title'] ?></a>-->
            <?php } else{
                    if( is_array($lien) && array_key_exists('title', $lien) ): ?>
                        <!-- <a href="<?= site_url(); ?>/#blocForm" class="btn fixe wow slideInRight"><?= $lien['title'] ?></a> -->
            <?php endif; } ?>
        </div>  
    </section>
    <?php } ?>
</header>
<?php endif; //singular ?>
