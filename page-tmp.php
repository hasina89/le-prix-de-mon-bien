<?php
// template name: tmp
global $woocommerce;
  // check ra avy any @mail ==> continuer process achat
if( isset($_GET['sim']) && !empty( $_GET['sim'] ) && $_GET['sim'] == 'continue' ){
  if( isset($_GET['lid']) && !empty( $_GET['lid'] ) ){
    if( isset($_GET['email']) && !empty( $_GET['email'] ) ){
      $id = strip_tags( $_GET['lid'] );
      $email = strip_tags( $_GET['email'] );

      $lead = get_lead_data($id, $email);

      if( $lead ){
        $mode = $lead->mode;

        if( $mode == '' || $mode == 'reduit' ){
          $product_id = 176;
        }else{
          $product_id = 360;
        }

        $_SESSION['Data'] = json_decode( $lead->data );
        $_SESSION['pj']   = json_decode( $lead->attachment );

        
        $woocommerce->cart->empty_cart();
        $woocommerce->cart->add_to_cart( $product_id, 1 );

        header( 'Location: ' . site_url( "/paiement" ) );
      }else{
        wp_die('Expertise déjà acquise !');
      }
      
    }else{
      wp_die('Provenance incertaine :) ');
    }
  }else{
    wp_die('Provenance incertaine :) ');
  }

  
}else{
  wp_die('Provenance incertaine :) ');
}


?>