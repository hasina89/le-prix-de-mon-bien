<?php

/**

 * Template Name: partenaires

 */

if( isset( $_GET['_rdc'] ) && !empty( $_GET['_rdc'] ) && "1" == wp_strip_all_tags( $_GET['_rdc'] ) ){
    $_SESSION['REFERER_BIEN'] = $_SERVER['HTTP_REFERER'];
}

get_header('partenaires'); 

get_template_part( 'template-parts/header/header', 'partenaire' );

?>
<section class="sec_pourquoi">
    <div class="container">
        <div class="blctitre">
            <h2>
                <?= get_field('grand_titre_copie') ?>
                <strong><?= get_field('petit_titre_copie') ?></strong>
            </h2>
            <div class="intro">
                <p><?= get_field('desciptionn') ?></p> 
            </div>
        </div> 
        <div class="blcTimeline">
            <div class="liste_etape" id="etape_timeline">
                <?php
                    $steps = get_field('les_differentes_etapes');

                    foreach( $steps as $step ):
                ?>
                        <div class="etape">
                            <div class="picto">
                                <img src="<?= $step['icone'] ?>">
                            </div>
                            <div class="info_etape">
                                <p><?= $step['explication'] ?></p>
                            </div>
                        </div>
                <?php endforeach; ?>
            </div>
            <a href="#blocParte" class="btn scroll"><?= get_field('bouton_partenaire') ?></a>
        </div>
    </div>   
</section>

<section class="blocPrix prix_partenaire scroll" id="Prix">
    <div class="container">
        <div class="row">
            <div class="col-md-7 wow slideInLeft">
                <h2><?= get_field('grand_titre_copie2') ?></h2>
                <div class="d-flex flex-wrap">
                    <?= get_field('liste_des_avantages') ?>
                </div>
            </div>
            <div class="blocImg col-md-5 wow slideInRight">
                <img src="<?= IMG_DIR ?>img-avantage-partenaire.jpg" alt="">
            </div>
        </div>
    </div>
</section>

<section class="section_biens section_biens_partenaires" id="section_biens_partenaires">
    <div class="container">
        <div class="blcTitre">
            <h2><strong>Biens disponibles à la vente </strong></h2>
            <a href="<?= site_url(); ?>/liste-biens" class="btn btn_partenaire">Voir tous les biens</a>
        </div>
        <?php echo liste_biens('A vendre'); ?>
    </div>
</section>

<section id="blocParte" class="sec_offre_partenaire">
    <div class="container">
        <h2><?= get_field('les_offres') ?></h2>
        <div class="blctitre">
            <div class="intro">
                <p>
                    <!-- Bénéficiez de -50% à vie <br>pour les 100 premiers abonnés. -->
                    <?= get_field('promo_carte') ?>
                </p> 
            </div>
        </div> 
        <div class="list_offre">
           <!--  <div class="item basique">
                <div class="content">
                    <div class="blcTop">
                        <div class="icon_offre"></div>
                        <div class="nom_offre">Basique</div>
                        <div class="prix">47€<span>/mois</span></div>
                        <div class="nombre_code_postal">1 code postal</div>
                    </div>
                    <div class="blcBottom">
                        <p>Vous recevez les biens<br> en vente dans la commune <br>que vous aurez sélectionné.</p>
                        <a href="#popup_partenaire" class="btn fancybox" data-fancybox="">s'abonner</a>
                    </div>

                </div>
            </div> -->
            <div class="item premium">
                <div class="content">
                    <div class="blcTop">
                        <div class="icon_offre"></div>
                        <div class="nom_offre">Mensuel</div>
                        <!-- <div class="prix old_price">394€<span>/mois</span></div> -->
                        <div class="prix new_price"><?= PRIX_NORMAL ?>€<span>/mois</span></div>
                        <!-- <div class="text_promo">
                            <?php echo str_replace('{nbr_abo}', $show, get_field('promo_50_carte')) ?>
                        </div> -->
                        <div class="nombre_code_postal">3 codes postaux</div>
                    </div>
                    <div class="blcBottom">
                        <p><?= get_field('reception_des_biens') ?></p>
                         <a href="#popup_partenaire" class="btn fancyboxss" data-target="mois"><?= get_field('bouton_abonnement') ?></a>
                    </div>

                </div>
            </div>
            <div class="item vip">
                <div class="content">
                    <div class="blcTop">
                        <div class="icon_offre"></div>
                        <div class="nom_offre">annuel</div>
                        <div class="prix old_price"><?= PRIX_NORMAL_AN ?>€<span>/an</span></div>
                        <div class="prix new_price"><?= PRIX_REDUIT_AN ?>€<span>/an</span></div>
                        <div class="text_promo">
                            <?php echo str_replace('{nbr_abo}', $show, get_field('promo_50_carte')) ?>
                        </div>
                        <div class="nombre_code_postal">10 codes postaux</div>
                    </div>
                    <div class="blcBottom">
                        <p><?= get_field('reception_des_biens') ?></p>
                         <a href="#popup_partenaire" class="btn fancyboxss" data-target="annee"><?= get_field('bouton_abonnement') ?></a>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<!-- <a href="#popup_partenaire" class="link_pp fancybox" data-fancybox="groupe1"></a> -->
<div class="popup_partenaire <?php if( current_user_is_subscribed() ) echo 'popup_abonne_kely' ?>" id="popup_partenaire" style="display:none">
    <?php 
        if( !current_user_is_subscribed() ):
            $current_user = wp_get_current_user();
    ?>
        <div class="content olo_vaovao">
            <h2><?= get_field('titre_popup_abonnement') ?></h2>
            <form id="go-partner">
                <div class="form-group">
                    <div class="champ w-50">
                        <input type="text" id="nom" name="nom" placeholder="Nom*" required=""
                        <?php if( $current_user ) echo "value='$current_user->billing_last_name'" ?>>
                    </div>
                    <div class="champ w-50">
                        <input type="text" id="prenom" name="prenom" placeholder="Prénom*" required=""
                        <?php if( $current_user ) echo "value='$current_user->billing_first_name'" ?>>
                    </div>
                </div>
                <div class="form-group">
                    <div class="champ w-50">
                       <input type="email" id="email" name="email" placeholder="Email*" required=""
                       <?php if( $current_user ) echo "value='$current_user->user_email'" ?>>
                    </div>
                    <div class="champ w-50">
                        <input type="text" id="telephone" name="telephone" placeholder="Téléphone*" required=""
                        <?php if( $current_user ) echo "value='$current_user->billing_phone'" ?>>
                    </div>
                </div>
                <!-- <div class="form-group form_radio">
                    <div class="champ w-35">
                        <div class="label_form white">numéro I.P.I.</div>
                        <div class="radio numero_ipi_oui">
                            <input type="radio" id="numero_ipi_oui" class="form-control numero_ipi"  value="oui" name="num_ipi"
                            <?php if( get_user_meta($current_user->ID,'invoice_ipi_number', true) ) echo "checked" ?>
                            >
                            <div class="blcLabel">
                                <label for="numero_ipi_oui">Oui</label>
                                <input type="text" name="numero__ipi" placeholder="numéro I.P.I."  class="numero-ipi"
                                <?php if( $current_user ) echo "value='". get_user_meta($current_user->ID,'invoice_ipi_number', true) ."'" ?>>
                            </div>
                        </div>
                    </div>
                    <div class="champ w-15">
                        <div class="radio numero_ipi_non">
                            <input type="radio" id="numero_ipi_non" class="form-control numero_ipi"  value="non" name="num_ipi">
                            <div class="blcLabel">
                                <label for="numero_ipi_non">non</label>
                            </div>
                        </div>
                    </div>
                </div> -->


                <div class="form-group">
                    <div class="champ w-50">
                        <input type="radio" id="numero_ipi_oui" class="form-control numero_ipi"  value="oui" name="num_ipi" checked style="position: absolute;opacity: 0;top: 0;z-index: -99;">
                       <input type="number" name="numero__ipi" placeholder="numéro I.P.I."  
                                <?php if( $current_user ) echo "value='". get_user_meta($current_user->ID,'invoice_ipi_number', true) ."'" ?>>
                    </div>
                    <div class="champ w-50 champ_acceptance">
                        <div>
                            <input type="checkbox" id="input_acceptance" name="accept_cgu" class="input--checkbox__input input_acceptance">
                            <a href="#popup_condition_general" class="input--checkbox__label label_acceptance"><?= get_field('accpet_cgv') ?></a>
                        </div>
                        <div>
                            <input type="checkbox" id="info_correct" name="info_correct" class="input--checkbox__input">
                            <label class="input--checkbox__label" for="info_correct"><?= get_field('accpet_cgv_copie') ?></label>
                        </div>
                    </div>
                </div>

                

                <div class="form-group form_paiement">
                    <div class="label_form"><?= get_field('selection_mode_de_paiement') ?></div>
                    <div class="champ w-32">
                        <label for="visa">
                            <input id="visa" type="radio" class="paiement" value="stripe" name="paiement">
                            <span class="icon_radio"></span>
                            <span class="img"><img src="<?= IMG_DIR ?>visa2.png" alt="vendez mon bien"></span>
                        </label>
                    </div>
                    <div class="champ w-32">
                        <label for="ban_contact">
                            <input id="ban_contact" type="radio" class="paiement" value="stripe_bancontact" name="paiement">
                            <span class="icon_radio"></span>
                            <span class="img"><img src="<?= IMG_DIR ?>bancontact2.png" alt="vendez mon bien"></span>
                        </label>
                    </div>
                    <div class="champ w-32">
                        <label for="paypal_">
                            <input id="paypal_" type="radio" class="paiement" value="ppcp-gateway" name="paiement">
                            <span class="icon_radio"></span>
                            <span class="img"><img src="<?= IMG_DIR ?>paypal2.jpg" alt="vendez mon bien"></span>
                        </label>
                    </div>
                    <div class="champ w-22">
                        <label for="tsy_fantatra">
                            <input id="tsy_fantatra" type="radio" class="paiement" value="payconiq" name="paiement">
                            <span class="icon_radio"></span>
                            <span class="img"><img src="<?= IMG_DIR ?>payconiq2.png" alt="vendez mon bien"></span>
                        </label>
                    </div>
                </div>
                <div class="submit-abus sipan">
                    <input type="hidden" name="modalite" id="modalite">
                    <button type="submit" class="submit"><?= get_field('submit_achat_abo') ?></button>
                </div>
            </form>
        </div>
    <?php else: ?>
        <div class="content olo_abonne">
            <?= get_field('deja_abonne') ?>
        </div>
    <?php endif; ?>
</div>

<div class="temoignage_partenaire">
  <!--   <?php  get_template_part( 'template-parts/content/bloc', 'temoignage' ); ?> -->
  <section id="Temoinage" class="blocTest text-center wow fadeIn">
    <?php
        $arg = array(
            'post_type' => 'temoignage',
        );

        $q = new WP_Query( $arg );

        if( $q->have_posts() ):
    ?>
    <h2>
        Ils ont testé et approuvé !
    </h2>
    <div class="slideTeste">
        <?php while( $q->have_posts() ) : $q->the_post(); ?>
        <div class="test">
            <div class="icon d-flex justify-content-center align-items-center">
                <?php 
                    $etoile = get_field('evaluation'); 
                    if( $etoie > 0 ):
                        for ($i=1; $i = $etoile  ; $i++) { 
                            echo '<img src="'.IMG_DIR .'icon-star.png" alt="">';
                        }
                    endif;
                ?>
            </div>
            <div class="text">
                <?php the_field('texte_temoignage') ?>
            </div>
            <div class="photo"><img src="<?= IMG_DIR ?>photo-temoignage.jpg" alt=""></div>
            <strong class="text-uppercase"><?php the_title() ?><span><?php the_field('compagnie') ?></span></strong>
        </div>
        <?php endwhile; ?>
        
    </div>
    <?php wp_reset_postdata(); endif; ?>
</section>

  <!-- popup connexion -->
<div class="popup_connex" id="se_connecter" style="display:none">
    <?php echo do_shortcode('[lrm_form default_tab="login" logged_in_message="Vous êtes déjà authentifé"]'); ?>
</div>

<div class="popup_condition_general" id="popup_condition_general" style="display:none">
    <div class="inner_condition">
        <div class="content_condition">
            <h2>conditions générales de vente</h2>
           <?php the_field('texte_cgu') ?>

        </div>
       
        <div class="btn_validation">
            <div class="btn acceptance_btn">J’accepte</div>
            <!-- <div class="btn acceptance_btn" onclick="parent.jQuery.fancybox.close()">J’accepte</div> -->
        </div>
    </div>
</div>
  
</div>
<div class="home blockUI blockOverlay"></div>
<?php get_footer('partenaire'); ?>
