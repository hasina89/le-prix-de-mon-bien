<?php

/**

 * Template Name: FAQ

 */
get_header(); 

$args = array(
  'post_type' => 'faqs',
  'posts_per_page' => -1,
);

$q = new WP_Query( $args );

if( $q->have_posts() ):
  
?>

<section class="test__">
  <div class="container">
    <div id="accordion">
      <?php while( $q->have_posts() ): $q->the_post(); ?>
        <h2><?php the_title() ?></h2>
        <div>
          <?php the_content() ?>
        </div>
      <?php endwhile; ?>    
    </div>
  </div>
</section>

<script type="text/javascript">
    var $ = jQuery.noConflict();
    $( function() {
      $( "#accordion" ).accordion({
        autoHeight: true,
        animated: false,
        collapsible: true,
        heightStyle: "content"
      });
    } );
</script>

<?php
  endif;
get_footer(); 
