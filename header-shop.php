<!DOCTYPE html>
<?php astra_html_before(); ?>
<html <?php language_attributes(); ?>>
<head>
    <?php //astra_head_top(); ?>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11" />
    <link rel="icon" href="<?= IMG_DIR . 'fav.png'; ?>">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?> >
<div class="barre_top barre_top_page">
   <div class="blcTop">
        <div class="container">
            <?php $link = get_field('s_bouton_2'); ?>
            <?php $lien = get_field('bouton_estimer','option'); ?>
             <a href="<?= site_url(); ?>" class="logo wow fadeInUp">
                <img src="<?= IMG_DIR ?>vendezmonbien-pos.svg" alt="">
            </a>
            <!-- <a href="<?= site_url(); ?>/#blocForm" class="btn scroll"><?= $lien['title'] ?></a> -->
        </div>
    </div>
</div>
<header class="header-arson shop" style="background-image: url(<?php the_field('banner', 'option') ?>);" >
    <div class="top">
        
    </div>
</header>