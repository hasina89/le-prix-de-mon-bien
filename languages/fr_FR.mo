��          �      <      �     �     �     �     �  	     -   "     P     `     r     �     �     �     �     �  E   �  !        (  q  5     �     �     �     �               (     ?     W     n  	   �     �     �     �  :   �     �                                  	                                                                    
    Add New Address Additional billing addresses Address Added successfully. Address Changed successfully. Addresses Are you sure you want to delete this address? Billing Address Billing Addresses Billing address Choose an Address.. Delete Edit Manage Address Set as default The following addresses will be used on the checkout page by default. There are no saved addresses yet  hidden value Project-Id-Version: Themehigh Multiple Addresses 1.0.0
Report-Msgid-Bugs-To: support@themehigh.com
PO-Revision-Date: 2021-09-29 12:27+0300
Language-Team: LANGUAGE <EMAIL@ADDRESS>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.0
Last-Translator: 
Plural-Forms: nplurals=2; plural=(n > 1);
Language: fr_FR
 Ajouter une adresse Autres adresses de facturation Adresse bien ajoutée Adresse mise à jour Adresses Supprimer cette adresse ? Adresse de facturation Adresses de facturation Adresse de facturation Choisir une adresse... Supprimer Editer Gérer les adresses Utiliser par défaut Les adresses suivantes seront utilisées lors du paiement. Aucune adresse sauvegardée Valeur cachée 