<?php

// print_r($_POST);

$garage = 0;
$parking = 0;
$terrasse = 0;
$jardin = 0;


if (!empty($_POST)) {
    extract($_POST);

    if ($banquier) {
        $msg_banquier = "J'ai déjà rencontré mon banquier";
    } else {
        $msg_banquier = "J'ai pas encore rencontré mon banquier";
    }


    error_reporting(E_ALL ^ E_WARNING);
    $url = "http://webservices.whoman2.be/websiteservices/EstateService.svc";
    $clientID = "2cdcc433e57f4bed8de0";
    $officeID = 4675;
    //$codes_postaux = str_replace(' ', '', $codes_postaux);
    //$codes_postaux = explode(',', $codes_postaux);


    function get($method, $varname, $request)
    {
        global $url;
        $geturl = "$url/$method?$varname=" . json_encode($request);
        $response = file_get_contents($geturl);
        return json_decode(json_encode(simplexml_load_string($response)));
    }

    function post($method, $varname, $body)
    {
        global $url;
        $posturl = "$url/$method";
        $data = '{"' . $varname . '": ' . json_encode($body) . '}';
        $options = array(
            'http' => array(
                'header' => "Content-type: application/json\r\nContent-length: " . strlen($data),
                'method' => 'POST',
                'content' => $data
            ),
        );
        $context = stream_context_create($options);
        $result = file_get_contents($posturl, false, $context);
        return json_encode($result);
    }

    function getValue($parent, $name, $index)
    {
        $vars = is_array($parent) ? get_object_vars($parent[$index]) : get_object_vars($parent);
        return $vars[$name];
    }

    // Define Contact class
    class Contact
    {
        public $__type = "EstateServiceUpdateContactRequest:Whoman.Estate";
        public $ClientID;
        public $Address1;
        public $Address2;
        public $AgreementMail;
        public $AgreementSms;
        public $Box;
        public $City;
        public $Comments;
        public $ContactOriginID;
        public $ContactTitleID;
        public $ContactTypeIDList;
        public $CountryID;
        public $EstateID;
        public $FacebookLogin;
        public $FirstName;
        public $LanguageID;
        public $Message;
        public $Name;
        public $Number;
        public $OfficeID;
        public $PrivateEmail;
        public $PrivateMobile;
        public $PrivateTel;
        public $RepresentativeIDList;
        public $SearchCriteria;
        public $StatusID;
        public $Zip;
    }

    // Define ContactSearchCriteria class
    class ContactSearchCriteria
    {
        public $__type = "EstateServiceUpdateContactRequestSearchCriteria:Whoman.Estate";
        public $AreaRange;
        public $CategoryIdList;
        public $CountryId;
        public $EnumID_1;
        public $Fronts;
        public $Furnished;
        public $Garage;
        public $Garden;
        public $GardenAreaRange;
        public $GroundAreaRange;
        public $InvestmentEstate;
        public $MinRooms;
        public $NumericValue_1;
        public $Parking;
        public $PriceRange;
        public $PurposeIdList;
        public $PurposeStatusIdList;
        public $RegionIDList;
        public $State;
        public $SubCategoryIdList;
        public $SubdetailID_1;
        public $Terrace;
        public $TextValue_1;
        public $ZipList;
    }

    // Get contact origins
    $contactOrigins = get(
        'GetContactOriginListXml',
        'EstateServiceGetContactOriginListRequest',
        array(
            'ClientId' => $clientID, 'OfficeId' => $officeID, 'Page' => 0, 'RowsPerPage' => 10, 'Language' => 'nl-BE'
        )
    )->ContactOriginList->EstateServiceGetContactOriginListResponseContactOrigin;

    // Get contact types
    $baseContactTypes = get(
        'GetContactTypeListXml',
        'EstateServiceGetContactTypeListRequest',
        array(
            'ClientId' => $clientID, 'OfficeId' => $officeID, 'Page' => 0, 'RowsPerPage' => 10, 'Language' => 'nl-BE'
        )
    )->BaseContactTypeList->EstateServiceGetContactTypeListResponseBaseContactType;

    // Get contact titles
    $contactTitles = get(
        'GetContactTitleListXml',
        'EstateServiceGetContactTitleListRequest',
        array(
            'ClientId' => $clientID,
            'OfficeId' => $officeID, 'Page' => 0, 'RowsPerPage' => 10, 'Language' => 'nl-BE'
        )
    )->ContactTitleList->EstateServiceGetContactTitleListResponseContactTitle;

    /*$contactTitles = '';
    $baseContactTypes = '';
    $contactOrigins = '';*/

    // Define search criteria
    $searchCriteria = new ContactSearchCriteria();
    $searchCriteria->AreaRange = array(0, $Surface);
    $searchCriteria->CategoryIdList = array(1);
    $searchCriteria->CountryId = 1;
    //$searchCriteria->Fronts = $facades;
    $searchCriteria->Furnished = 0;
    //$searchCriteria->Garage = $garage;
    $searchCriteria->Garden = $gardenSurface;
    $searchCriteria->GardenAreaRange = array($gardenSurface, 9999999);
    $searchCriteria->GroundAreaRange = array(5, 9999999);
    $searchCriteria->InvestmentEstate = 0;
    $searchCriteria->MinRooms = $chambre_nbr;
    $searchCriteria->Parking = $parking;
    $searchCriteria->PriceRange = array(0, 9999999);
    $searchCriteria->PurposeIdList = array(1);
    $searchCriteria->PurposeStatusIdList = array(1);
    $searchCriteria->RegionIDList = array(1826);
    $searchCriteria->State = 1;
    $searchCriteria->SubCategoryIdList = array(1);
    //$searchCriteria->Terrace = $terrasse;
    $searchCriteria->ZipList = $postal;

    // Define contact
    $contact = new Contact();
    $contact->ClientID = $clientID;
    $contact->Address1 = $adresse;
    //$contact->Address2 = $adresse;
    $contact->AgreementMail = true;
    $contact->AgreementSms = true;
    $contact->Box = "box";
    $contact->City = $ville;
    $contact->Comments = $msg_banquier . " \r\n " . "commentaire";
    // $contact->ContactOriginID = getValue($contactOrigins, 'ContactOriginId', 0);
    $contact->ContactOriginID = '';
    $contact->ContactTitleID = '';
    // $contact->ContactTitleID = getValue($contactTitles, 'ContactTitleId', $genre);
    $contact->ContactTypeIDList = '';
    /*$contact->ContactTypeIDList = array(
        getValue(
            $baseContactTypes[0]->ContactTypes->EstateServiceGetContactTypeListResponseContactType,
            'ContactTypeId',
            0
        )
    );*/
    $contact->CountryID = 1;
    //$contact->FirstName = $prenom;
    //$contact->LanguageID = $langue;
    $contact->Message = $msg_banquier . " \r\n " . "message";
    //$contact->Name = $nom;
    //$contact->Number = $rue_nbr;
    $contact->OfficeID = $officeID;
    $contact->PrivateEmail = $email;
    $contact->PrivateMobile = $numero;
    //$contact->PrivateTel = $tel_fix;
    $contact->SearchCriteria = $searchCriteria;
    $contact->StatusID = 1;
    $contact->Zip = $postal;

    // print_r($contactOrigins);
    // print_r($contactTitles);
    $theContact = post('UpdateContact', 'estateServiceUpdateContactRequest', $contact);
    $AtheContact = json_decode(json_decode($theContact, true), true);

    $errors = $AtheContact['d']['Errors'];
    if (empty($errors)) {
        echo json_encode(['status'=>'OK']);
    } else {
        echo json_encode(['status'=>'KO']);
    }

}
?>