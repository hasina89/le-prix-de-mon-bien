<h3>Signaler un abus</h3>
<div class="content">
    <p>Remplir le formulaire ci-dessous pour signaler l'abus que vous avez constaté.</p>
    <form id="form-abus">
        <div class="form-group w-100">
            <div class="select">
                <select name="sujet">
                    <option value="">Faites votre choix</option>
                    <option value="Harcèlement">Harcèlement</option>
                    <option value="Atteinte à votre image">Atteinte à votre image</option>
                    <option value="Tentative de captation de la clientèle">Tentative de captation de la clientèle</option>
                    <option value="Non respect du règlement">Non respect du règlement</option>
                    <option value="Autre(s)">Autre(s)</option>
                </select>
            </div>
        </div>
        <div class="form-group w-100 autre_sujet">
            <div class="champ">
                <input type="text" name="autre_sujet" placeholder="Votre sujet">
            </div>
        </div>
        <div class="form-group">
            <div class="champ">
                <input type="text" name="nom" placeholder="Nom">
            </div>
        </div>
        <div class="form-group">
            <div class="champ">
                <input type="text" name="prenom" placeholder="Prénom">
            </div>
        </div>
        <div class="form-group">
            <div class="champ">
                <input type="email" name="email" placeholder="Email">
            </div>
        </div>
        <div class="form-group">
            <div class="champ">
                <input type="text" name="telephone" placeholder="Téléphone">
            </div>
        </div>
        <div class="form-group w-100">
            <div class="champ">
                <textarea name="message" placeholder="Message"></textarea>
            </div>
        </div>
        <div class="submit-abus sipan">
            <input type="submit" name="submit-abus" value="Envoyer" class="submit">
        </div>
    </form>
</div>
<script type="text/javascript">
    jQuery("#form-abus select").change(function(){
        var value = jQuery(this).find("option:selected").attr("value");
        if(value=='Autre(s)'){
          jQuery('.autre_sujet').addClass('show');
        } else{ 
          jQuery('.autre_sujet').removeClass('show');
        }
    });
</script>