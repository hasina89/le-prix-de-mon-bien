<?php

/* BO Leads */

function pool_list_menu() {

    add_submenu_page( 

        'options-general.php',

        'Liste des Leads',

        'Liste des Leads',

        'manage_options',

        'simulation_leads',

        'user_pool_list_fx'

    );  

}

add_action('admin_menu', 'pool_list_menu');



// Insert Customer

function save_lead($email, $adresse, $code_postal, $ville, $rue ){

    global $wpdb;

    $leads_table = $wpdb->prefix.'leads';



    $wpdb->insert($leads_table, array(

        "email"       => $email,

        "adresse"     => $adresse,

        "code_postal" => $code_postal,

        "ville"       => $ville,

        "rue"         => $rue,

    ));

    return $wpdb->insert_id;

}



function user_pool_list_fx(){



    global $wpdb;

    $sql = "SELECT * FROM {$wpdb->prefix}leads";

    $leads = $wpdb->get_results($sql);  

    $total = count($leads);

    ?>

    <style>

        .liste_news .wp-list-table { width: 99% !important; }

        .liste_news .wp-list-table thead tr{ background: #eeecec !important; }

        .alternate, .striped > tbody > tr td { border-bottom: 1px solid #f1f1f1; }

        .alternate, .striped > tbody > :nth-child(2n+1), ul.striped > :nth-child(2n+1){ background-color: #f9f9f9 !important; }

        /*.alternate, .striped > tbody > tr td.unfinished { border-left: 5px solid #f00; border-bottom: 3px solid transparent; border-top: 3px solid transparent; }*/

        .alternate, .striped > tbody > tr td.finished { border-left: 5px solid #10b805; border-bottom: 3px solid transparent; border-top: 3px solid transparent; background: rgba(16, 184, 5, 0.16); }

    </style>

    <div class="wrap">

        <h1 class="wp-heading-inline">Liste de Simulations</h1>

        <p>Il y a <b><?= $total; ?></b> lead(s) dans votre base de données</p>

        <table class="wp-list-table widefat fixed striped">

            <thead>

                <tr>

                    <th>Email</th>

                    <th>Adresse</th>

                    <th>No de la rue</th>

                    <th>Code Postal</th>

                    <th>Ville</th>

                </tr>

            </thead>

            

            <tbody id="the-list">

                <?php 

	                if(!empty($leads)):

		                foreach ($leads as $lead) : 

							$lead_id     = $lead->id;

							$email       = $lead->email;

							$adresse     = $lead->adresse;

							$rue         = $lead->rue;

							$code_postal = $lead->code_postal;

							$ville       = $lead->ville;  

	                	?>            

	                    <tr>

	                        <td><?= $email; ?></td>

	                        <td><?= $adresse; ?></td>

	                        <td><?= $rue; ?></td>

	                        <td><?= $code_postal; ?></td>

	                        <td><?= $ville; ?></td>

	                    </tr>

	                <?php 

	            		endforeach; 

                	else: ?>

                	<tr>

                        <td colspan="5" align="center">Aucun lead trouvé :)</td>

                    </tr>

            	<?php endif; ?>

            </tbody>

        </table>

    </div>

<?php }


