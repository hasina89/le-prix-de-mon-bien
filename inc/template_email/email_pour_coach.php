<head>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;700;800&display=swap" rel="stylesheet">
    <style>
        body{
            font-family: 'Open Sans', sans-serif;
            color: #333;
        }
        span,p, a{
            color: #333;
        }        
        .color *{
            color:rgba(57,171,135,1);
        }
        img{
            image-rendering: -webkit-optimize-contrast;
        }
        @media (max-width: 500px){        
            .font-box{
                font-size: 12px !important;
            }
            .font-box2{
                font-size: 9px !important;
            }
            .box-l{
                padding: 50px 0 0 10px !important;
                background-size: 30px !important;
            }
            .box-r{
                padding: 30px 10px 20px 20px !important;
            }
            .mailContent{
                padding: 40px 20px !important;
            }
            .small-f{
                font-size: 18px !important;
            } 
            .mailContent span{
                font-size: 15px !important;
            }
            .mailContent h3{
                font-size: 16px !important;
            }
            .mailContent h2{
                font-size: 14px !important;
            }
            .right-reset{
                text-align: center !important;
                padding-right: 0 !important;
            }
            .right-reset img{
                margin: 0 auto !important;
            }
            .text-small{
                font-size: 12px !important;
            }
        }
    </style>
</head>
<body style="margin: 0;">
    <table align="center" style="max-width: 650px; margin: 0 auto; width: 100%; background: #fff;" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
        <tr style="text-align:center; background-image: url('https://leprixdemonbien.be/email.png'); background-repeat: no-repeat; background-position: center top; background-size: cover;">
            <td style="padding: 60px 20px 60px;">
                <table style="width: 100%">
                    <tr>
                        <td class="right-reset" style="text-align: right; padding-right: 50px;">
                            <img src="https://leprixdemonbien.be/logo-reverse.png" alt="" style="display: block; margin: 0 0 0 auto; width: 100%; max-width:221px;">
                            <br>
                            <h2 class="small-f" style="color:#ffffff;font-weight:700;font-size:18px">Vendre SEUL c'est bien,<br> COMME un PRO c'est mieux !</h2>
                        </td>
                </table>
            </td>
        </tr>
        <tr style="background-image: url('https://leprixdemonbien.be/bg.png'); background-repeat: no-repeat; background-position: center top; background-size: cover;">
            <td class='mailContent' style="padding:40px 20px 25px;line-height: 1.75; font-size: 15px; color: #333">
                <table style="width: 100%">
                    <tr>
                        <h3 style="color:rgba(57,171,135,1); margin-bottom: 0;  line-height: 1; font-size: 24px; margin-bottom: -8px;">Je désire être coaché.</h3>                        
                         <p style="line-height: 20px;">&nbsp;</p>
                        <p style="color:#2b7895;">Veuillez me recontactez.</p>
    
                        <p style="color:#2b7895;">Mes informations :</p>

                        <p style="color:#2b7895;">
                           <b>Nom :</b> <?= $nom ?><br>
                           <b>Prénom :</b> <?= $prenom ?><br>
                           <b>Email :</b> <?= $email ?><br>
                           <b>Téléphone :</b> <?= $tel ?><br>
                        </p>
                    </tr>
                    <!-- <tr>
                        <td style="padding-top: 5px;">
                            <img style="width: 100%; display: block; max-width: 604px; margin: 0 auto;" src="https://leprixdemonbien.be/steps-1.png" alt="">
                        </td> 
                    </tr> -->
                    <!-- <tr>
                        <td>
                            <img style="width: 100%; display: block; max-width: 604px; margin: 0 auto;" src="https://leprixdemonbien.be/steps-2.png" alt="">
                            </td> 
                     </tr> -->
                     <!-- <tr>
                        <td>
                            <img style="width: 100%; display: block; max-width: 604px; margin: 0 auto;" src="https://leprixdemonbien.be/steps-3.png" alt="">
                        </td> 
                     </tr> -->
                </table>
            </td>
        </tr>
        <!-- <tr style="background-image: url('https://leprixdemonbien.be/bg.png'); background-repeat: no-repeat; background-position: center top; background-size: cover;">
            <td>
                <table style="width: 100%;">
                    <tr>
                        <td style="color:rgba(57,171,135,1); text-align: center; padding-bottom: 60px;">
                            <div class="color" style="color:rgba(57,171,135,1); text-align: center;">
                                <strong>Multi diffusion de votre bien ! </strong><br>
                                <span>Le délais de 30 jours débute.</span><br><br>
                                <strong>Un bilan journalier de vos visites et un accompagnementdurant<br> toute la vente est mis en place !</strong><br><br>
                                <strong>Le bien est VENDU ? Félicitation !</strong><br>
                                <span>Nous dresserons le bilan de notre aventure ensemble !</span><br><br>
                                <strong>Le bien n'est pas vendu.</strong><br>
                                <span>Nous ne vous abandonnons pas,<br>nous avons toujours une solution pour vous !</span>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr> -->
        <!-- <tr style="background: linear-gradient(to right, rgba(231,231,231,1) 0%,rgba(248,248,248,1) 100%);">
            <td>
                <table style="width: 100%">
                    <tr>
                        <td class="box-l" style="padding: 50px 0 0 40px; width: 40%; background-image: url('https://leprixdemonbien.be/quote-p.png'); background-repeat: no-repeat; background-size: 50px; background-position: 100% 40px;"><img style="width: 100%; display: block;" src="https://leprixdemonbien.be/quote-side-email.png" alt=""></td>
                        <td class="box-r" style="padding: 30px 40px 20px 40px; width: 60%;">                                
                            <h2 class="font-box" style="margin-bottom: 24px; color:#2b7895; font-weight: 700; font-size: 20px; line-height:1.4;">J'ai choisi le pack Wi Love et en 17 jours j'avais le meilleur acheteur pour ma maison ! Merci Wicoach</h2>
                            <p class="font-box2" style="margin-bottom: 0; color: #2b7895; font-size: 12px;"><strong>STÉPHANIE</strong>-Braine-le-Comte</p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr> -->
       <!--  <tr style="background-image: url('https://leprixdemonbien.be/bg.png'); background-repeat: no-repeat; background-position: center top; background-size: cover;"><td style="padding: 20px;"></td></tr> -->
        <tr style="background-image: url('https://leprixdemonbien.be/bg.png'); background-repeat: no-repeat; background-position: center top; background-size: cover;">
            <td style="border-top: 1px solid #e3e3e3; padding: 25px 20px;">
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align:center;">
                            <img src="https://leprixdemonbien.be/logo-footer.png" alt="" style="width: 176px; margin: 0 auto 20px;">
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">
                            <span class="text-small" style="color:#2b7895; text-decoration: none; line-height: 2; font-size: 16px;">
                                <strong>Adresse :</strong> Boulevard du Souverain 24 - 1170 Bruxelles
                            </a>
                        </td>
                    </tr>
                    <!-- <tr>
                        <td style="text-align: center;">
                            <a class="text-small" href="tel:065879887" style="color:#2b7895; text-decoration: none; line-height: 1.5; font-size: 16px;">
                                <strong>Tél.:</strong> 065 87 98 87
                            </a>
                                - 
                            <a class="text-small" href="mailto:contact@wicoach.be" style="color:#2b7895; text-decoration: none;  line-height: 1.5; font-size: 16px;">
                                <strong>Email:</strong> contact@wicoach.be
                            </a>
                        </td>
                    </tr> -->
                </table>
            </td>
        </tr>
    </table>
</body>