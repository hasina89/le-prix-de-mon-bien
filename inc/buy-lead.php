<?php

add_action('wp_ajax_buy_lead', 'buy_lead');
add_action('wp_ajax_nopriv_buy_lead', 'buy_lead');

function buy_lead(){
	if(isset($_POST) && !empty($_POST)){
		global $woocommerce;

		$str = http_build_query($_POST);
    parse_str($str, $Data);

    extract($Data);

    if( $agent_name == '' ) wp_die('#');
    if( $paiement == '' ) wp_die('#');
    if( $propID == '' ) wp_die('#');

    $fiche_id = strip_tags( (int)$fiche_id );

    $default_payment_id = 'stripe';

    $paiement =  wp_strip_all_tags( $paiement );

    WC()->session->set( 'chosen_payment_method', $paiement );   

    $woocommerce->cart->empty_cart();

    ob_start();

    if( "no" == $buy_now ){
      $woocommerce->cart->add_to_cart( $fiche_id, 1 );
      $p = '&bn=no';
    }elseif( "yes" == $buy_now ){

      $now = new DateTime();

      $date_creation = get_field('field_creation', $propID ) ? get_field('field_creation', $propID ) : "01/01/2021 13:42";

      $date_creation_1 = new DateTime( $date_creation );
      $date_creation_1 = $date_creation_1->format('Y-m-d H:i:s');
      $date_creation_1 = new DateTime( $date_creation_1 );
      $_24h_apres_ajout = $date_creation_1->add( new DateInterval('P1D') );

      if( $now->diff( $_24h_apres_ajout )->invert > 0 ){
?>
        <div class="d-flex justify-content-center buy_now_expired">
          <h3>Désolé, le délai pour l'achat exclusif de ce bien est dépassé !</h3>
        </div>
        <div class="d-flex justify-content-center">
          <a href="<?= get_permalink($propID) ?>">Passer à l'achat normal</a>
        </div>
<?php
        $out = ob_get_clean();
        echo $out;
        wp_die();

      }

      $woocommerce->cart->add_to_cart( $fiche_id, 5 );
      $p = '&bn=yes';

    }else{
      wp_die('HAHAHAH');
    }

?>
    <iframe id="frame1" class="<?= $paiement ?>" src="<?= site_url('paiement') .'?method='.$paiement.$p ?>" style="width:100%; min-height: 100vh" onload="resizeIframe(this)"></iframe>
    <script>
      function resizeIframe(obj) {
        obj.style.height = obj.contentWindow.document.documentElement.scrollHeight + 100 + 'px';
      }
    </script>
<?php

    $out = ob_get_clean();
    echo $out;

	   wp_die();

  }
}
