<div class="listBiens" id="listBiens">
      <?php

        $now = new DateTime();

         $par_page = -1;

         $arg = array(
            'post_type'      => 'property',
            'posts_per_page' => $par_page,
            'paged'          => $paged,
            'meta_key'       => 'statut_transaction',
            'orderby'        => array(
               'meta_value' => 'ASC',
               'ID'         => 'DESC',
               
            ),
         );

         if( $statut_transaction ){
            $new_arg = array( 'meta_value' => $statut_transaction );

            $arg = $arg + $new_arg;
         }

         $q = new WP_Query( $arg );

         if( $q->have_posts() ){
            while( $q->have_posts() ){
               $q->the_post();

               $propID = get_the_ID();
               $fiche_cloturee = get_post_meta( get_the_ID(), 'fiche_cloturee', true );

               $statut = get_field('statut_transaction');
               if( $fiche_cloturee && 'Vendu' != $statut ){
                  update_field('statut_transaction','Sous option', $propID);
                  $statut = get_field('statut_transaction');
               }

               if( 'Vendu' == $statut ){
                  $color = '#a29f78 !important';
               }elseif( 'Sous option' == $statut ){
                  $color = '#9b9b9b !important';
               }else{
                  $color = '#00E29D !important';
               }


        $date_creation = get_field('field_creation' ) ? get_field('field_creation' ) : "01/01/2021 13:42";

        $date_creation_1 = new DateTime( $date_creation );
        $date_creation_1 = $date_creation_1->format('Y-m-d H:i:s');
        $date_creation_1 = new DateTime( $date_creation_1 );

        $date_creation_2 = new DateTime( $date_creation );
        $date_creation_2 = $date_creation_2->format('Y-m-d H:i:s');
        $date_creation_2 = new DateTime( $date_creation_2 );

        $_24h_apres_ajout = $date_creation_1->add( new DateInterval('P0DT24H') );


        $fin = $date_creation_2->add( new DateInterval('P2D') );
        $diff = $now->diff($fin);

         $etat_bien_class = "etat_biens d-flex justify-content-center";
         $item_misy_chrono = "item_sans_chrono";
         if( $diff->invert == 0 ){
            $etat_bien_class = 'etat_biens misy_chrono';
            $item_misy_chrono = "item_avec_chrono";
         }
      ?>

      <div class="item <?= 'Vendu' == $statut || 'Sous option' == $statut ? 'no_btn_vente':''; ?> <?= 'Vendu' == $statut ? 'container_vendu':''; ?> <?= $item_misy_chrono ?>">
          <div class="content" <?= 'Vendu' == $statut ? 'style="opacity:0.5"':''; ?>>
              <div class="top_biens">
                  <div class="blcImg">
                      
                          <?php
                             $existing_imgs = get_field('field_60e44529224ad', $propID);
                             if( is_array($existing_imgs) && count( $existing_imgs ) ){
                               $pic = $existing_imgs[0];
                             if( array_key_exists('image_large', $pic)):
                          ?>
                          <div class="img">
                             <img class="image" src="<?= $pic['image_large'] ?>">
                          </div>
                          <?php endif; }else{ ?>
                           <div class="img img_logo">
                             <img class="image logo_vdmb" src="<?= IMG_DIR . 'logo_vendez_partenaires.svg' ?>">
                          </div>
                          <?php } ?>
         
                      
                      <div class="info_top">
                           <?php 
                                $peb_img = 'peb-A.png';
                                if( get_field('niveau_peb', $propID) ) $peb_img = 'peb-'.get_field('niveau_peb', $propID).'.png';
                            ?>
                          <span class="peb"><img  class="img_peb" src="<?= IMG_DIR. $peb_img ?>"></span>
                          <div class="blcSous_option">
                              <span class="tag" style="background:<?= $color ?>">
                                 <?php echo $statut; ?>
                              </span>
                              <?php
                                $show_buynow_btn = false;

                                if( $statut != 'Vendu' && $statut != 'Sous option' ){
                                    if( $now->diff( $_24h_apres_ajout )->invert == 0 ){

                                      $was_bought_now = get_post_meta( $propID, 'was_bought_now', true );

                                      if( !$was_bought_now || ''==$was_bought_now ){

                                        $show_buynow_btn = true;

                                        $url = wp_nonce_url( get_permalink($propID), "nonce_buy_now", '_sc' );
                              ?>
                                        <a href="<?= $url ?>" class="buy_now" data-prop="<?= $propID ?>">buy now</a>
                              <?php      
                                      }
                                    }
                                } 
                              ?>
                          </div>
                      </div>
                      <?php if( 'Vendu' == $statut ){ ?>
                        <div class="bien_vendu">
                           <div class="titre_vendu">Bien vendu par</div> 
                           <div><img class="image_oblique" src="<?= IMG_DIR . 'logo_vendez_partenaires.svg' ?>"></div>
                        </div>
                     <?php } ?>
                  </div>
                  
                  <div class="<?= $etat_bien_class ?>">
                      <div class="left">
                           <?php
                              $fiche_id = get_current_product_id() ? get_current_product_id() : 0;
                              $sales = (int)get_post_meta( $fiche_id, 'total_sales', true );
                              $achat_max = 5;
                              $total_sales = $achat_max - $sales;

                               if( $total_sales < 0 ) {
                                  $total_sales = 0;
                               }
                            ?>
                          Il reste :<br> 
                          <?php echo $total_sales .'/5' ?> achat(s)
                      </div>
                      <?php if( $diff->invert == 0 ){ 
                        $minutes = ($diff->days * 24 * 60) + ($diff->h * 60) + $diff->i + ( $diff->s / 60);
                      ?>
                         <div class="right">
                             Terminé dans : <br> <span class="temps" data-minutes-left="<?= $minutes ?>"></span>
                         </div>
                     <?php } ?>
                  </div>
              </div>
              <div class="bottom_biens<?= $show_buynow_btn ? ' misy_buy_now':'' ?>">
                  <div class="blcPrix">
                      <div class="location"><?= get_field('categorie_batiment') ?> à <?= get_field('ville') ?></div>
                      <div class="prix">
                          Faire offre à partir de
                          <span><?= millier( get_field('prix_de_vente') ); ?>€</span>
                      </div>
                  </div>
                  <div class="blc_caract_biens">
                      <div class="blc">
                          <?php if (get_field('field_60e44a6bf62db', $propID)) { ?>
                              <span class="picto"><img  class="img_peb" src="<?= IMG_DIR.'icon-bed.png' ?>"></span>
                          <?php } ?>
                          <span><?= get_field('field_60e44a6bf62db', $propID) ?></span>
                      </div>
                      <div class="blc">
                          <span class="picto"><img  class="img_peb" src="<?= IMG_DIR.'icon-distance.png' ?>"></span>
                          <span>
                              <?php if( get_field('field_60e44899d4def', $propID) )
                                  echo get_field('field_60e44899d4def', $propID);
                              ?> <sup>m2</sup>
                          </span>
                      </div>
                  </div>
                  <?php 
                     $fiche_id = get_current_product_id() ? get_current_product_id() : 0;
                     $sales = (int)get_post_meta( $fiche_id , 'total_sales', true );
                     if( !$fiche_cloturee && 'Vendu' != $statut ): ?>
                      <div class="hide">
                        <?php
                          $texte_btn = $sales < 5 ? "vendre ce bien" : "accès complet";

                          if( $show_buynow_btn ){
                        ?>
                            <a href="<?= $url ?>" class="btn buy_now">buy now</a>
                        <?php } ?>

                          <a href="<?= get_permalink( get_the_ID() ) ?>" class="btn"><?= $texte_btn ?></a>

                      </div>
                  <?php endif; ?>
              </div>
          </div>
      </div>
      <?php 
            }

            wp_reset_postdata();
         }
      ?>

  </div>