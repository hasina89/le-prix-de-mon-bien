<?php

$showitems = ($range * 2)+1;

global $paged;

if(empty($paged)) $paged = 1;

$number = $paged - round($showitems/2); 

if($pages == '') {
    global $wp_query;

    $pages = $wp_query->max_num_pages;
    if(!$pages)
    {
        $pages = 1;
    }
}
if(1 != $pages) {

?>
<ul class='listPage'>
    <?php 

    	if($paged > 1 && $showitems < $pages){ 
    ?>
    		<li class='pager'><a href='<?= get_pagenum_link($paged - 1) ?>'>&lsaquo;</a></li>
    <?php 
		}

        if($paged > 2 && $paged > $range+1 && $showitems < $pages) {
    ?>
            <li class='pager'><span><a href='<?= get_pagenum_link(1) ?>'>1</a></span></li>
    <?php 
        }

		// if( $number > 1 ) echo "<li class='pager'>...</li>";

    for ($i=1; $i <= $pages; $i++) {
        if ( 1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ) ){
        	if( $paged == $i){
    ?>
    			<li class="pager active"><span><a class='page-link'><?= $i ?></a></span></li>
    <?php
        	}else{
    ?>
    			<li class='pager'>
    				<span><a href='<?= get_pagenum_link($i) ?>' class="page-link"><?= $i ?></a></span>
    			</li>
    <?php
        	}
    
        }
    }

    // if( $number < $pages && $paged < $pages-1) echo "<li class='pager'>...</li>";

    if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages){
    ?>
    	<li class='pager'><span><a class='page-link' href='<?= get_pagenum_link( $pages ) ?>'><?= $pages ?></a></span></li>
    <?php
    }
    if ($paged < $pages && $showitems < $pages){
    ?>
        <li class='pager'><span><a class='page-link' href="<?= get_pagenum_link($paged + 1) ?>">&rsaquo;</a></span></li>
    <?php
    }
    ?>

</ul>
<?php } ?>