<?php 
	
	add_action( 'wp_ajax_coupon_check_via_ajax', 'coupon_check_via_ajax' ); 
	add_action( 'wp_ajax_nopriv_coupon_check_via_ajax', 'coupon_check_via_ajax' );

	function coupon_check_via_ajax(){ 
		$code = strtolower(trim($_POST['code']));

		$coupon = new WC_Coupon($code); 
		$coupon_post = get_post($coupon->id); 

		if(!empty($coupon_post) && $coupon_post != null){ 
			$status = 0; 
			if($coupon_post->post_status == 'publish'){ 
				$status = 1; 
			} 
		}else{ 
			$status = 0; 
		} 
		print json_encode( 
			[ 'status' => $status ] 
		); 

		exit(); 
	} 

	
?>