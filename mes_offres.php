<?php
$current_user = wp_get_current_user();
 
$customer_offers = get_posts( array(
    'numberposts' => -1,
    'author'  		=> $current_user->ID,
    'post_type'   => 'offre',
    'post_status' => 'publish',
) );

// var_dump( $customer_offers );

?>
<div class="content_offres">
    <div class="sec_offre">
        <h2 class="titre_offre">Mes Offres</h2>
        <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table">
            <thead>
                <tr>
                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-number"><span class="nobr">Nom et Réf. du bien</span></th>
                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-date"><span class="nobr">Date</span></th>
                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-total"><span class="nobr">Prix</span></th>
                    <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions"><span class="nobr">Actions</span></th>
                </tr>
            </thead>

            <tbody>

                <?php 
                    if(is_array( $customer_offers ) ): 
                        foreach( $customer_offers as $offer ):
                            $nom_ref = get_the_title( get_field('id_du_bien', $offer->ID ) );
                            $link = get_the_permalink( get_field('id_du_bien', $offer->ID ) );
                ?>

                            <tr class="woocommerce-orders-table__row ">
                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number" data-title="Commande"><a href="<?= $link ?>"><?= $nom_ref ?></a></td>
                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-date" data-title="Date"><?= get_field('date_de_loffre', $offer->ID) ?></td>
                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-total" data-title="Total"><span class="woocommerce-Price-amount amount"><?= millier( get_field('prix_de_loffre', $offer->ID) ) ?><span class="woocommerce-Price-currencySymbol">€</span></span></td>
                                <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions" data-title="Actions">
                                    <div class="item_btn">
                                        <a href="<?= get_field('doc_offre', $offer->ID) ?>" target="_blank" class="boutton_action print_order">Document offre</a>
                                    </div>
                                    <div class="item_btn">
                                        <a href="<?= get_field('carte_didentite', $offer->ID) ?>" class="boutton_action print_order">Carte d'identité</a>
                                    </div>
                                </td> 
                            </tr>

                        <?php endforeach; ?>
                <?php endif; ?>
            </tbody>
        </table>
        <!-- <div class="woocommerce-pagination woocommerce-pagination--without-numbers woocommerce-Pagination">
            <a class="woocommerce-button woocommerce-button--next woocommerce-Button woocommerce-Button--next button" href="#">Suivant</a>
        </div> -->
    </div>
  <div>