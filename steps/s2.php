<!-- Step 1 -->
<input type="hidden" name="prev_step" value="1">
<input type="hidden" name="curr_step" value="2">
<div class="tab current">
    <div class="estimation-form">
        <div class="titleStep text-left">
            <div class="step d-flex justify-content-start align-items-center">
                <a class="precedent"></a>
                <span><strong>2</strong> / 10 Espaces</span>
            </div>
            <h3>Quels sont les espaces disponibles&nbsp;?</h3>
        </div>
    </div>
    <div class="row_area">
        <div>
            <div class="estimation-row component-has-garden-area d-flex align-items-center justify-content-start">
                <div class="estimation-row__icon">
                    <img src="<?= IMG_DIR ?>terrace.svg" alt="icon">
                </div>
                <div class="estimation-row__label">
                    <div class="d-flex align-items-center justify-content-between">
                        <label>Y a-t-il un jardin ?</label>
                        <div class="estimation-row__input estimation-row__input--small">
                            <label for="gardenExist" class="component-input-switch input--switch ">
                                <input type="checkbox" id="gardenExist" name="garden_exist" value="Oui" class="input--switch__input">
                                <span class="input--switch__label"></span>
                            </label>
                        </div>
                    </div>
                    <div class="jardin estimation-row__subfield estimation-row__subfield--full-icon marge">
                        <div class="estimation-row__subfield__label estimation-row__subfield__label--full">
                            <label>Quelle est la superficie du jardin en m² ?</label>
                        </div>
                        <div class="estimation-row__subfield__input estimation-row__subfield__input--full">
                            <div class="component-input-range-with-value input-range-with-value d-flex justify-content-between align-items-center">
                                <div class="input-range-with-value__slider range-wrap">
                                    <div class="range-value field" id="gardenSurface"></div>
                                    <input id="gardenSurface_input" name="gardenSurface" type="range" min="0" max="10000" tabindex="0" class="input--range__input" value="<?= $t= sess('gardenSurface') != '' ? sess('gardenSurface') : '0' ?>" step="1">
                                </div>
                                <div class="input-range-with-value__input">
                                    <div class="field">
                                        <div class="input-group input-group--append">
                                            <input id="gardenOutput" type="number" name="gardenSurfaceOutput" min="1" max="10000" value="<?= $t= sess('gardenSurfaceOutput') != '' ? sess('gardenSurfaceOutput') : '0' ?>" class="input--text input-group__main input-group__main--append">
                                            <div id="surfaceOutput_front2" class="output"></div>
                                            <div class="input-group__append">
                                                <abbr class="input-group__abbr">m²</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!---->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="estimation-row component-has-garden-area d-flex align-items-center justify-content-start">
                <div class="estimation-row__icon">
                    <img src="<?= IMG_DIR ?>surfaceterrasse.svg" alt="icon">
                </div>
                <div class="estimation-row__label">
                    <div class="d-flex align-items-center justify-content-between">
                        <label>Y a-t-il une terrasse ?</label>
                        <div class="estimation-row__input estimation-row__input--small">
                            <label for="terrasse" class="component-input-switch input--switch ">
                                <input type="checkbox" id="terrasse" name="terrasse" value="Oui" class="input--switch__input">
                                <span class="input--switch__label"></span>
                            </label>
                        </div>
                    </div>
                    <div class="terrasse estimation-row__subfield estimation-row__subfield--full-icon marge">
                        <div class="estimation-row__subfield__label estimation-row__subfield__label--full">
                            <label>Quelle est la superficie de la terrasse en m² ?</label>
                        </div>
                        <div class="estimation-row__subfield__input estimation-row__subfield__input--full">
                            <div class="component-input-range-with-value input-range-with-value d-flex justify-content-between align-items-center">
                                <div class="input-range-with-value__slider range-wrap">
                                    <div class="range-value field" id="terrasseSurface"></div>
                                    <input id="terrasseSurface_input" name="terraseSurface" type="range" min="0" max="2000" tabindex="0" class="input--range__input" value="<?= $t= sess('terraseSurface') != '' ? sess('terraseSurface') : '0' ?>" step="1">
                                </div>
                                <div class="input-range-with-value__input">
                                    <div class="field">
                                        <div class="input-group input-group--append">
                                            <input id="terrasseOutput" type="number" name="surfaceTerrasse" value="<?= $t= sess('surfaceTerrasse') != '' ? sess('surfaceTerrasse') : '0' ?>" class="input--text input-group__main input-group__main--append" max="2000">
                                            <div id="surfaceOutput_front3" class="output"></div>
                                            <div class="input-group__append">
                                                <abbr class="input-group__abbr">m²</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!---->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="estimation-row component-has-garden-area d-flex align-items-center justify-content-start">
                <div class="estimation-row__icon">
                    <img src="<?= IMG_DIR ?>grenier.svg" alt="icon">
                </div>
                <div class="estimation-row__label">
                    <div class="d-flex align-items-center justify-content-between">
                        <label>Y a-t-il un grenier ?</label>
                        <div class="estimation-row__input estimation-row__input--small">
                            <label for="grenier" class="component-input-switch input--switch ">
                                <input type="checkbox" id="grenier" name="grenier" value="Oui" class="input--switch__input">
                                <span class="input--switch__label"></span>
                            </label>
                        </div>
                    </div>
                    <div class="grenier estimation-row__subfield estimation-row__subfield--full-icon marge">
                        <div class="estimation-row__subfield__label estimation-row__subfield__label--full">
                            <label>Quelle est la superficie du grenier en m² ?</label>
                        </div>
                        <div class="estimation-row__subfield__input estimation-row__subfield__input--full">
                            <div class="component-input-range-with-value input-range-with-value d-flex justify-content-between align-items-center">
                                <div class="input-range-with-value__slider range-wrap">
                                    <div class="range-value field" id="grenierSuperficy"></div>
                                    <input id="grenierSuperficy_input" name="grenierSuperficie" type="range" min="0" max="2000" tabindex="0" class="input--range__input" value="<?= $t= sess('grenierSuperficie') != '' ? sess('grenierSuperficie') : '0' ?>" step="1">
                                </div>
                                <div class="input-range-with-value__input">
                                    <div class="field">
                                        <div class="input-group input-group--append">
                                            <input id="grenierOutput" type="number" name="grenierSurface" value="<?= $t= sess('grenierSuperficie') != '' ? sess('grenierSuperficie') : '0' ?>" class="input--text input-group__main input-group__main--append" max="2000">
                                            <div id="surfaceOutput_front4" class="output"></div>
                                            <div class="input-group__append">
                                                <abbr class="input-group__abbr">m²</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div><!---->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>   
        <div class="estimation-row upload-file">
            <div class="col_left">
                <label> Pour plus de précision vous pouvez ajouter une photo de la façade avant , façade arrière , jardin et des éléments qui mettent le bien en valeur .</label>
            </div>
            <div class="col_right">
                <div class="chp">
                    <div class="cont-file">
                        <div class="content_cont_file">
                            <input type="file" name="file[]" class="input-file" id="imgInp">
                            <div class="content-preview"><img  class="imgPreview" id="img_init"></div>
                            <div class="listBoutton">
                                <i> Charger</i>
                                <i class="reset" style="display: none">Supprimer</i>
                            </div>
                        </div>
                         <div class="more">Ajouter une autre photo</div>
                    </div>
                    <span id="spn_inputs"></span>
                </div>
                <div class="info">
                    <p><a href="javascript:void(0);" id="more_files"></a></p>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="estimation-row component-email component-charge text-left">
            <div class="estimation-row total component-has-garden-area d-flex align-items-center justify-content-start">
                <div class="estimation-row__icon">
                    <img src="<?= IMG_DIR ?>chargetotale.svg" alt="icon">
                </div>
                <div class="estimation-row__label estimation-row__label--space">
                    <label>Charge totale mensuelle</label>
                </div>
            </div>
            <div class="estimation-row__input estimation-row__input--large">
                <div class="input-group input-group--append">
                    <input id="component-email__input-text" name="charge" type="number" placeholder="0" class="input--text input-text" value="<?= sess('charge') ?>">
                   <div class="input-group__append">
                        <abbr class="input-group__abbr">€</abbr>
                    </div>
                </div>
            </div>
            <div class="estimation-row__error estimation-row__error--space" style="display: none;"><!----></div>
        </div>
    </div>
</div>
<div class="blcBoutton btnForm clr btnForm float-right">
    <button type="button" id="prevBtn" class="precedent bouton btn misy_scrol">< étape
        précédente
    </button>
    <span class="sipan">
        <button type="submit" id="nextBtn" class="bouton btn misy_scrol">étape suivante ></button>
    </span>
</div>
<?php
    if( sess('terasse') == "Oui" ) 
        echo '<script>setTimeout(function(){$("#terrasse").trigger("click")},2000)</script>';

    if( sess('grenier') == "Oui"  ) 
        echo '<script>setTimeout(function(){$("#grenier").trigger("click")},2000)</script>';

    if( sess('garden_exist') == "Oui") 
        echo '<script>setTimeout(function(){$("#gardenExist").trigger("click")},2000)</script>';
?>
<script type="text/javascript">

    $('#terrasseSurface_input').on('input', function() {
        let val = $(this).val();
        $('#terrasseOutput').val(val);
    });

    $('#terrasseOutput').on('input', function(){
      //console.log($(this).val())
      $('#terrasseSurface_input').val($(this).val())
    });


    $('#grenierSuperficy_input').on('input', function() {
        let val = $(this).val();
        $('#grenierOutput').val(val);
    });

    $('#grenierOutput').on('input', function(){
      //console.log($(this).val())
      $('#grenierSuperficy_input').val($(this).val())
    });

    /* Show Hidden */
    $('#gardenExist').change(function(){
        if (this.checked) {
            $('.jardin').fadeIn('slow');
        }
        else {
            $('.jardin').fadeOut('slow');
        }
    });

    $('#gardenSurface_input').on('input', function() {
        let val = $(this).val();
        $('#gardenOutput').val(val);
    });

    $('#gardenOutput').on('input', function(){
      //console.log($(this).val())
      $('#gardenSurface_input').val($(this).val())
    });

    /* - or + value */
    $(".input-group--stepper button").on("click", function() {
        var $button = $(this);
        var oldValue = $button.parent().find("input").val();

        var btnTXT = $button.text();
        var trimBtnTXT = $.trim(btnTXT);

        if (trimBtnTXT == "+") {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            if (oldValue > 1) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }
        $button.parent().find("input").val(newVal);
        return false;
    });

    /* Show Hidden */
    $('#gardenExist').change(function(){
        if (this.checked) {
            $('.jardin').fadeIn('slow');
        }
        else {
            $('.jardin').fadeOut('slow');
        }
    });

    $('#terrasse').change(function(){
        if (this.checked) {
            $('.terrasse').fadeIn('slow');
        }
        else {
            $('.terrasse').fadeOut('slow');
        }
    });

    $('#grenier').change(function(){
        if (this.checked) {
            $('.grenier').fadeIn('slow');
        }
        else {
            $('.grenier').fadeOut('slow');
        }
    });
</script>
<script>
    function readURL(input, id = '#img_init') {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(id).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }
    $("#imgInp").change(function() {
        readURL(this);
    });

    function getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }
    $('#more_files').on('click', function() {
        // fileInit();

        var input_id = 'ip_' + getRandomInt(9999999);
        var img_class = 'img_' + getRandomInt(9999999);
        var input_id_sharp = '#' + input_id;

        $('.upload-file .chp').append('<div class="cont-file cont-file_parent"><div class="content_cont_file"><input type="file"  class="input-file ' + input_id + '" id= "' + input_id + '" name="file[]"><div class="content-preview"><img class="imgPreview ' + img_class + '"></div><div class="listBoutton"><i> Charger</i><i class="reset" style="display: none">Supprimer</i></div></div><div class="more">Ajouter une autre photo</div></div>');

        var imgNextID_dyn = '.' + img_class;
        $(input_id_sharp).change(function() {
            readURL(this, imgNextID_dyn);
        });

        fileInitOter();
        reset_file_upload();
        more_file();
        $('.cont-file').each(function() {
            var index = $(this).index();
            var img = $(this).find(".imgPreview");
            img.attr('id', index);
            var input = $(this).find(".input-file");
        });

    });
    fileInit();
    fileInitOter();
    reset_file_upload();
    more_file();


    function handleFileSelect(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('.cont-file').each(function() {
                    imgId = '#' + $('.imgPreview').attr('id');
                    $(this).find(imgId).attr('src', e.target.result);
                });
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


    $("input[type='file']").change(function() {
        handleFileSelect(this);
    });

    function fileInit() {
        $('.input-file').change(function() {
            iz = $(this)
            val = iz.val()
            par = iz.parent(".cont-file")
            txtExt = par.find("span")
            txtBroswer = par.find(".listBoutton i")
            txtExt.text(val)
            // txtBroswer.text("Modifier");
            txtBroswer.css('display', 'none');
            var index = $(this).parents(".cont-file").index();
            var img = $(this).siblings(".imgPreview");
            img.attr('id', index);

            // iz.hide();
            txtExt.hide();

            imgPreview = iz.siblings('.content-preview').find('.imgPreview');
            imgPreview_parent = imgPreview.parents('.content-preview');
            imgPreview.addClass('shown');
            imgPreview_parent.addClass('shown');

        });
    }

    function fileInitOter() {
        $('.input-file').each(function() {
            $(this).change(function() {
                $(this).parents(".cont-file").addClass('uploaded');

                iz = $(this)
                resetBtn = iz.siblings('.listBoutton').find('.reset');
                imgPreview = iz.siblings('.content-preview').find('.imgPreview');
                imgPreview_parent = imgPreview.parents('.content-preview');
                imgPreview.addClass('shown');
                imgPreview_parent.addClass('shown');
                val = iz.val()
                if (val != "") {
                    par = iz.parent(".content_cont_file")
                    txtExt = par.find("span")
                    txtBroswer = par.find(".listBoutton i")
                    par.siblings('.more').addClass('show')
                    txtExt.text(val)
                    // txtBroswer.text("Modifier")
                    txtBroswer.css('display', 'none');
                    resetBtn.show()
                    resetBtn.text('Supprimer');

                    // iz.hide();
                    txtExt.hide();
                }

            })
        })
    }

    function reset_file_upload() {
        $('.reset').each(function() {
            $('.reset').on('click', function() {
                reset = $(this);
                reset.parents('.cont-file_parent').remove();
                btnChanger = $(this).siblings('i');
                inputFile = $(this).parents('.content_cont_file').find('.input-file');
                fakeText = $(this).siblings('span');
                imgPreview = $(this).parents('.listBoutton').siblings('.content-preview').find('.imgPreview');
                imgPreview_parent = imgPreview.parents('.content-preview');
                btnChanger.text("Charger");
                inputFile.val('');

                inputFile.show();
                fakeText.show();
                $('.more').removeClass('show');
                btnChanger.css('display', 'flex');


                fakeText.text("Aucun fichier sélectionné");

                imgPreview.attr('src', '');
                imgPreview.removeClass('shown');
                imgPreview_parent.removeClass('shown');

                if (reset.parents('#spn_inputs').length == 1) {
                    reset.parent('.cont-file').remove();
                } else {
                    reset.hide();

                }
                return false;
            });
        });
    }

    function more_file() {
        $('.more').each(function() {
            $(this).click(function() {
                $('#more_files').trigger('click');
                $(this).removeClass('show')

            })
        })
    }

    $('.precedent').on('click', function() {
        var prev_step = $('[name=prev_step]').val();
        var curr_step = $('[name=curr_step]').val();
        var oldHtml = $('#form_content').html();
        $.ajax(ajaxurl, {
            type: 'POST',
            data: 'action=back_from_' + curr_step + '&show=' + prev_step,
            dataType: 'html',
            success: function(resp) {
                $('#form_content').html(resp);

                // $('html, body').animate({
                // scrollTop: $(".top-step").offset().top
                // }, 1000)              
            }
        });

        return false;
    });
</script>