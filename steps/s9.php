<!-- Step 9 -->
<input type="hidden" name="prev_step" value="8">
<input type="hidden" name="curr_step" value="9">
<div class="tab tab9 current etape_apropos_de_vous">
    <div class="estimation-form">
        <div class="titleStep text-left">
            <div class="step d-flex justify-content-start align-items-center">
                <a class="precedent"></a>
                <span><strong>9</strong> / 10 Vos informations</span>
            </div>
            <h3>À propos de vous...</h3>
        <div class="estimation-row component-email text-left">
           
            <!-- manomboka eto -->
            <div class="row_step">
                <div class="component-consent component-email__consent">
                    <div class="component-consent__label">
                        <label class="field__label">En envoyant ma demande de contact, je déclare accepter que mes données complétées dans ce formulaire soient utilisées pour les buts mentionnés.</label>
                    </div>
                    <div class="component-consent__input">
                        <div class="component-check-box field">
                            <div class="input--checkbox">
                                <input id="agree" type="radio" class="input--checkbox__input" checked name="rgpd" value="Oui">
                                <label for="agree" class="input--checkbox__label">
                                    J'accepte
                                    <span data-v-d0628c3a="" class="component-tooltip tooltip">
                                            <a data-v-d0628c3a=""
                                               class="component-tooltip tooltip__button">
                                                <svg width="16" height="16" viewBox="0 0 20 20"
                                                     fill="none" xmlns="http://www.w3.org/2000/svg"
                                                     data-v-d0628c3a="">
                                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                                          d="M10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM9 5V7H11V5H9ZM11 14C11 14.55 10.55 15 10 15C9.45 15 9 14.55 9 14V10C9 9.45 9.45 9 10 9C10.55 9 11 9.45 11 10V14ZM2 10C2 14.41 5.59 18 10 18C14.41 18 18 14.41 18 10C18 5.59 14.41 2 10 2C5.59 2 2 5.59 2 10Z"
                                                          fill="#6288B9"></path>
                                                </svg>
                                            </a>
                                            <span data-v-d0628c3a=""
                                                  class="component-tooltip tooltip__popin"
                                                  style="left: 0px;">
                                                <span data-v-d0628c3a=""
                                                      class="component-tooltip tooltip__text">Vous recevrez des conseils pour vous aider à mieux suivre l’évolution du marché et à préparer vos futurs projets (immobiliers).</span>
                                            </span>
                                    </span>
                                </label>
                            </div>
                        </div>
                        <div class="component-check-box field">
                            <div class="input--checkbox">
                                <input id="desagree" type="radio" class="input--checkbox__input" name="rgpd" value="Oui">
                                <label for="desagree" class="input--checkbox__label">
                                    Je n'accepte pas
                                    <span data-v-d0628c3a="" class="component-tooltip tooltip">
                                            <a data-v-d0628c3a=""
                                               class="component-tooltip tooltip__button">
                                                <svg width="16" height="16" viewBox="0 0 20 20"
                                                     fill="none" xmlns="http://www.w3.org/2000/svg"
                                                     data-v-d0628c3a="">
                                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                                          d="M10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM9 5V7H11V5H9ZM11 14C11 14.55 10.55 15 10 15C9.45 15 9 14.55 9 14V10C9 9.45 9.45 9 10 9C10.55 9 11 9.45 11 10V14ZM2 10C2 14.41 5.59 18 10 18C14.41 18 18 14.41 18 10C18 5.59 14.41 2 10 2C5.59 2 2 5.59 2 10Z"
                                                          fill="#6288B9"></path>
                                                </svg>
                                            </a>
                                            <span data-v-d0628c3a=""
                                                  class="component-tooltip tooltip__popin">
                                                <span data-v-d0628c3a=""
                                                      class="component-tooltip tooltip__text">Vous recevrez des conseils pour vous aider à acheter un bien immobilier.</span>
                                            </span>
                                    </span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row_step">
                <div class="component-consent component-email__consent">
                    <div class="component-consent__label">
                        <label class="field__label">Sur une échelle de 0 à 10, quelle est votre motivation à vendre votre bien?</label>
                    </div>
                    <div class="component-consent__input">
                        
                        <div class="listItem">
                            <div class="item-evaluation">
                                <input id="un" type="checkbox" class="evaluation" name="motiv" value="1"
                                <?php if( sess('motiv') == '1' ) echo 'checked'; ?>
                                >
                                <label for="un">1</label>
                            </div>
                            <div class="item-evaluation">
                                <input id="deux" type="checkbox" class="evaluation" name="motiv" value="2">
                                <?php if( sess('motiv') == '2') echo 'checked'; ?>
                                >
                                <label for="deux">2</label>
                            </div>
                            <div class="item-evaluation">
                                <input id="trois" type="checkbox" class="evaluation" name="motiv" value="3"
                                <?php if( sess('motiv') == '3') echo 'checked'; ?>
                                >
                                <label for="trois">3</label>
                            </div>
                            <div class="item-evaluation">
                                <input id="quatre" type="checkbox" class="evaluation" name="motiv" value="4"                                 <?php if( sess('motiv') == '4') echo 'checked'; ?>
                                >
                                <label for="quatre">4</label>
                            </div>
                            <div class="item-evaluation">
                                <input id="cinq" type="checkbox" class="evaluation" name="motiv" value="5"
                                <?php if( sess('motiv') == '5') echo 'checked'; ?>
                                >
                                <label for="cinq">5</label>
                            </div>
                             <div class="item-evaluation">
                                <input id="six" type="checkbox" class="evaluation" name="motiv" value="6"
                                <?php if( sess('motiv') == '6') echo 'checked'; ?>
                                >
                                <label for="six">6</label>
                            </div>
                            <div class="item-evaluation">
                                <input id="sept" type="checkbox" class="evaluation" name="motiv" value="7"
                                <?php if( sess('motiv') == '7') echo 'checked'; ?>
                                >
                                <label for="sept">7</label>
                            </div>
                            <div class="item-evaluation">
                                <input id="huit" type="checkbox" class="evaluation" name="motiv" value="8"
                                <?php if( sess('motiv') == '8') echo 'checked'; ?>
                                >
                                <label for="huit">8</label>
                            </div>
                            <div class="item-evaluation">
                                <input id="neuf" type="checkbox" class="evaluation" name="motiv" value="9"
                                <?php if( sess('motiv') == '9') echo 'checked'; ?>
                                >
                                <label for="neuf">9</label>
                            </div>
                            <div class="item-evaluation">
                                <input id="dix" type="checkbox" class="evaluation" name="motiv" value="10">
                                <?php if( sess('motiv') == '10') echo 'checked'; ?>
                                >
                                <label for="dix">10</label>
                            </div>                            
                        </div>                       
                    </div>
                     <input type="text" name="fake_motiv" class="fake_motiv" readonly required 
                        <?php if( sess('motiv') != '' ) echo "value='filled'" ?>>
                </div>
            </div>
        </div>
        <div>
            <div class="row_step">
                <div class="estimation-row component-email component-charge text-left">
                    <div class="estimation-row total component-has-garden-area d-flex align-items-center justify-content-start">
                        <div class="estimation-row__label estimation-row__label--space">
                            <label>Quelle est la valeur de votre bien ?</label>
                        </div>
                    </div>
                    <div class="estimation-row__input estimation-row__input--large">
                        <div class="input-group input-group--append">
                          <?php $obj = sess('envisage_vendre_bien') ? sess('envisage_vendre_bien') : '' ?>
                            <input id="component-email__input-text" name="prixvente" type="number" placeholder="0" class="input--text input-text" value="<?= $obj ?>">
                           <div class="input-group__append">
                                <abbr class="input-group__abbr">€</abbr>
                            </div>
                        </div>
                    </div>
                    <div class="estimation-row__error estimation-row__error--space" style="display: none;"><!----></div>
                </div>
            </div>
            <div class="component-consent component-acceptance">
                <div class="component-consent__input">
                    <div class="component-check-box field">
                        <div class="input--checkbox">
                            <input id="acceptance" type="checkbox"
                                   class="input--checkbox__input" name="acceptement" value="Oui" required>
                            <label for="acceptance"
                                   class="input--checkbox__label">Je certifie que les informations mentionnées sont réelles
                                <span data-v-d0628c3a="" class="component-tooltip tooltip">
                                        <a data-v-d0628c3a=""
                                           class="component-tooltip tooltip__button">
                                            <svg width="16" height="16" viewBox="0 0 20 20"
                                                 fill="none" xmlns="http://www.w3.org/2000/svg"
                                                 data-v-d0628c3a="">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                      d="M10 0C4.48 0 0 4.48 0 10C0 15.52 4.48 20 10 20C15.52 20 20 15.52 20 10C20 4.48 15.52 0 10 0ZM9 5V7H11V5H9ZM11 14C11 14.55 10.55 15 10 15C9.45 15 9 14.55 9 14V10C9 9.45 9.45 9 10 9C10.55 9 11 9.45 11 10V14ZM2 10C2 14.41 5.59 18 10 18C14.41 18 18 14.41 18 10C18 5.59 14.41 2 10 2C5.59 2 2 5.59 2 10Z"
                                                      fill="#6288B9"></path>
                                            </svg>
                                        </a>
                                </span>
                            </label>
                        </div>
                    </div>
                </div>
                <p class="component-consent__disclaimer">Nous sommes soucieux de vos données et
                            de leur sécurité, elles ne seront ni partagées, ni vendues. Leur usage reste
                            exclusivement chez "vendezmonbien.be".</p>
            </div>
        </div>
        
    </div>
</div>
<div class="blcBoutton btnForm clr btnForm float-right">
    <button type="button" id="prevBtn" class="precedent bouton btn misy_scrol">< étape
        précédente
    </button>
    <span class="sipan">
        <button type="submit" id="nextBtn" class="bouton btn misy_scrol">Recevoir son rapport</button>
    </span>
</div>
<?php 
  if( sess('motiv') == '1' )
    echo '<script>setTimeout(function(){$("#un").trigger("click")},2000)</script>'; 
    if( sess('motiv') == '2' )
    echo '<script>setTimeout(function(){$("#deux").trigger("click")},2000)</script>'; 
    if( sess('motiv') == '3' )
    echo '<script>setTimeout(function(){$("#trois").trigger("click")},2000)</script>'; 
    if( sess('motiv') == '4' )
    echo '<script>setTimeout(function(){$("#quatre").trigger("click")},2000)</script>'; 
    if( sess('motiv') == '5' )
    echo '<script>setTimeout(function(){$("#cinq").trigger("click")},2000)</script>'; 
    if( sess('motiv') == '6' )
    echo '<script>setTimeout(function(){$("#six").trigger("click")},2000)</script>'; 
    if( sess('motiv') == '7' )
    echo '<script>setTimeout(function(){$("#sept").trigger("click")},2000)</script>'; 
    if( sess('motiv') == '8' )
    echo '<script>setTimeout(function(){$("#huit").trigger("click")},2000)</script>'; 
    if( sess('motiv') == '9' )
    echo '<script>setTimeout(function(){$("#neuf").trigger("click")},2000)</script>'; 
    if( sess('motiv') == '10' )
    echo '<script>setTimeout(function(){$("#dix").trigger("click")},2000)</script>';     
    
    ?>


<script>
  $(".evaluation").click(function(){
    var vis = $(this);
    vis_parent = vis.parents();
    vis_parent_prev = vis_parent.prevAll();
    vis_parent_prev.addClass('checked');
    vis_parent_next = vis_parent.nextAll();
    vis_parent_next.removeClass('checked');
    vis.attr('checked',true);
    vis_parent.addClass('checked');
    $('[name=fake_motiv]').val("filled");
  });
</script>
<script>
  $('.precedent').on('click', function(){
    var prev_step = $('[name=prev_step]').val();
    var curr_step = $('[name=curr_step]').val();
    var oldHtml = $('#form_content').html();
    $.ajax(ajaxurl, {
      type: 'POST',
      data: 'action=back_from_' + curr_step +'&show='+prev_step,
      dataType: 'html',
      success: function(resp){
        $('#form_content').html( resp);
        console.log(resp);
        // $('html, body').animate({
        // scrollTop: $(".top-step").offset().top
        // }, 1000)              
      }
    });

    return false;
})
</script>
<script>
  $('[name=proprietaire_bien]').click(function(){
    if( $('[name=proprietaire_bien]:checked').val() == 'Oui' || $('[name=proprietaire_bien]:checked').val() == 'Non' ){
      $('[name=fake_proprio').val('filled');
    }else{
      $('[name=fake_proprio').val();
    }
  })


</script>