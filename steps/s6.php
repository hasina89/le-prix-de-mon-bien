<!-- Step 7 -->
<input type="hidden" name="prev_step" value="5">
<input type="hidden" name="curr_step" value="6">

<div class="tab tab4 current">
    <div class="estimation-form">
        <div class="titleStep text-left">
            <div class="step d-flex justify-content-start align-items-center">
                <a class="precedent"></a>
                <span><strong>6</strong> / 10 Spécifications</span>
            </div>
            <h3>Nuisances eventuelles</h3>
        </div>
        <div class="item">
            <div class="estimation-row component-property-condition">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>nuissance.svg" alt="icon">
                    </div>
                    <label>Nuisances sonores</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon text-left">
                    <div class="input--radio">
                        <input type="radio" value="Aucune" id="aucune-nuis-sonore"
                               name="nuisance" class="input--radio__input batiment restore"
                               <?php if( !sess('nuisance') || sess('nuisance') == '' || sess('nuisance') == "Aucune") echo "checked" ?>
                               >
                        <label for="aucune-nuis-sonore" class="input--radio__label">Aucune</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="faible" name="nuisance" value="Faible"
                               class="input--radio__input batiment"
                               <?php if( sess('nuisance') == "Faible") echo " checked" ?>
                               >
                        <label for="faible" class="input--radio__label">Faible</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="moyenne" name="nuisance" value="Moyenne"
                               class="input--radio__input batiment"
                                <?php if( sess('nuisance') == "Moyenne") echo " checked" ?>
                               >
                        <label for="moyenne" class="input--radio__label">Moyenne</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="importante" name="nuisance"
                               class="input--radio__input batiment" value="Importante"
                                <?php if( sess('nuisance') == "Importante") echo " checked" ?>
                               >
                        <label for="importante" class="input--radio__label">Importante</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="estimation-row component-property-condition">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>visavis.svg" alt="icon">
                    </div>
                    <label>Vis-à-vis</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon text-left">
                    <div class="input--radio">
                        <input type="radio" value="Aucune" id="aucune-vis-vis"
                               name="vis_a_vis" class="input--radio__input batiment restore"
                               <?php if( !sess('vis_a_vis') || sess('vis_a_vis') == '' || sess('vis_a_vis') == "Aucune") echo "checked" ?>
                                >
                        <label for="aucune-vis-vis" class="input--radio__label">Aucune</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="faible-vis-vis" name="vis_a_vis" value="Faible"
                               class="input--radio__input batiment"
                               <?php if( sess('vis_a_vis') == "Faible") echo " checked" ?>
                               >
                        <label for="faible-vis-vis" class="input--radio__label">Faible</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="moyenne-vis-vis" name="vis_a_vis" value="Moyenne"
                               class="input--radio__input batiment"
                               <?php if( sess('vis_a_vis') == "Moyenne") echo " checked" ?>
                               >
                        <label for="moyenne-vis-vis" class="input--radio__label">Moyenne</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="importante-vis-vis" name="vis_a_vis"
                               class="input--radio__input batiment" value="Importante"
                               <?php if( sess('vis_a_vis') == "Importante") echo " checked" ?>
                               >
                        <label for="importante-vis-vis" class="input--radio__label">Importante</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="estimation-row component-property-condition">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>infiltration.svg" alt="icon">
                    </div>
                    <label>Infiltrations / humidité</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon text-left">
                    <div class="input--radio">
                        <input type="radio" value="Aucune" id="aucune-infiltration"
                               name="infiltration" class="input--radio__input batiment restore" 
                               <?php if( !sess('infiltration') || sess('infiltration') == '' || sess('infiltration') == "Aucune") echo "checked" ?>
                               >
                        <label for="aucune-infiltration" class="input--radio__label">Aucune</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="faible-infiltration" name="infiltration" value="Faible"
                               class="input--radio__input batiment"
                               <?php if( sess('infiltration') == "Faible") echo " checked" ?>
                               >
                        <label for="faible-infiltration" class="input--radio__label">Faible</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="moyenne-infiltration" name="infiltration" value="Moyenne"
                               class="input--radio__input batiment"
                               <?php if( sess('infiltration') == "Moyenne") echo " checked" ?>
                               >
                        <label for="moyenne-infiltration" class="input--radio__label">Moyenne</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="importante-infiltration" name="infiltration"
                               class="input--radio__input batiment" value="Importante"
                               <?php if( sess('infiltration') == "Importante") echo " checked" ?>
                               >
                        <label for="importante-infiltration" class="input--radio__label">Importante</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="estimation-row component-property-condition">
                <div class="d-flex justify-content-start align-items-center">
                    <div class="estimation-row__icon">
                        <img src="<?= IMG_DIR ?>environnement.svg" alt="icon">
                    </div>
                    <label>Environnement / voisinage</label>
                </div>
                <div class="estimation-row__input estimation-row__input--full-icon text-left">
                    <div class="input--radio">
                        <input type="radio" value="Mauvais" id="mauvais"
                               name="environnement" class="input--radio__input batiment restore" 
                               <?php if( !sess('environnement') || sess('infiltration') == '' || sess('infiltration') == "Mauvais") echo "checked" ?>
                               >
                        <label for="mauvais" class="input--radio__label" >Mauvais</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="moyen" name="environnement" value="Moyen"
                               class="input--radio__input batiment"
                               <?php if( sess('environnement') == "Moyen") echo " checked" ?>
                               >
                        <label for="moyen" class="input--radio__label">Moyen</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="bon" name="environnement" value="Bon"
                               class="input--radio__input batiment"
                               <?php if( sess('environnement') == "Bon") echo " checked" ?>
                               >
                        <label for="bon" class="input--radio__label">Bon</label>
                    </div>
                    <div class="input--radio">
                        <input type="radio" id="tres-bon" name="environnement"
                               class="input--radio__input batiment" value="Très bon"
                               <?php if( sess('environnement') == "Très bon") echo " checked" ?>
                               >
                        <label for="tres-bon" class="input--radio__label">Très bon</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="blcBoutton btnForm clr btnForm float-right">
    <button type="button" id="prevBtn" class="precedent bouton btn misy_scrol">< étape
        précédente
    </button>
    <span class="sipan">
        <button type="submit" id="nextBtn" class="bouton btn misy_scrol">étape suivante ></button>
    </span>
</div>
<script>
  $('.precedent').on('click', function(){
    var prev_step = $('[name=prev_step]').val();
    var curr_step = $('[name=curr_step]').val();
    var oldHtml = $('#form_content').html();
    $.ajax(ajaxurl, {
      type: 'POST',
      data: 'action=back_from_' + curr_step +'&show='+prev_step,
      dataType: 'html',
      success: function(resp){
        $('#form_content').html( resp);
        console.log(resp);
        // $('html, body').animate({
        // scrollTop: $(".top-step").offset().top
        // }, 1000)              
      }
    });

    return false;
})
</script>