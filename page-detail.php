

<section class="blocDescri">
   <div class="imgDescri" id="slideImgDescri">
     <img src="<?= IMG_DIR ?>img-descri.jpg" alt="">
   </div>
   <div class="ctDescri">
      <div class="introDescri">
         <div class="itemDescri">
            <div class="itemIntro">
               <img src="<?= IMG_DIR ?>icone-bed.svg" alt="">
               <div>
                  <strong>chambre(s)</strong>
                  <span> chambres</span>
               </div>
            </div>
            <div class="itemIntro">
               <img src="<?= IMG_DIR ?>icone-resize.svg" alt="">
               <div>
                  <strong>Superficie</strong>
                  <span>
                  200 m² habitable - 300 m² totale 
                  </span>
               </div>
            </div>
         </div>
         <div class="blocBtnDetail">
            <a href="tel:0800 12 249" class="btn btn-tel"><b>Appelez-nous</b>0800 12 249</a>
            <a href="#plan_rdv" class="btn scroll">Visiter ce bien</a>
            <a href="#faire_offre" class="btn scroll">Faire une offre</a>
         </div>
      </div>
      <div class="descri">
         <div>
            <div class="textDescri">
               <p class="tmcep">Belle villa offrant un grand living en parquet, cuisine super équipée, 3 chambres, bureau, salle de bains, 2 WC séparés, buanderie, garage, cave, terrasse, jardin.</p>
            </div>
            <span class="lire_la_suite">Lire la suite</span>
         </div>
      </div>
   </div>
</section>


<section class="blocBandeau">
   <div class="container">
      <div class="valeur">Valeur du bien : 2.400€</div>
      <div class="blocBtnDetail">
         <a href="tel:0800 12 249" class="btn btn-tel"><b>Appelez-nous</b>0800 12 249</a>
         <a href="#plan_rdv" class="btn">Visiter ce bien</a>
         <a href="#faire_offre" class="btn">Faire une offre</a>
      </div>
   </div>
</section>



<section class="blocSpec">
   <div class="container">
      <div class="slideBien">
        <div class="item"><img src="<?= IMG_DIR ?>slide1.jpg" alt=""></div>
        <div class="item"><img src="<?= IMG_DIR ?>slide1.jpg" alt=""></div>
        <div class="item"><img src="<?= IMG_DIR ?>slide1.jpg" alt=""></div>
        <div class="item"><img src="<?= IMG_DIR ?>slide1.jpg" alt=""></div>
      </div>
      <div class="spec">
         <div class="content">
            <div class="titre_spec">
                 <h2>Détails du bien</h2>
             </div>
            <div>
               <h3>Détails extérieurs</h3>
               <ul>
                  <li>État : excellent état</li>
                  <li>Sup. habitable : 200 m²</li>
                  <li>Sup. du terrain : 300 m²</li>
                  <li>Jardin : Non</li>
                  <li>Terrasse : Non</li>
                  <li>Parking : Oui</li>
               </ul>
            </div>
            <div>
               <h3>Détails intérieurs</h3>
               <ul>
                  <li>Nombre de chambre : </li>
                  <li>Nombre de salle de bain : </li>
                  <li>Cuisine : amer. super-équipée</li>
                  <li>Buanderie : Non</li>
                  <li>Cave : Non</li>
                  <li>Chauffage : pas installé</li>
                  <li>Châssis : aluminium</li>
               </ul>
            </div>
            <div>
               <h3>Caractéristiques énergétiques</h3>
               <ul>
                  <li><img src="<?= IMG_DIR ?>peb-A.png" alt="PEB" class="peb"></li>
                  <li>E spéc. : 123 kWh/m².an</li>
                  <li>PEB No. : B</li>
                  <li>Niveau PEB : A</li>
                  <li>PEB Total : 123 Kwh/an</li>
                  <li>CO2 : 123 kg/m2/an</li>
               </ul>
            </div>
            <div class="blocBtnDetail">
               <a href="#" class="btn" target="_blank">télécharger la fiche</a>
            </div>
         </div>
      </div>
   </div>
</section>

<section class="sec_download_doc">
    <div class="container">
       <div class="blc_download">
            <div class="content">
               <h2 class="titre">Documents utiles</h2>
               <div class="listDoc">
                   <a href="#" class="btn btn_doc" target="_blank">PEB</a>
                   <a href="#" class="btn btn_doc" target="_blank">Plan de secteur</a>
                   <a href="#" class="btn btn_doc" target="_blank">Plan du cadastre</a>
                   <a href="#" class="btn btn_doc" target="_blank">Certificat électrique</a>
                   <a href="#" class="btn btn_doc" target="_blank">Titre de propriété</a>
                   <a href="#" class="btn btn_doc" target="_blank">BDES</a>
                   <a href="#" class="btn btn_doc" target="_blank">Récapitulatif de la dernière AG</a>
               </div>
           </div>
       </div>
   </div>
</section>



<section class="plan_visite">
   <div class="container">
      <div class="col_left" id="plan_rdv">
         <div class="content">
            <div class="textPlan">
               <p><b>IL RESTE ENCORE <span class="textNbvisite">5</span> VISITES À PLANIFIER</b>Choisissez un créneau horaire pour votre visite (5 visites maximum)</p>
            </div>
            <form class="blcChp_visite" action="" method="post" id="olr_reservation_form">
       <div class="row_visite">
            
             <div class="listDate">
                <div class="item item1">
                   
                   <label for="date1" class="titre">Visite 1</label>
                   <select name="date1" id="date1">
                      <option selected="true" value="">Sélectionnez dans la liste</option>
                   </select>
                   <div id="selected_date1" class="selected_date hidden">
                      <label class="titre">Valider</label>
                      <span class="button-bloc"><input type="button" name="submit-abus" value="OUI" class="submit desk" id="submit_reservation_1">&nbsp;<input type="button" name="cancel-abus" value="NON" class="submit desk" id="cancel_reservation_1"></span>
                   </div>
                </div>
                <div class="item item2 desable">
                
                   <label for="date2" class="titre">Visite 2</label>
                   <select name="date2" id="date2" disabled="disabled">
                      <option value="">Sélectionnez dans la liste</option>
                   </select>
                   <div id="selected_date2" class="selected_date hidden"><label class="titre">Valider</label>
                      <span class="button-bloc"><input type="button" name="submit-abus" value="OUI" class="submit desk" id="submit_reservation_2">&nbsp;<input type="button" name="cancel-abus" value="NON" class="submit desk" id="cancel_reservation_2"></span>
                   </div>
                </div>
                <div class="item item3 desable">
                  
                   <label for="date3" class="titre">Visite 3</label>
                   <select name="date3" id="date3" disabled="disabled">
                      <option value="">Sélectionnez dans la liste</option>
                   </select>
                   <div id="selected_date3" class="selected_date hidden"><label class="titre">Valider</label>
                      <span class="button-bloc"><input type="button" name="submit-abus" value="OUI" class="submit desk" id="submit_reservation_3">&nbsp;<input type="button" name="cancel-abus" value="NON" class="submit desk" id="cancel_reservation_3"></span>
                   </div>
                </div>
                <div class="item item4 desable"> 
                   <label for="date4" class="titre">Visite 4</label>
                   <select name="date4" id="date4" disabled="disabled">
                      <option value="">Sélectionnez dans la liste</option>
                   </select>
                   <div id="selected_date4" class="selected_date hidden"><label class="titre">Valider</label>
                      <span class="button-bloc"><input type="button" name="submit-abus" value="OUI" class="submit desk" id="submit_reservation_4">&nbsp;<input type="button" name="cancel-abus" value="NON" class="submit desk" id="cancel_reservation_4"></span>
                   </div>
                </div>
                <div class="item item5 desable">
                  
                   <label for="date5" class="titre">Visite 5</label>
                   <select name="date5" id="date5" disabled="disabled">
                      <option value="">Sélectionnez dans la liste</option>
                   </select>
                   <div id="selected_date5" class="selected_date hidden"><label class="titre">Valider</label>
                      <span class="button-bloc"><input type="button" name="submit-abus" value="OUI" class="submit desk" id="submit_reservation_5">&nbsp;<input type="button" name="cancel-abus" value="NON" class="submit desk" id="cancel_reservation_5"></span>
                   </div>
                </div>
             </div>
   
        
       </div>
    </form>
         </div>
      </div>
      <div class="col_right" id="faire_offre">
         <div class="content">
               <div class="textPlan">OFFRES REçUES</div>
               <div class="listOffre mCustomScrollbar _mCS_1 mCS_no_scrollbar">
                  <div id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style="max-height: none;" tabindex="0">
                     <div id="mCSB_1_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y" style="position:relative; top:0; left:0;" dir="ltr">
                        <div id="mCSB_1" class="mCustomScrollBox mCS-inset-dark mCSB_vertical mCSB_inside" style="max-height: none;" tabindex="0">
                           <div id="mCSB_1_container" class="mCSB_container mCS_y_hidden mCS_no_scrollbar_y" style="position:relative; top:0; left:0;" dir="ltr">
                              <div id="sans_offre" class="item">
                                 Aucune offre n'a été déposée           
                              </div>
                           </div>
                           <div id="mCSB_1_scrollbar_vertical" class="mCSB_scrollTools mCSB_1_scrollbar mCS-inset-dark mCSB_scrollTools_vertical" style="display: none;">
                              <a href="#" class="mCSB_buttonUp"></a>
                              <div class="mCSB_draggerContainer">
                                 <div id="mCSB_1_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 0px; height: 0px; top: 0px;">
                                    <div class="mCSB_dragger_bar" style="line-height: 0px;"></div>
                                    <div class="mCSB_draggerRail"></div>
                                 </div>
                              </div>
                              <a href="#" class="mCSB_buttonDown"></a>
                           </div>
                        </div>
                     </div>
                     <div id="mCSB_1_scrollbar_vertical" class="mCSB_scrollTools mCSB_1_scrollbar mCS-light mCSB_scrollTools_vertical">
                        <div class="mCSB_draggerContainer">
                           <div id="mCSB_1_dragger_vertical" class="mCSB_dragger" style="position:absolute;">
                              <div class="mCSB_dragger_bar"></div>
                              <div class="mCSB_draggerRail"></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="blcOffre">
                  <div class="blcTitreOffre">
                     <span class="bouton_close"></span>
                     <a class="btn">Soyez le premier à faire une offre</a>
                  </div>
               </div>
            <style type="text/css">
               .plan_visite .col_right .content.active{ padding-bottom: 100px; }
               @media (max-width:600px) {
               .plan_visite .col_right .content.active{ padding-bottom: 50px; }
               }
            </style>
         </div>
      </div>
   </div>
</section>

<style type="text/css">
    .slideBien{ display: flex;flex-wrap: wrap; }
    .slideBien .item{ width: 25%; }


    /*CSS RDV*/
   .listDate .textError {
   color: red !important;
}

.textError.titre_acces {
      text-align: center;
      color:red;
}

.textSuccess.titre_acces {
   text-align: center;
   center;color:green;
}

.loading {
    display: block;
    background: url('../image/loading.gif') no-repeat center middle;
    width: 100%;
    height: 200px;
    margin: 0 auto;
}
/* Hide all the children of the 'loading' element */
.loading * {
    display: none;
}

.listDate button, .listDate select {
    width: 100%;
    font-size: 16px;
    margin: 10px;
}

.plan_visite .blcChp_visite .submit {
    display: inline-block !important;
    margin-top: 10px !important;
    padding: 0 10px 0 10px !important;
    width: auto !important;
}

.hidden {
    display: none;
}

.listDate .selected_date  {
    font-size: 15px;
    padding: 16px 0 16px 10px;
}



.button-bloc {
    float: right;
    display: flex;
}

.plan_visite .listDate select,
.plan_visite .selected_date{ height: 71px;border-radius: 35px;margin: 0;font:bold 18px/28px "lato";-webkit-box-shadow: 0px 4px 16px -7px rgba(0,0,0,0.75);-moz-box-shadow: 0px 4px 16px -7px rgba(0,0,0,0.75);box-shadow: 0px 4px 16px -7px rgba(0,0,0,0.75);border: none;;padding: 10px 35px;text-transform: uppercase;color: #ffffff;}
.plan_visite .listDate select{ background:  url(../image/arrow-down-b.svg)calc(100% - 35px) center no-repeat #A29F78;-webkit-appearance:none; -ms-appearance:none; appearance:none; -moz-appearance:none; -o-appearance:none;padding: 10px 35px;color: #ffffff;text-transform: uppercase; }
.plan_visite .selected_date{ display: flex;justify-content: space-between;align-items: center; }
.plan_visite .selected_date.hidden{ display: none; }
.blcChp_visite .right{ padding-left: 20px; }
.plan_visite .listDate .item {width: 50%;margin-bottom: 27px;padding: 0 10px}

.plan_visite .blcChp_visite .button-bloc .submit{ margin-top: 0!important;width: 100px!important;display: block;height: 54px;background: #ffffff; }
.blcChp_visite .row_visite .selected_date label { margin-bottom: 0; }
.plan_visite .selected_date{ background:#A29F78 ; }
.plan_visite .selected_date.planifie{ background: #ffffff;color: #002850;position: relative;padding-left: 60px; }
.plan_visite .selected_date.planifie:before{ content: '';width: 23px;height: 18px;background:  url(../image/check.svg)center no-repeat;position: absolute;top: 50%;margin-top: -9px;left: 29px; }
.plan_visite .blcChp_visite .button-bloc .submit[name="submit-abus"]{ margin-right: 5px; }
.plan_visite .blcChp_visite .button-bloc .submit[name="cancel-abus"]{ margin-left: 5px; }
.plan_visite .blcChp_visite .button-bloc .submit:checked{ background: #000; }
.plan_visite .blcChp_visite .button-bloc .submit:hover{ background:#00E29D;color: #ffffff; }


@media (max-width:1720px) {
    .plan_visite .listDate select, .plan_visite .selected_date{ padding: 10px 20px; }
    .plan_visite .blcChp_visite .button-bloc .submit { width: 80px!important; }

}

@media (max-width:1440px) {
    .plan_visite .selected_date.planifie::before { left: 15px; }
    .plan_visite .selected_date.planifie{ padding-left: 50px; }

}

@media (max-width:768px) {
    .plan_visite .listDate .item { width: 100%; }
}

@media (max-width:601px) {
   .blcChp_visite .right{ padding-left: 0; } 
   .plan_visite .selected_date.planifie{ padding-right: 15px;font-size: 16px; }
   .plan_visite .listDate .item{ padding: 0; }
}


@media (max-width:601px) {
    
}

</style>





