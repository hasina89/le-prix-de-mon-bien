<?php
get_header(); 

if( have_posts() ):
  
?>

<section>
  <div class="container">
    <div id="accordion">
      <?php while( have_posts() ): the_post(); ?>
        <h2><?php the_title() ?></h2>
        <div>
          <?php the_content() ?>
        </div>
      <?php endwhile; ?>    
    </div>
  </div>
</section>

<script type="text/javascript">
    var $ = jQuery.noConflict();
    $( function() {
      $( "#accordion" ).accordion({
        autoHeight: true,
        animated: false,
        collapsible: true,
        heightStyle: "content"
      });
    } );
</script>

<?php
  endif;
get_footer(); 
?>