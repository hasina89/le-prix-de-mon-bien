<?php

/**

 * Template Name: Merci

 */

	$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  $url_components = parse_url($url);
  parse_str($url_components['query'], $params);
	extract($params);

  add_filter( 'wp_mail_from', 'sender_email_lpdmb' );
  add_filter( 'wp_mail_from_name', 'sender_name_lpdmb' );
  
  $sender = get_field('sender','option');
	

  if( !isset( $org ) || empty( $org ) ){
    $attachments = explode('|', str_replace("\\", '/', $pj)  );
    if( !isset( $ct ) || empty( $ct ) ){
    if( 'agence' == $ct ){

      $destinataire = get_field('agent_immo', 'option');

      $subject = "Demande contact Agent Immo";

      $headers = array("From: $email",'Cc: $sender', 'Content-Type: text/html; charset=UTF-8');

      ob_start();

        include 'inc/template_email/email_estimation.php';

      $body_mail = ob_get_clean();

      if(@wp_mail( $destinataire, $subject, $body_mail, $headers, $attachments )){

          $ret['status'] = 'OK';

          $ret['result'] = 1;

          $ret['msg'] = '<h2 class="title_success">Merci pour votre confiance !</h2><p class="success">Un de nos experts analyse votre dossier, il vous répondra dans les 48H ! TOP Chrono ! </p>';

      }else {

          $ret['status'] = 'KO';

          $ret['result'] = 0;

          $ret['msg'] = '<h2 class="title_error">Erreur</h2><p class="error">une erreur est survenue</p>';

      }

    }elseif( 'coach' == $ct ){

      $destinataire = get_field('coach', 'option');

      $subject = "Demande à être coaché";

      $headers = array("From: $email",'Cc: '.$sender, "Reply-To: $email", 'Content-Type: text/html; charset=UTF-8');

      ob_start();

        include 'inc/template_email/email_pour_coach.php';

      $body_mail = ob_get_clean();

      if(@wp_mail( $destinataire, $subject, $body_mail, $headers, $attachments )){

          $ret['status'] = 'OK';

          $ret['result'] = 1;

          $ret['msg'] = '<h2 class="title_success">Merci pour votre confiance !</h2><p class="success">Un de nos coachs analyse votre dossier, il vous répondra dans les 48H ! TOP Chrono ! </p>';

      }else {

          $ret['status'] = 'KO';

          $ret['result'] = 0;

          $ret['msg'] = '<h2 class="title_error">Erreur</h2><p class="error">une erreur est survenue</p>';

      }
      
    }
    }

  }else{
      // logique quand origine = newsletter

      $sender = get_field('sender','option');
      
      $headers = array('Cc: '.$sender, "Reply-To: " .$sender, 'Content-Type: text/html; charset=UTF-8');

      ob_start();

        include 'inc/template_email/email_client.php';

      $body_mail = ob_get_clean();

      if( 'coach' == $ct ):             
          // Hooking up our functions to WordPress filters 
          add_filter( 'wp_mail_from', 'sender_email_wicoach' );
          add_filter( 'wp_mail_from_name', 'sender_name_wicoach' );
      elseif( 'agence' == $ct ):
          add_filter( 'wp_mail_from', 'sender_email_lpdmb' );
          add_filter( 'wp_mail_from_name', 'sender_name_lpdmb' );           
      endif;  

      if(@wp_mail( $email, 'Merci', $body_mail, $headers )){

          $ret['status'] = 'OK';

          $ret['result'] = 1;

      }else {

          $ret['status'] = 'KO';

          $ret['result'] = 0;

          $ret['msg'] = '<h2 class="title_error">Erreur</h2><p class="error">une erreur est survenue</p>';

      }
  }

get_header(); ?>

<script type="text/javascript">
    var $ = jQuery.noConflict();
     $('.map-head img').on('load', function() {
        $(this).parents('.header-banner').addClass('afficher');
       
    })
</script>
<?php get_footer(); ?>
