<?php
	$current_user = wp_get_current_user();
 
  // GET USER ORDERS (COMPLETED + PROCESSING)
  $customer_orders = get_posts( array(
      'numberposts' => -1,
      'meta_key'    => '_customer_user',
      'meta_value'  => $current_user->ID,
      'post_type'   => wc_get_order_types(),
      // 'post_status' => array_keys( wc_get_is_paid_statuses() ),
      'post_status' => array('wc-processing','wc-completed'),
  ) );

  $abos = array();
  $fiches = array();

  if( count( $customer_orders) ){
  	foreach( $customer_orders as $c_order ){

      $order = wc_get_order( $c_order->ID );
      $items = $order->get_items();
      foreach( $items as $item ){
        $prod_id = $item->get_product_id();
        if( ID_ABO == $prod_id || ID_ABO_AN == $prod_id ){
          $abos[] = $c_order;
        }elseif( get_current_product_fiche( $prod_id) ){
          $fiches[] = $c_order;
        }
      }

  	}
  }
  ?>

  <div class="content_facture">
    <div class="sec_facture">
      <h2 class="titre_factures"> Facture des fiches</h2>

      <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table andrana">
        <thead>
          <tr>
              <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-number"><span class="nobr">Nom et Réf. du bien</span></th>
              <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions"><span class="nobr">Factures</span></th>
          </tr>
        </thead>

        <tbody>
            <?php 
              foreach( $fiches as $fiche ):
                $order = wc_get_order( $fiche->ID ); 
                $url_facture = admin_url() . 'admin-ajax.php?action=generate_wpo_wcpdf&document_type=invoice&order_ids='.$fiche->ID.'&my-account&_wpnonce='.wp_create_nonce();

                $items = $order->get_items();                
            ?>
                    <tr class="woocommerce-orders-table__row  order">
                      <?php 
                      foreach( $items as $item ):
                        $prod_id = $item->get_product_id();
                       ?>
                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number">
                            <a href="<?= esc_url( get_permalink( get_current_product_fiche($prod_id) ) ) ?>">
                              <?= get_the_title( get_current_product_fiche($prod_id) ) ?>
                            </a>
                        </td>
                      <?php endforeach; ?>
                        <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions">
                          <?php 
                          // if( ! empty( $actions ) ) :
                          ?>
                          <div class="item_btn">
                              <a href="<?= $url_facture ?>" class="boutton_action print_order">
                                Télécharger la facture
                              </a>
                            </div>
                          <?php //endif; ?>
                        </td>
                    </tr>
                
            <?php endforeach; ?>
        </tbody>
      </table>
    </div>
    <div class="sec_abonnement">
      <h2 class="titre_factures">Factures des abonnements</h2>

      <table class="woocommerce-orders-table woocommerce-MyAccount-orders shop_table shop_table_responsive my_account_orders account-orders-table andrana">
        <thead>
          <tr>
              <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-number"><span class="nobr">ID</span></th>
              <th class="woocommerce-orders-table__header woocommerce-orders-table__header-order-actions"><span class="nobr">Factures</span></th>
          </tr>
        </thead>

        <tbody>
            <?php 
              foreach( $abos as $abo ):
                $order = wc_get_order( $abo->ID ); 
                $url_facture = admin_url() . 'admin-ajax.php?action=generate_wpo_wcpdf&document_type=invoice&order_ids='.$abo->ID.'&my-account&_wpnonce='.wp_create_nonce();

                $items = $order->get_items();                
            ?>
                <tr class="woocommerce-orders-table__row  order">
                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-number">
                        <a href="#">#<?= $abo->ID ?></a>
                    </td>
                    <td class="woocommerce-orders-table__cell woocommerce-orders-table__cell-order-actions">
                      <div class="item_btn">
                          <a href="<?= $url_facture ?>" class="boutton_action print_order">Télécharger la facture</a>
                        </div>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  <div>

  