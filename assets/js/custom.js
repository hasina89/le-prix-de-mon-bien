var $ = jQuery.noConflict();

$(function ($) {

	/* wow animation */
	new WOW({
		mobile:false,
	}).init() ;

	/* Slide Test */
	$('.slideTeste').slick({
		dots: true,
		infinite: true,
		autoplaySpeed: 4000,
		speed: 1000,
		slidesToShow: 3,
		slideToScroll: 1,
		centerMode:true,
		arrows: true,
		autoplay: true,
		pauseOnHover: false,
		responsive: [
			{
				breakpoint: 1300,
				settings: {
					slidesToShow: 2,
					centerMode:false
				}
			},
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 1,
					centerMode:false,
					arrows:false
				}
			}
		]
	});

	// SCROLL //
	$(".scroll").click(function() {
		var c = $(this).attr("href");
		$('html, body').animate({ scrollTop: $(c).offset().top }, 800, "linear");
		return false;
	});

	
	// to top right away
	if ( window.location.hash ) scroll(0,0);
	// void some browsers issue
	setTimeout( function() { scroll(0,0); }, 1);

	$(function() {

    // *only* if we have anchor on the url
    if(window.location.hash) {

        // smooth scroll to the anchor id
        $('html, body').animate({
            scrollTop: $(window.location.hash).offset().top + 'px'
        }, 2000, 'swing');


    }

})



	// $(".sousForm .btn.misy_scrol").click(function() {
	// 	$('html, body').animate({
	//       scrollTop: $(".top-step").offset().top
	//     }, 1000)
	// });

	$("#APARTMENT, #MAISON").click(function() {
		var vis = $(this);
		$("#APARTMENT, #MAISON").removeClass('selected_property');
		vis.addClass('selected_property');

		var selected_property = $('.selected_property').val();
		if(selected_property == 'Maison'){
			$('#tab2').addClass('invisible_part');
		}else {
			$('#tab2').removeClass('invisible_part');			
		}


		// $('html, body').animate({
	 //      scrollTop: $("#blocForm").offset().top
	 //    }, 1000)
	});

	// END SCROLL //
	

	
});


// SLIDER AREA
$(function() {
    var handle = $("#custom-handle-area span");
    $("#slider-surface").slider({
        min: 0,
        max: 1000,
        step: 1,
        value: 0,
        create: function() {
            handle.text($(this).slider("value"));
        },
        slide: function(event, ui) {
            $("#surfaceOutput").val(ui.value);
            handle.text(ui.value);
        }
    });
    var initialValue = $("#slider-surface").slider("option", "value");
    $("#surfaceOutput").val(initialValue);
    $("#surfaceOutput").change(function() {
        var oldVal = $("#slider-surface").slider("option", "value");
        var newVal = $(this).val();
        if (isNaN(newVal) || newVal < 0 || newVal > 1000) {
            $("#surfaceOutput").val(oldVal);
            handle.text(oldVal);
        } else {
            $("#slider-surface").slider("option", "value", newVal);
            handle.text(newVal);
        }
    });
});

// SLIDER YEAR
$(function() {
    var handle = $("#custom-handle-year span");
    $("#slider-year").slider({
        min: 1600,
        max: 2020,
        step: 1,
        value: 1600,
        create: function() {
            handle.text($(this).slider("value"));
        },
        slide: function(event, ui) {
            $("#yearOutput").val(ui.value);
            handle.text(ui.value);
        }
    });

    var initialValue = $("#slider-year").slider("option", "value");
    $("#yearOutput").val(initialValue);
    $("#yearOutput").change(function() {
        var oldVal = $("#slider-year").slider("option", "value");
        var newVal = $(this).val();
        if (isNaN(newVal) || newVal < 1600 || newVal > 2020) {
            $("#yearOutput").val(oldVal);
            handle.text(oldVal);
        } else {
            $("#slider-year").slider("option", "value", newVal);
            handle.text(newVal);
        }
    });
});

// SLIDER JARDIN
$(function() {
    var handle = $("#custom-handle-garden span");
    $("#slider-surface-jardin").slider({
        min: 0,
        max: 100000,
        step: 1,
        value: 0,
        create: function() {
            handle.text($(this).slider("value"));
        },
        slide: function(event, ui) {
            $("#gardenOutput").val(ui.value);
            handle.text(ui.value);
        }
    });
    var initialValue = $("#slider-surface-jardin").slider("option", "value");
    $("#gardenOutput").val(initialValue);
    $("#gardenOutput").change(function() {
        var oldVal = $("#slider-surface-jardin").slider("option", "value");
        var newVal = $(this).val();
        if (isNaN(newVal) || newVal < 0 || newVal > 2000) {
            $("#gardenOutput").val(oldVal);
            handle.text(oldVal);
        } else {
            $("#slider-surface-jardin").slider("option", "value", newVal);
            handle.text(newVal);
        }
    });
});

// SLIDER TERRASSE
$(function() {
    var handle = $("#custom-handle-terrasse span");
    $("#slider-surface-terrasse").slider({
        min: 0,
        max: 2000,
        step: 1,
        value: 0,
        create: function() {
            handle.text($(this).slider("value"));
        },
        slide: function(event, ui) {
            $("#terrasseOutput").val(ui.value);
            handle.text(ui.value);
        }
    });
    var initialValue = $("#slider-surface-terrasse").slider("option", "value");
    $("#terrasseOutput").val(initialValue);
    $("#terrasseOutput").change(function() {
        var oldVal = $("#slider-surface-jardin").slider("option", "value");
        var newVal = $(this).val();
        if (isNaN(newVal) || newVal < 0 || newVal > 2000) {
            $("#terrasseOutput").val(oldVal);
            handle.text(oldVal);
        } else {
            $("#slider-surface-terrasse").slider("option", "value", newVal);
            handle.text(newVal);
        }
    });
});

// SLIDER GRENIER
$(function() {
    var handle = $("#custom-handle-grenier span");
    $("#slider-surface-grenier").slider({
        min: 0,
        max: 2000,
        step: 1,
        value: 0,
        create: function() {
            handle.text($(this).slider("value"));
        },
        slide: function(event, ui) {
            $("#grenierOutput").val(ui.value);
            handle.text(ui.value);
        }
    });
    var initialValue = $("#slider-surface-grenier").slider("option", "value");
    $("#grenierOutput").val(initialValue);
    $("#grenierOutput").change(function() {
        var oldVal = $("#slider-surface-grenier").slider("option", "value");
        var newVal = $(this).val();
        if (isNaN(newVal) || newVal < 0 || newVal > 2000) {
            $("#grenierOutput").val(oldVal);
            handle.text(oldVal);
        } else {
            $("#slider-surface-grenier").slider("option", "value", newVal);
            handle.text(newVal);
        }
    });
});

/* - or + value */
$(".input-group--stepper button").on("click", function() {
	var $button = $(this);
	var oldValue = $button.parent().find("input").val();

	var btnTXT = $button.text();
    var trimBtnTXT = $.trim(btnTXT);

	if (trimBtnTXT == "+") {
		var newVal = parseFloat(oldValue) + 1;
	} else {
		if (oldValue > 1) {
			var newVal = parseFloat(oldValue) - 1;
		} else {
			newVal = 0;
		}
	}
	$button.parent().find("input").val(newVal);
	return false;
});

/* Show Hidden */
$('#gardenExist').change(function(){
	if (this.checked) {
		$('.jardin').fadeIn('slow');
	}
	else {
		$('.jardin').fadeOut('slow');
	}
});

$('#terrasse').change(function(){
	if (this.checked) {
		$('.terrasse').fadeIn('slow');
	}
	else {
		$('.terrasse').fadeOut('slow');
	}
});

$('#grenier').change(function(){
	if (this.checked) {
		$('.grenier').fadeIn('slow');
	}
	else {
		$('.grenier').fadeOut('slow');
	}
});

// $("#mf_component_parking").on("click", function(){
// 	$( "#sub1, #sub2" ).toggleClass( "show", 200 );
// });

$('.batiment').change(function(){
	var etat = $('.etat');
	var renove = $('.renove');
	var i = $(this).attr("id");
	if (i=="bonEtat") {
		etat.fadeIn(200);
		renove.fadeOut(200);
	}
	else if(i=="renove") {
		renove.fadeIn(200);
		etat.fadeOut(200);
	}
	else if(i=="arenove") {
		renove.fadeOut(200);
		etat.fadeOut(200);
	}
	else {
		etat.fadeOut(200);
		renove.fadeOut(200);
	}
});

$('.sousDate').change(function(){
	var j = $(this).attr("value");
	if (j == "with_date") {
		$('.enterDate').prop("disabled", false);
	}
	else{
		$('.enterDate').prop("disabled", true).val('');
	}
});

$('.ouiNon').change(function(){
	var k = $(this).attr("value");
	if (k == "true") {
		$('.proprietaire').fadeIn(200);
	}
	else if (k == "false"){
		$('.proprietaire').fadeOut(200);
	}
});

// Orientation
$('.compass-slider__input button').click(function(){
	var vis = $(this);
	var orientation = $('#orientation');
	var dataOrientation = vis.attr('data-orientation');
	var vCursor = $('.compass-slider__cursor');
	var screen = $('.compass-slider__output');

	orientation.val(vis.attr('data-txt'));
	screen.text(vis.attr('data-txt'));
	vCursor.css( { 'transform': 'rotate(' + dataOrientation + 'deg)'});
	console.log(dataOrientation);
	return false
});

// MULTI-STEP FORM
$(document).ready(function () {
	// Random Alert shown for the fun of it
	function randomAlert() {
		var min = 5,
			max = 20;
		var rand = Math.floor(Math.random() * (max - min + 1) + min); //Generate Random number between 5 - 20
		// post time in a <span> tag in the Alert
		$("#time").html('Next alert in ' + rand + ' seconds');
		$('#timed-alert').fadeIn(500).delay(3000).fadeOut(500);
		setTimeout(randomAlert, rand * 1000);
	};
	randomAlert();
});

// Multi-Step Form
//var currentTab = 0; // Current tab is set to be the first tab (0)
//showTab(currentTab); // Display the crurrent tab
function showTab(n) {
	// This function will display the specified tab of the form...
	var x = document.getElementsByClassName("tab");
	x[n].style.display = "block";
	//... and fix the Previous/Next buttons:
	if (n == 0) {
		document.getElementById("prevBtn").style.display = "none";
	} else {
		document.getElementById("prevBtn").style.display = "inline";
	}
	if (n == (x.length - 1)) {
		document.getElementById("nextBtn").innerHTML = "Recevoir son rapport";
		$('#nextBtn')
			.addClass('send')
			.removeClass('misy_scrol');
		document.getElementById("prevBtn").style.display = "inline";
		document.getElementById("title").style.display = "none";
	} else {
		document.getElementById("nextBtn").innerHTML = "étape suivante >";
	}
}

function nextPrev(n) {
	// This function will figure out which tab to display
	var x = document.getElementsByClassName("tab");
	// Exit the function if any field in the current tab is invalid:
	if (n == 1 && !validateForm()) return false;
	// Hide the current tab:
	x[currentTab].style.display = "none";
	// Increase or decrease the current tab by 1:
	currentTab = currentTab + n;
	// if you have reached the end of the form...
	if (currentTab >= x.length) {
		$('.tab.tab9.checked.current').css('display','block !important');
		// ... the form gets submitted:
		$('#regForm').submit();
		// $('#response').text('Envoi en cours ...');
		return false;

	}else{
		
	}
	// Otherwise, display the correct tab:
	showTab(currentTab);
	$('html, body').animate({
      scrollTop: $(".top-step").offset().top
    }, 1000)
}



$(".input-radio-label").click(function superNext(){
	nextPrev(1);
})

// $("#nextBtn").click(function superNext(){
// 	$('html, body').animate({
//       scrollTop: $(".top-step").offset().top
//     }, 1000)
// })

function validateForm() {
	var valid = true;
	var tab = $('.tab');
	var curTab = tab.eq(currentTab);
	var reqFields = curTab.find("input[required], select[required]");

	tab.removeClass('current');
	curTab.addClass('current');

	for (i = 0; i < reqFields.length; i++) {
		if ($(reqFields[i]).val() == "" || $(reqFields[i]).val() == null ) {
			$(reqFields[i]).addClass('invalid');
			valid = false;
		}

		// console.log($('.selected_property').val());
	}
	return valid;
}

$(".numbers-row .button").on("click", function () {
	var $button = $(this);
	var oldValue = $button.parent().find("input").val();
	if ($button.text() == "+") {
		var newVal = parseFloat(oldValue) + 1;
	} else {
		if (oldValue > 1) {
			var newVal = parseFloat(oldValue) - 1;
		} else {
			newVal = 1;
		}
	}
	$button.parent().find("input").val(newVal);
});

$('#send_fafana').click(function(){ 
	$('#regForm').submit();
});

// $('#regForm').on('submit', function(){

// 	var form = $('#regForm')[0];
// 	var action = "estimation";//form.attr('action');
// 	var methodType = "POST";//form.attr('method');
// 	// var data = form.serialize();
// 	var btn = $('button.send');
// 	var txtButton = btn.text();

// 	/* fichiers */
// 	var total_upload = $('input[type=file]').length;

// 	var data = new FormData( form );
// 	for( var i = 0; i < total_upload; i++){
// 	  if ( $('.input-file')[i].value != "" ){
// 	      data.append('files['+i+']', $('.input-file')[i].files[0] );
// 	  }
// 	}
// 	var mode = $('#btn_plein').data('mode');
// 	data.append('mode', mode);
// 	data.append('action', "estimation");

// 	$.ajax(ajaxurl, {
// 		type: methodType,
// 		processData: false,
//     	contentType: false,
// 		data: data,
// 		dataType: 'json',
// 		beforeSend:function(){
// 			// $('#response').html('<div style="color: #000;font-weight:bold;">Loading ...</div>');
// 			// $('.sousForm .formText, #Temoinage').hide();
// 		},
// 		success: function(resp){
// 			// console.log(data);
// 			window.location = resp.chekout;
//  		  $('.bouton.btn.send').attr('disabled','disabled');
// 		},
// 		fail: function(){
// 			$('#Temoinage').show();
// 		}
// 	});

	

// 	return false;
// });


$(document).ready(function() {
    $("#regForm").validate({
        rules: {
            // The key name on the left side is the name attribute
            // of an input field. Validation rules are defined

            email: {
                required: true,
                email: true,
            },

            telephone: {
                required: true,
				minlength: 5,
				phoneplus: true
            },
            fake_motiv: {
            	required: true,
            	minlength: 1
            },
            fake_proprio: {
            	required: true,
            	//minlength: 1
            }
        },
        // Specify validation error messages
        messages: {

            email: {
                required: "Veuillez compléter votre adresse e-mail",
                email: "Veuillez saisir un e-mail valide"
            },
            telephone: {
				required: "Veuillez compléter votre téléphone",
				minlength: "Caractère minimum 2",
				phoneplus: "Format de numéro incorrect"
			},
			fake_motiv: {
				required: "Veuillez préciser votre motivation à vendre votre bien",
				minlength: "Caractère minimum 2",
			},
			fake_proprio: {
				required: "Veuillez préciser si vous êtes propriétaire du bien à estimer",
				minlength: "Caractère minimum 2",
			}

        },

     
    })

    jQuery.validator.addMethod("phoneplus", function(value, element) {
	   return this.optional(element) || /^[+]?\d*$/.test(value); });

    $('#nextBtn').click(function() {
        if ($('.tab9').hasClass('current')) {
            $("#regForm").valid();

        }


    });
});


/* FONCTIONALITE FORM */
// Slider value
 $('#surface_habitable_input').on('input', function() {
 	let val = $(this).val();
 	$('#surfaceOutput').val(val);

});

$('#surfaceOutput').on('input', function(){
  //console.log($(this).val())
  $('#surface_habitable_input').val($(this).val())
});



$('#gardenSurface_input').on('input', function() {
 	let val = $(this).val();
 	$('#gardenOutput').val(val);
});

$('#gardenOutput').on('input', function(){
  //console.log($(this).val())
  $('#gardenSurface_input').val($(this).val())
});


$('#terrasseSurface_input').on('input', function() {
	let val = $(this).val();
	$('#terrasseOutput').val(val);
});

$('#terrasseOutput').on('input', function(){
  //console.log($(this).val())
  $('#terrasseSurface_input').val($(this).val())
});


$('#grenierSuperficy_input').on('input', function() {
	let val = $(this).val();
	$('#grenierOutput').val(val);
});

$('#grenierOutput').on('input', function(){
  //console.log($(this).val())
  $('#grenierSuperficy_input').val($(this).val())
});


$('#anneSlide_input').on('input', function() {
 	let val = $(this).val();
 	$('#yearOutput').val(val);
});

$('#yearOutput').on('input', function(){
  //console.log($(this).val())
  $('#anneSlide_input').val($(this).val())
});


// SURFACE HABITABLE
const
  surface_habitable_input = document.getElementById('surface_habitable_input'),
  surface_habitable_output = document.getElementById('surfaceOutput'),
  surface_habitable = document.getElementById('surface_habitable'),
  res = document.getElementById("surfaceOutput-front"),
  setValue = ()=>{
    const
      newValue = Number( (surface_habitable_input.value - surface_habitable_input.min) * 100 / (surface_habitable_input.max - surface_habitable_input.min) ),
      newPosition = 10 - (newValue * 0.2);
    surface_habitable.innerHTML = `<span>${surface_habitable_input.value}</span>`;
    surface_habitable.style.left = `calc(${newValue}% + (${newPosition}px))`;


  };
// document.addEventListener("DOMContentLoaded", setValue);
surface_habitable_input.addEventListener('input', setValue);
surface_habitable_output.addEventListener('input', setValue);
surface_habitable_input.addEventListener("input", function() {
    res.innerHTML = surface_habitable_input.value;
}, false); 
surface_habitable_output.addEventListener("input", function() {
    res.innerHTML = surface_habitable_output.value;
}, false); 



// SURFACE JARDIN

const
  gardenSurface_input = document.getElementById('gardenSurface_input'),
  gardenSurface_output = document.getElementById('gardenOutput'),
  gardenSurface = document.getElementById('gardenSurface'),
  surfaceOutput_front2 = document.getElementById("surfaceOutput_front2"),
  setValue2 = ()=>{
    const
      newValue = Number( (gardenSurface_input.value - gardenSurface_input.min) * 100 / (gardenSurface_input.max - gardenSurface_input.min) ),
      newPosition = 10 - (newValue * 0.2);
    gardenSurface.innerHTML = `<span>${gardenSurface_input.value}</span>`;
    gardenSurface.style.left = `calc(${newValue}% + (${newPosition}px))`;
  };
document.addEventListener("DOMContentLoaded", setValue2);
gardenSurface_input.addEventListener('input', setValue2);
gardenSurface_output.addEventListener('input', setValue2);
gardenSurface_input.addEventListener("input", function() {
    surfaceOutput_front2.innerHTML = gardenSurface_input.value;
}, false); 
gardenSurface_output.addEventListener("input", function() {
    surfaceOutput_front2.innerHTML = gardenSurface_input.value;
}, false);


// SURFACE TERRASSE
const
  terrasseSurface_input = document.getElementById('terrasseSurface_input'),
  terrasseSurface_output = document.getElementById('terrasseOutput'),
  terrasseSurface = document.getElementById('terrasseSurface'),
  surfaceOutput_front3 = document.getElementById("surfaceOutput_front3"),
  setValue3 = ()=>{
    const
      newValue = Number( (terrasseSurface_input.value - terrasseSurface_input.min) * 100 / (terrasseSurface_input.max - terrasseSurface_input.min) ),
      newPosition = 10 - (newValue * 0.2);
    terrasseSurface.innerHTML = `<span>${terrasseSurface_input.value}</span>`;
    terrasseSurface.style.left = `calc(${newValue}% + (${newPosition}px))`;
  };
document.addEventListener("DOMContentLoaded", setValue3);
terrasseSurface_input.addEventListener('input', setValue3);
terrasseSurface_output.addEventListener('input', setValue3);
terrasseSurface_input.addEventListener("input", function() {
    surfaceOutput_front3.innerHTML = terrasseSurface_input.value;
}, false); 
terrasseSurface_output.addEventListener("input", function() {
    surfaceOutput_front3.innerHTML = terrasseSurface_input.value;
}, false);


// SURFACE GRENIER

const
  grenierSuperficy_input = document.getElementById('grenierSuperficy_input'),
  grenierSuperficy_output = document.getElementById('grenierOutput'),
  grenierSuperficy = document.getElementById('grenierSuperficy'),
  surfaceOutput_front4 = document.getElementById("surfaceOutput_front4"),
  setValue4 = ()=>{
    const
    newValue = Number( (grenierSuperficy_input.value - grenierSuperficy_input.min) * 100 / (grenierSuperficy_input.max - grenierSuperficy_input.min) ),
    newPosition = 10 - (newValue * 0.2);
    grenierSuperficy.innerHTML = `<span>${grenierSuperficy_input.value}</span>`;
    grenierSuperficy.style.left = `calc(${newValue}% + (${newPosition}px))`;
  };
document.addEventListener("DOMContentLoaded", setValue4);
grenierSuperficy_input.addEventListener('input', setValue4);
grenierSuperficy_output.addEventListener('input', setValue4);
grenierSuperficy_input.addEventListener("input", function() {
    surfaceOutput_front4.innerHTML = grenierSuperficy_input.value;
}, false);
grenierSuperficy_output.addEventListener("input", function() {
    surfaceOutput_front4.innerHTML = grenierSuperficy_input.value;
}, false); 

// ANNE DE CONSTRUCTION

const
  anneSlide_input = document.getElementById('anneSlide_input'),
  anneSlide_output = document.getElementById('yearOutput'),
  anneSlide = document.getElementById('anneSlide'),
  surfaceOutput_front5 = document.getElementById("surfaceOutput_front5"),
  setValue5 = ()=>{
    const
      newValue = Number( (anneSlide_input.value - anneSlide_input.min) * 100 / (anneSlide_input.max - anneSlide_input.min) ),
      newPosition = 10 - (newValue * 0.2);
    anneSlide.innerHTML = `<span>${anneSlide_input.value}</span>`;
    anneSlide.style.left = `calc(${newValue}% + (${newPosition}px))`;
  };
document.addEventListener("DOMContentLoaded", setValue5);
anneSlide_input.addEventListener('input', setValue5);
anneSlide_output.addEventListener('input', setValue5);
anneSlide_input.addEventListener("input", function() {
    surfaceOutput_front5.innerHTML = anneSlide_input.value;
}, false); 
anneSlide_output.addEventListener("input", function() {
    surfaceOutput_front5.innerHTML = anneSlide_input.value;
}, false); 


// DATEPICKER

  $( function() {
    $( "#datepicker" ).datepicker({
    	altField: "#datepicker",
		closeText: 'Fermer',
		prevText: 'Précédent',
		nextText: 'Suivant',
		currentText: 'Aujourd\'hui',
		monthNames: ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'],
		monthNamesShort: ['Janv.', 'Févr.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juil.', 'Août', 'Sept.', 'Oct.', 'Nov.', 'Déc.'],
		dayNames: ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'],
		dayNamesShort: ['Dim.', 'Lun.', 'Mar.', 'Mer.', 'Jeu.', 'Ven.', 'Sam.'],
		dayNamesMin: ['D', 'L', 'M', 'M', 'J', 'V', 'S'],
		weekHeader: 'Sem.',
		dateFormat: 'yy-mm-dd',
		minDate: 0
    });
  } );

$(".select-call").change(function(){
  var value = $(this).find("option:selected").attr("value");
  if(value=='oui'){
  	$('.date').addClass('show');
  } else{ 
  	$('.date').removeClass('show');
  }
});

$(".evaluation").click(function(){
	var vis = $(this);
	vis_parent = vis.parents();
	vis_parent_prev = vis_parent.prevAll();
	vis_parent_prev.addClass('checked');
	vis_parent_next = vis_parent.nextAll();
	vis_parent_next.removeClass('checked');
	vis.attr('checked',true);
	vis_parent.addClass('checked')
	$('[name=fake_motiv]').val("filled");
});

$('#check_coupon').click(function(){
	$.ajax({
		type: 'POST',
		url: ajaxurl,
		data: {
			'code' : $('input[name="codepromo"]').val(),
			'action' : 'coupon_check_via_ajax'
		},
		dataType: 'json',
		beforeSend:function(){
			$('#check_coupon').val('...');
		},
		success: function(data){
			if(data.status == 0){
				$('#check_coupon')
					.addClass('nety-le-coupon')
					.val( 'Correct');
			}else {
				$('#check_coupon')
					.removeClass('nety-le-coupon')
					.val( 'Valider');
			}
		}
	});
	return false;
});


// INPUT FILE

// preview
function readURL(input, id='#img_init') {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function(e) {
		 	$(id).attr('src', e.target.result);
		}
		reader.readAsDataURL(input.files[0]); // convert to base64 string
	}
}
$("#imgInp").change(function() {
  readURL(this);
});

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

$('#more_files').on('click', function(){
		// fileInit();
			
		var input_id = 'ip_'+getRandomInt(9999999); 
		var img_class = 'img_'+getRandomInt(9999999); 
		var input_id_sharp = '#'+input_id; 

		$('.upload-file .chp').append('<div class="cont-file"><div class="content_cont_file"><input type="file"  class="input-file '+input_id+'" id= "'+input_id+'" name="file[]"><div class="content-preview"><img class="imgPreview '+img_class+'"></div><div class="listBoutton"><i> Charger</i><i class="reset" style="display: none">Supprimer</i></div></div><div class="more">Ajouter une autre photo</div></div>');

		var imgNextID_dyn = '.'+img_class;
		$(input_id_sharp).change(function() {
		  	readURL(this, imgNextID_dyn);
		});

		fileInitOter();
		reset_file_upload();
		more_file();
		$('.cont-file').each(function(){
			var index = $(this).index();
			var img = $(this).find(".imgPreview");
			img.attr('id',index);
			var input = $(this).find(".input-file");
		});

	});
	fileInit();
	fileInitOter();
	reset_file_upload();
	more_file();


	  function handleFileSelect(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
            	$('.cont-file').each(function(){
	                imgId = '#'+$('.imgPreview').attr('id');
	                $(this).find(imgId).attr('src', e.target.result);
                });
            }

            reader.readAsDataURL(input.files[0]);
        }
      }

		
 	  $("input[type='file']").change(function(){
        handleFileSelect(this);
      });

	function fileInit(){
		$('.input-file').change(function() {
			iz=$(this)
			val=iz.val()
			par=iz.parent(".cont-file")
			txtExt=par.find("span")
			txtBroswer=par.find(".listBoutton i")
			txtExt.text(val) 
			// txtBroswer.text("Modifier");
			var index = $(this).parents(".cont-file").index();
			var img = $(this).siblings(".imgPreview");
			img.attr('id',index);

			// iz.hide();
			txtExt.hide();

			imgPreview=iz.siblings('.content-preview').find('.imgPreview');
			imgPreview_parent =imgPreview.parents('.content-preview') ;
			imgPreview.addClass('shown');
			imgPreview_parent.addClass('shown');

		});
	}
	function fileInitOter(){
		$('.input-file').each(function(){
			$(this).change(function() {
				$(this).parents(".cont-file").addClass('uploaded');
				
				iz=$(this)
				resetBtn=iz.siblings('.listBoutton').find('.reset');
				imgPreview=iz.siblings('.content-preview').find('.imgPreview');
				imgPreview_parent =imgPreview.parents('.content-preview') ;
				imgPreview.addClass('shown');
				imgPreview_parent.addClass('shown');
				val=iz.val()
				if(val!=""){
					par=iz.parent(".content_cont_file")
					txtExt=par.find("span")
					txtBroswer=par.find(".listBoutton i")
					par.siblings('.more').addClass('show')
					txtExt.text(val) 
					// txtBroswer.text("Modifier")
					resetBtn.show()
					resetBtn.text('Supprimer');

					// iz.hide();
					txtExt.hide();
				}
				
			})
		})
	}

	function reset_file_upload(){
		$('.reset').each(function(){
			$('.reset').on('click', function(){
				reset = $(this);

				btnChanger = $(this).siblings('i');
				inputFile  = $(this).siblings('.input-file');
				fakeText   = $(this).siblings('span');
				imgPreview   = $(this).parents('.listBoutton').siblings('.content-preview').find('.imgPreview');
				imgPreview_parent =imgPreview.parents('.content-preview') ;
				btnChanger.text("Charger");
				inputFile.val('');

				inputFile.show();
				fakeText.show();
				$('.more').removeClass('show')


				fakeText.text("Aucun fichier sélectionné");

				imgPreview.attr('src', '');
				imgPreview.removeClass('shown');
				imgPreview_parent.removeClass('shown');

				if ( reset.parents('#spn_inputs').length == 1 ){
					reset.parent('.cont-file').remove();
				}else{
					reset.hide();

				}
				return false;
			});
		});
	}
	function more_file(){
	$('.more').each(function(){
		$(this).click(function(){
			$('#more_files').trigger('click');
			$(this).removeClass('show')

		})
	})
	}

	$('.coupon-mobile').click(function() {
		$('.triangle').toggleClass('active');

	})

	$('.triangle .close').click(function() {
		$('.triangle').removeClass('active');

	})

	$('[name=proprietaire_bien]').click(function(){
		if( $('[name=proprietaire_bien]:checked').val() == 'Oui' || $('[name=proprietaire_bien]:checked').val() == 'Non' ){
			$('[name=fake_proprio').val('filled');
		}else{
			$('[name=fake_proprio').val();
		}
	})

	$('.page-template-page-merci .map-head img').on('load', function() {
	    $(this).addClass('rakoto');
	   
	})


	// Accordion
	$("#accordion>div").slideUp();
	$("#accordion > h2").on("click", function(){
		alert('Ny zo');
		$(this).toggleClass("show");
		if ($(this).hasClass("show")) {
			$(this).siblings("#accordion>div").slideDown(400);
		}
		else {
			$(this).siblings("#accordion>div").slideUp(400);
		}
	});