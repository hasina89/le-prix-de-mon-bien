jQuery(function($){
	$('#form-nl').on('submit',function(){
		var form = $(this);
		var data = form.serialize();
		var btn = $('#btn-demande');
		var txt = btn.val();

		$.ajax(ajaxurl, {
			type: 'POST',
			data: {
				data: data,
				action: 'demande_exp'
			},
			dataType: 'json',			
			beforeSend:function(){
				btn.val('Envoi en cours...');
			},
			success: function(resp){

				console.log(resp.url);
				window.location.href = resp.url;
			}
		});

		return false;
	})
});