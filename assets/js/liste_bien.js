var $ = jQuery.noConflict();
$(function () {
    // external js: isotope.pkgd.js

    // init Isotope
    var $grid = $('.filter_biens').isotope({
        itemSelector: '.item',
        layoutMode: 'fitRows'
    });

    var iso = $grid.data('isotope');
    var $paginationContainer = $('.pagination .listPage');
    var $paginationContainerTitle = $('.pagination .filter-count');
    var $parPage = $paginationContainer.data('parpage');

    $grid.isotope({
        filter: '.1'
    });
    // bind filter button click
    $('.etat_biens .list').on('click', 'li', function() {
        var filterValue = $(this).attr('data-filter');
        $grid.isotope({
            filter: filterValue
        });
        $nbrElem = getFilteredElementLength();

        $paginationContainer.html('');
        $paginationContainerTitle.text('');

        $nbrDePage = Math.ceil( $nbrElem / $parPage );
        
        if( $nbrDePage > 1 ){
            $paginationContainerTitle.text('Page');
            for( var i = 1; i <= $nbrDePage; i++ ){
                var activeClass = "";
                if( i == 1 ){
                    activeClass = " active";
                }
                $paginationContainer
                .append('<li class="pager'+ activeClass +'" data-filter=".'+ i +'"><span>'+ i +'</span></li>');
            }
        }
    });

    function getFilteredElementLength() {
      return iso.filteredItems.length;
    }

    // change is-checked class on buttons
    $('.etat_biens .list').each(function(i, buttonGroup) {
        var $buttonGroup = $(buttonGroup);
        $buttonGroup.on('click', 'li', function() {
            $buttonGroup.find('.active').removeClass('active');
            $(this).addClass('active');
        });
    });

    $('.order_price').on('change',function(){
        $('#blcSearch').trigger('submit');
    });

    $('.pagination .listPage').on('click', 'li', function() {
        var filterValue = $(this).attr('data-filter');
        $grid.isotope({
            filter: filterValue
        });
    });
    $('.pagination .listPage').each(function(i, buttonGroup) {
        var $buttonGroup = $(buttonGroup);
        $buttonGroup.on('click', 'li', function() {
            $buttonGroup.find('.active').removeClass('active');
            $(this).addClass('active');

            var currFilter = $(this).data('filter');

            $('.pagination .listPage li[data-filter="'+ currFilter +'"]').addClass('active');
        });
    });



    /*** MOBILE ***/
   
    $(".triage .inner,.pagination .inner").click(function() {
        w = $(window).width()
        if (w < 1023) {
            $(this).find('ul').slideToggle();
            $('.triage li,.pagination li').click(function(){
               var text_span =  $(this).find('span').text();
               var parents_siblings = $(this).parents('ul').siblings('.v-biens');
               parents_siblings.text(text_span)

            })
            
        }
        
    });


});
