var $ = jQuery.noConflict();
$(function () {
$('#etape_timeline').slick({
    dots: false,
    infinite: true,
    autoplaySpeed: 6000,
    speed: 980,
    cssEase: 'linear',
    arrows: false,
    autoplay: true,
    pauseOnHover: false,
    slidesToShow: 5,
    slidesToScroll: 1,
    responsive: [{
            breakpoint: 1280,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows:true
            }
        },
        {
            breakpoint: 980,
            settings: {
                slidesToShow: 2,
                arrows:true
            }
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                arrows:true
            }
        },
        {
            breakpoint: 601,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows:true
            }
        },
    ]
});

$('#listBiens').slick({
    dots: false,
    infinite: true,
    autoplaySpeed: 6000,
    speed: 980,
    cssEase: 'linear',
    arrows: true,
    autoplay: true,
    pauseOnHover: false,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [{
            breakpoint: 1442,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows:true
            }
        },
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 2,
                arrows:true
            }
        },
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows:true
            }
        },
        {
            breakpoint: 601,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows:true
            }
        },
    ]
});

// $('.list_offre').slick({
//     dots: false,
//     infinite: true,
//     autoplaySpeed: 6000,
//     speed: 980,
//     cssEase: 'linear',
//     arrows: false,
//     autoplay: true,
//     pauseOnHover: false,
//     slidesToShow: 3,
//     slidesToScroll: 1,
//     responsive: 
//     [
//         {
//             breakpoint: 1200,
//             settings: {
//                 slidesToShow: 2,
//                 slidesToScroll: 1,
//                 dots:true
//             }
//         },
//         {
//             breakpoint: 980,
//             settings: {
//                 slidesToShow: 1,
//                 slidesToScroll: 1,
//                 dots:true
//             }
//         },
//     ]
// });
/* Slide Test */
$('.slideTeste').slick({
    dots: true,
    infinite: true,
    autoplaySpeed: 4000,
    speed: 1000,
    slidesToShow: 3,
    slideToScroll: 1,
    centerMode:true,
    arrows: true,
    autoplay: false,
    pauseOnHover: false,
    responsive: [
        {
            breakpoint: 1300,
            settings: {
                slidesToShow: 2,
                centerMode:false
            }
        },
        {
            breakpoint: 1024,
            settings: {
                slidesToShow: 1,
                centerMode:false,
                arrows:false
            }
        }
    ]
});

/*
  Achat abonnement 
  */

  $(".fancyboxss").fancybox({
        fitToView: false,
        autoSize: false,
        closeClick: false,
        openEffect: 'none',
        closeEffect: 'none',
        beforeShow: function(instance, slide){
          var link = slide.opts.$orig;
          var modalite = link.data('target');
          $('#modalite').val( modalite );
          $('html.html-has-lrm').addClass('overflow')

        },
        afterClose: function() {
          $('html.html-has-lrm').removeClass('overflow')
        }
   });

  $('.btn-connect').not(".lrm-show-if-logged-in").fancybox({
    beforeShow: function(instance, slide){
          $('html.html-has-lrm').addClass('overflow')

        },
        afterClose: function() {
          $('html.html-has-lrm').removeClass('overflow')
        }

  })
  jQuery.validator.addMethod("ipi_nbr", function(value, element) {
    return this.optional( element ) || /^\d{6}$/.test( value );
  }, 'Numéro IPI invalide. Format : 123456');

  $('#go-partner').validate({
    rules: {          
      nom: {
        required: true
      },
      prenom: {
        required: true
      },
      email: {
        required: true,
        email: true
      },
      telephone: {
        required: true
      },
      numero__ipi: {
        required: true,
        ipi_nbr: true
      },
      paiement: {
        required: true
      },
      modalite: {
        required: true
      },
      accept_cgu: {
        required: true
      },
      info_correct: {
        required: true
      }
    },
    messages: {
      nom: {
        required: "Veuillez entrer votre nom"
      },
      prenom: {
        required: "Veuillez entrer votre nom"
      },
      email: {
        required: "Veuillez entrer votre email",
        email: "Adresse email invalide"
      },
      telephone: {
        required: "Veuillez entrer votre numéro de téléphone"
      },
      numero__ipi: {
        required: "Veuillez entrer votre numéro IPI"
      },
      postal: {
        required: "Veuillez sélectionner un code postal"
      },
      paiement: {
        required: "Veuillez choisir un mode de paiement"
      },
      accept_cgu: {
        required: "Veuillez cocher cette case"
      },
      info_correct: {
        required: "Veuillez cocher cette case"
      }
    },
    submitHandler: function(form){
      $('.home.blockUI.blockOverlay').fadeIn('300');

      var data = new FormData( form );

      var email     = $('#email').val();
      var telephone = $('#telephone').val();
      var fname     = $('#prenom').val();
      var lname     = $('#nom').val();

      data.append('action','buy_abo');

      var container = $('#popup_partenaire .content');
      // var url = $('#paiement_url').val();

      $.ajax(ajaxurl, {
        type: 'POST',
        processData: false,
        contentType: false,
        data: data,
        dataType: 'html',
        success: function(resp){
          $('.home.blockUI.blockOverlay').fadeOut('300');
          container.html('Veuillez patienter...');
          container.html(resp);

        }
      });

    }, //end submit handler
    errorPlacement: function(error, element) {
      if (element.attr("name") == "paiement") {
         error.insertAfter(".form-group.form_paiement .label_form");
      }else {
        error.insertAfter(element);
      }
    }// end error emplacement
  }); //end
  /*
  Fin achat abonnement
  */

});



/** POPUP CONDITION GENERAL **/
$("#label_acceptance").on("click", function() {
  $('.fake_btn_acceptance').trigger('click');
});

$('.content_condition').on('scroll', function() {
    if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
        $('.btn_validation').addClass('accepted');
    }
})

$(".label_acceptance").fancybox({
      fitToView: false,
      autoSize: false,
      closeClick: false,
      openEffect: 'none',
      closeEffect: 'none',
      touch:false,
 });

$(".acceptance_btn").on("click", function() {
  $('#input_acceptance').attr("checked",true);
  parent.jQuery.fancybox.close();

})

/*** TIMER ***/
$('.temps').startTimer();


// $(".popup_condition_general h2").on("click", function() {
//   $(this).addClass('test')
// })

 // function iniFrame() {
 //        if(window.self !== window.top) {
 //          // !== the operator checks if the operands are
 //          // have not the same value or not equal type
 //          $('.page-template-page-checkout-php').addClass('inside_iframe');
 //        } else {
 //          // alert("The page is not iFrame").
 //        }
 //    }
 //    iniFrame();

 // SCROLL //
    $(".scroll").click(function() {
        var c = $(this).attr("href");
        $('html, body').animate({ scrollTop: $(c).offset().top }, 800, "linear");
        return false;
    });



    $(".create_acompte .text").click(function() {
        $.fancybox.close();
    });

    $(function() {
        // *only* if we have anchor on the url
        if(window.location.hash) {
            // smooth scroll to the anchor id
            $('html, body').animate({
                scrollTop: $(window.location.hash).offset().top + 'px'
            }, 2500, 'swing');
        }
    });


    
  








